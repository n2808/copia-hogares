namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nuevaDataaddgestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "VENTA_PERMANENCIA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_COORDINADOR", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_ASESOR", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_TIPODE_DE_CLIENTE", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_TIPO_DE_SOLICITUD", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_MULTIPLAY", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CUENTA_VENTA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_NOMBRE_COMPLETO_CLIENTE", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_tipodoc", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_NUMERO_CEDULA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_fechaExped", c => c.DateTime());
            AddColumn("dbo.Gestions", "VENTA_TIPO_DE_RED", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CIUDAD", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_DIRECCION", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CUENTA_MATRIZ", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_BARRIO", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_Estrato", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_OPERADOR_ACTUAL", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_TELEFONO_FIJO", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CELULAR", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_ent_correspondencia", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_Captura_Email", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CODIGO_DE_VENDEDOR", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CEDULA_DE_VENDEDOR", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_CANAL", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_serviciosActuales", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_SERVICIOS_VENDIDOS", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_AVANZADA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_VELOCIDAD_M", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_TOMAS_ADICIONALES_TV", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_cobroInstalacion", c => c.Boolean(nullable: false));
            AddColumn("dbo.Gestions", "VENTA_CAMPAÑA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_POLITICA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_ActualizarTarifa", c => c.Boolean());
            AddColumn("dbo.Gestions", "VENTA_RENTA_MENSUAL", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_fechaAgendamiento", c => c.DateTime());
            AddColumn("dbo.Gestions", "VENTA_captura_franja", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_captura_rentaMensual", c => c.Int());
            AddColumn("dbo.Gestions", "VENTA_OBSERVACION", c => c.String());
            AddColumn("dbo.Gestions", "VENTA__ID_VISION", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_RECHAZO_BACK", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_NUMERO_A_ACTIVAR", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_PLAN_ACT", c => c.String());
            DropColumn("dbo.Gestions", "captura_Estrato");
            DropColumn("dbo.Gestions", "Captura_Email");
            DropColumn("dbo.Gestions", "captura_ent_correspondencia");
            DropColumn("dbo.Gestions", "captura_tipodoc");
            DropColumn("dbo.Gestions", "captura_fechaExped");
            DropColumn("dbo.Gestions", "captura_serviciosActuales");
            DropColumn("dbo.Gestions", "captura_rentaMensual");
            DropColumn("dbo.Gestions", "captura_ActualizarTarifa");
            DropColumn("dbo.Gestions", "captura_cobroInstalacion");
            DropColumn("dbo.Gestions", "captura_fechaAgendamiento");
            DropColumn("dbo.Gestions", "captura_franja");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gestions", "captura_franja", c => c.String());
            AddColumn("dbo.Gestions", "captura_fechaAgendamiento", c => c.DateTime());
            AddColumn("dbo.Gestions", "captura_cobroInstalacion", c => c.Boolean());
            AddColumn("dbo.Gestions", "captura_ActualizarTarifa", c => c.Boolean());
            AddColumn("dbo.Gestions", "captura_rentaMensual", c => c.Int());
            AddColumn("dbo.Gestions", "captura_serviciosActuales", c => c.String());
            AddColumn("dbo.Gestions", "captura_fechaExped", c => c.DateTime());
            AddColumn("dbo.Gestions", "captura_tipodoc", c => c.String());
            AddColumn("dbo.Gestions", "captura_ent_correspondencia", c => c.String());
            AddColumn("dbo.Gestions", "Captura_Email", c => c.String());
            AddColumn("dbo.Gestions", "captura_Estrato", c => c.Guid());
            DropColumn("dbo.Gestions", "VENTA_PLAN_ACT");
            DropColumn("dbo.Gestions", "VENTA_NUMERO_A_ACTIVAR");
            DropColumn("dbo.Gestions", "VENTA_RECHAZO_BACK");
            DropColumn("dbo.Gestions", "VENTA__ID_VISION");
            DropColumn("dbo.Gestions", "VENTA_OBSERVACION");
            DropColumn("dbo.Gestions", "VENTA_captura_rentaMensual");
            DropColumn("dbo.Gestions", "VENTA_captura_franja");
            DropColumn("dbo.Gestions", "VENTA_captura_fechaAgendamiento");
            DropColumn("dbo.Gestions", "VENTA_RENTA_MENSUAL");
            DropColumn("dbo.Gestions", "VENTA_captura_ActualizarTarifa");
            DropColumn("dbo.Gestions", "VENTA_POLITICA");
            DropColumn("dbo.Gestions", "VENTA_CAMPAÑA");
            DropColumn("dbo.Gestions", "VENTA_captura_cobroInstalacion");
            DropColumn("dbo.Gestions", "VENTA_TOMAS_ADICIONALES_TV");
            DropColumn("dbo.Gestions", "VENTA_VELOCIDAD_M");
            DropColumn("dbo.Gestions", "VENTA_AVANZADA");
            DropColumn("dbo.Gestions", "VENTA_SERVICIOS_VENDIDOS");
            DropColumn("dbo.Gestions", "VENTA_captura_serviciosActuales");
            DropColumn("dbo.Gestions", "VENTA_CANAL");
            DropColumn("dbo.Gestions", "VENTA_CEDULA_DE_VENDEDOR");
            DropColumn("dbo.Gestions", "VENTA_CODIGO_DE_VENDEDOR");
            DropColumn("dbo.Gestions", "VENTA_Captura_Email");
            DropColumn("dbo.Gestions", "VENTA_captura_ent_correspondencia");
            DropColumn("dbo.Gestions", "VENTA_CELULAR");
            DropColumn("dbo.Gestions", "VENTA_TELEFONO_FIJO");
            DropColumn("dbo.Gestions", "VENTA_OPERADOR_ACTUAL");
            DropColumn("dbo.Gestions", "VENTA_captura_Estrato");
            DropColumn("dbo.Gestions", "VENTA_BARRIO");
            DropColumn("dbo.Gestions", "VENTA_CUENTA_MATRIZ");
            DropColumn("dbo.Gestions", "VENTA_DIRECCION");
            DropColumn("dbo.Gestions", "VENTA_CIUDAD");
            DropColumn("dbo.Gestions", "VENTA_TIPO_DE_RED");
            DropColumn("dbo.Gestions", "VENTA_captura_fechaExped");
            DropColumn("dbo.Gestions", "VENTA_NUMERO_CEDULA");
            DropColumn("dbo.Gestions", "VENTA_captura_tipodoc");
            DropColumn("dbo.Gestions", "VENTA_NOMBRE_COMPLETO_CLIENTE");
            DropColumn("dbo.Gestions", "VENTA_CUENTA_VENTA");
            DropColumn("dbo.Gestions", "VENTA_MULTIPLAY");
            DropColumn("dbo.Gestions", "VENTA_TIPO_DE_SOLICITUD");
            DropColumn("dbo.Gestions", "VENTA_TIPODE_DE_CLIENTE");
            DropColumn("dbo.Gestions", "VENTA_ASESOR");
            DropColumn("dbo.Gestions", "VENTA_COORDINADOR");
            DropColumn("dbo.Gestions", "VENTA_PERMANENCIA");
        }
    }
}
