namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizarcosto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DetalleVentas", "Codigo", c => c.String());
            AddColumn("dbo.DetalleVentas", "campana", c => c.String());
            AlterColumn("dbo.ValorAgregadoes", "costo", c => c.String());
            DropColumn("dbo.DetalleVentas", "costo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DetalleVentas", "costo", c => c.Int());
            AlterColumn("dbo.ValorAgregadoes", "costo", c => c.Int());
            DropColumn("dbo.DetalleVentas", "campana");
            DropColumn("dbo.DetalleVentas", "Codigo");
        }
    }
}
