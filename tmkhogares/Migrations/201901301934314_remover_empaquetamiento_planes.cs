namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remover_empaquetamiento_planes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlanesVentas", "EstratoId", "dbo.Configurations");
            DropForeignKey("dbo.PlanesPaquetes", "PaqueteId", "dbo.Paquetes");
            DropForeignKey("dbo.PlanesPaquetes", "PlanId", "dbo.Plans");
            DropForeignKey("dbo.Paquetes", "TipoPaqueteId", "dbo.Configurations");
            DropForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Paquetes");
            DropForeignKey("dbo.DetalleVentas", "PlanesVentaId", "dbo.PlanesVentas");
            DropForeignKey("dbo.ValorAgregadoPaquetes", "PaqueteId", "dbo.Paquetes");
            DropForeignKey("dbo.ValorAgregadoPaquetes", "ValorAgregadoId", "dbo.ValorAgregadoes");
            DropIndex("dbo.DetalleVentas", new[] { "PlanesVentaId" });
            DropIndex("dbo.PlanesVentas", new[] { "PaqueteId" });
            DropIndex("dbo.PlanesVentas", new[] { "EstratoId" });
            DropIndex("dbo.Paquetes", new[] { "TipoPaqueteId" });
            DropIndex("dbo.PlanesPaquetes", new[] { "PlanId" });
            DropIndex("dbo.PlanesPaquetes", new[] { "PaqueteId" });
            DropIndex("dbo.ValorAgregadoPaquetes", new[] { "ValorAgregadoId" });
            DropIndex("dbo.ValorAgregadoPaquetes", new[] { "PaqueteId" });
            DropTable("dbo.PlanesVentas");
            DropTable("dbo.Paquetes");
            DropTable("dbo.PlanesPaquetes");
            DropTable("dbo.ValorAgregadoPaquetes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ValorAgregadoPaquetes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ValorAgregadoId = c.Guid(nullable: false),
                        PaqueteId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlanesPaquetes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlanId = c.Guid(nullable: false),
                        PaqueteId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Paquetes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        TipoPaqueteId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlanesVentas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Estado = c.Boolean(nullable: false),
                        PaqueteId = c.Guid(nullable: false),
                        EstratoId = c.Guid(nullable: false),
                        costo = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ValorAgregadoPaquetes", "PaqueteId");
            CreateIndex("dbo.ValorAgregadoPaquetes", "ValorAgregadoId");
            CreateIndex("dbo.PlanesPaquetes", "PaqueteId");
            CreateIndex("dbo.PlanesPaquetes", "PlanId");
            CreateIndex("dbo.Paquetes", "TipoPaqueteId");
            CreateIndex("dbo.PlanesVentas", "EstratoId");
            CreateIndex("dbo.PlanesVentas", "PaqueteId");
            CreateIndex("dbo.DetalleVentas", "PlanesVentaId");
            AddForeignKey("dbo.ValorAgregadoPaquetes", "ValorAgregadoId", "dbo.ValorAgregadoes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ValorAgregadoPaquetes", "PaqueteId", "dbo.Paquetes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DetalleVentas", "PlanesVentaId", "dbo.PlanesVentas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Paquetes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Paquetes", "TipoPaqueteId", "dbo.Configurations", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PlanesPaquetes", "PlanId", "dbo.Plans", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PlanesPaquetes", "PaqueteId", "dbo.Paquetes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PlanesVentas", "EstratoId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
    }
}
