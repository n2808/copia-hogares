namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class estrato_detalleventa_add : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DetalleVentas", "estrato", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DetalleVentas", "estrato", c => c.Guid());
        }
    }
}
