namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seadicionalatabladeusuariosyconfiguraciondevalidador : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Document = c.String(),
                        Names = c.String(),
                        LastName = c.String(),
                        Phone1 = c.String(),
                        Phone2 = c.String(),
                        Phone3 = c.String(),
                        Email = c.String(),
                        Status = c.Boolean(nullable: false),
                        PassWord = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRols",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RolId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.RolId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.Gestions", "captura_Observacion", c => c.String());
            AddColumn("dbo.Gestions", "cantadaPor", c => c.Guid());
            AddColumn("dbo.Gestions", "ValidadoPor", c => c.Guid());
            AddColumn("dbo.Gestions", "DigitadaPor", c => c.Guid());
            AddColumn("dbo.Gestions", "InstaladaActulizadoPor", c => c.Guid());
            AddColumn("dbo.Gestions", "soportadaPor", c => c.Guid());
            AlterColumn("dbo.Gestions", "captura_barrio", c => c.String());
            AlterColumn("dbo.Gestions", "captura_telefono1", c => c.String());
            AlterColumn("dbo.Gestions", "captura_telefono2", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRols", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRols", "RolId", "dbo.Rols");
            DropIndex("dbo.UserRols", new[] { "UserId" });
            DropIndex("dbo.UserRols", new[] { "RolId" });
            AlterColumn("dbo.Gestions", "captura_telefono2", c => c.Guid());
            AlterColumn("dbo.Gestions", "captura_telefono1", c => c.Guid());
            AlterColumn("dbo.Gestions", "captura_barrio", c => c.Guid());
            DropColumn("dbo.Gestions", "soportadaPor");
            DropColumn("dbo.Gestions", "InstaladaActulizadoPor");
            DropColumn("dbo.Gestions", "DigitadaPor");
            DropColumn("dbo.Gestions", "ValidadoPor");
            DropColumn("dbo.Gestions", "cantadaPor");
            DropColumn("dbo.Gestions", "captura_Observacion");
            DropTable("dbo.UserRols");
            DropTable("dbo.Users");
            DropTable("dbo.Rols");
        }
    }
}
