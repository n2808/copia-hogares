namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addinboundraonllamada : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "Inbound_RazonLlamadaId", c => c.Guid());
            AddColumn("dbo.Gestions", "TipoServicioCamapa", c => c.String());
            CreateIndex("dbo.Gestions", "Inbound_RazonLlamadaId");
            AddForeignKey("dbo.Gestions", "Inbound_RazonLlamadaId", "dbo.Configurations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "Inbound_RazonLlamadaId", "dbo.Configurations");
            DropIndex("dbo.Gestions", new[] { "Inbound_RazonLlamadaId" });
            DropColumn("dbo.Gestions", "TipoServicioCamapa");
            DropColumn("dbo.Gestions", "Inbound_RazonLlamadaId");
        }
    }
}
