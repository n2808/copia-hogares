namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_servicio_datos_carga : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cargas", "estado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Cargas", "usu_cargo", c => c.Guid(nullable: false));
            AddColumn("dbo.Cargas", "ProductoId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cargas", "ProductoId");
            DropColumn("dbo.Cargas", "usu_cargo");
            DropColumn("dbo.Cargas", "estado");
        }
    }
}
