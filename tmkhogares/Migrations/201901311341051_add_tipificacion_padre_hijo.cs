namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tipificacion_padre_hijo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tipificacions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Nombre2 = c.String(),
                        Nombre3 = c.String(),
                        Descripcion = c.String(),
                        Estado = c.Boolean(nullable: false),
                        GestionEfectiva = c.Boolean(nullable: false),
                        order = c.Int(nullable: false),
                        padreId = c.Int(nullable: false),
                        codigoClaro = c.String(),
                        finalClaro = c.String(),
                        procesar = c.String(),
                        final = c.Boolean(nullable: false),
                        codigo = c.Int(),
                        productoId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productos", t => t.productoId, cascadeDelete: true)
                .Index(t => t.productoId);
            
            DropColumn("dbo.Gestions", "TipificacionId");
            DropColumn("dbo.Gestions", "TipificacioBackofficeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gestions", "TipificacioBackofficeId", c => c.Guid());
            AddColumn("dbo.Gestions", "TipificacionId", c => c.Guid());
            DropForeignKey("dbo.Tipificacions", "productoId", "dbo.Productos");
            DropIndex("dbo.Tipificacions", new[] { "productoId" });
            DropTable("dbo.Tipificacions");
        }
    }
}
