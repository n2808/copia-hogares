namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eliminartabladetalleventa : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DetalleVentas", "GestionId", "dbo.Gestions");
            DropIndex("dbo.DetalleVentas", new[] { "GestionId" });
            DropTable("dbo.DetalleVentas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DetalleVentas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        GestionId = c.Int(nullable: false),
                        Costo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PlanesVentaId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.DetalleVentas", "GestionId");
            AddForeignKey("dbo.DetalleVentas", "GestionId", "dbo.Gestions", "Id", cascadeDelete: true);
        }
    }
}
