namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DECOSADICIONALESCAMPOGESTION : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "VENTA_CAMPANA", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_LUGAREXPEDICION", c => c.String());
            AddColumn("dbo.Gestions", "VENTA_DECOSADICIONALES", c => c.String());
            AddColumn("dbo.Gestions", "CODIGO_EVIDENTE", c => c.String());
            DropColumn("dbo.Gestions", "VENTA_CAMPAÑA");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gestions", "VENTA_CAMPAÑA", c => c.String());
            DropColumn("dbo.Gestions", "CODIGO_EVIDENTE");
            DropColumn("dbo.Gestions", "VENTA_DECOSADICIONALES");
            DropColumn("dbo.Gestions", "VENTA_LUGAREXPEDICION");
            DropColumn("dbo.Gestions", "VENTA_CAMPANA");
        }
    }
}
