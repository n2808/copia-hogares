namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductosDetalleVenta : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductosDetalleVentas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        tipoServicio = c.Int(nullable: false),
                        NombreServicio = c.String(),
                        tarifaServicio = c.String(),
                        NumeroServicios = c.Int(nullable: false),
                        Mostrar = c.Boolean(nullable: false),
                        DetalleventaId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.DetalleventaId, cascadeDelete: false)
                .Index(t => t.DetalleventaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductosDetalleVentas", "DetalleventaId", "dbo.Gestions");
            DropIndex("dbo.ProductosDetalleVentas", new[] { "DetalleventaId" });
            DropTable("dbo.ProductosDetalleVentas");
        }
    }
}
