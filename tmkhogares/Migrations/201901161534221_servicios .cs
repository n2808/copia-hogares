namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class servicios : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Paquetes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        TipoPaqueteId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.TipoPaqueteId, cascadeDelete: false)
                .Index(t => t.TipoPaqueteId);
            
            CreateTable(
                "dbo.Plans",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        ServicioId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Servicios", t => t.ServicioId, cascadeDelete: false)
                .Index(t => t.ServicioId);
            
            CreateTable(
                "dbo.Servicios",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlanesPaquetes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlanId = c.Guid(nullable: false),
                        PaqueteId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Paquetes", t => t.PaqueteId, cascadeDelete: false)
                .ForeignKey("dbo.Plans", t => t.PlanId, cascadeDelete: true)
                .Index(t => t.PlanId)
                .Index(t => t.PaqueteId);
            
            CreateTable(
                "dbo.ValorAgregadoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        ServicioId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Servicios", t => t.ServicioId, cascadeDelete: false)
                .Index(t => t.ServicioId);
            
            CreateTable(
                "dbo.ValorAgregadoPaquetes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ValorAgregadoId = c.Guid(nullable: false),
                        PaqueteId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Paquetes", t => t.PaqueteId, cascadeDelete: false)
                .ForeignKey("dbo.ValorAgregadoes", t => t.ValorAgregadoId, cascadeDelete: false)
                .Index(t => t.ValorAgregadoId)
                .Index(t => t.PaqueteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ValorAgregadoPaquetes", "ValorAgregadoId", "dbo.ValorAgregadoes");
            DropForeignKey("dbo.ValorAgregadoPaquetes", "PaqueteId", "dbo.Paquetes");
            DropForeignKey("dbo.ValorAgregadoes", "ServicioId", "dbo.Servicios");
            DropForeignKey("dbo.PlanesPaquetes", "PlanId", "dbo.Plans");
            DropForeignKey("dbo.PlanesPaquetes", "PaqueteId", "dbo.Paquetes");
            DropForeignKey("dbo.Plans", "ServicioId", "dbo.Servicios");
            DropForeignKey("dbo.Paquetes", "TipoPaqueteId", "dbo.Configurations");
            DropIndex("dbo.ValorAgregadoPaquetes", new[] { "PaqueteId" });
            DropIndex("dbo.ValorAgregadoPaquetes", new[] { "ValorAgregadoId" });
            DropIndex("dbo.ValorAgregadoes", new[] { "ServicioId" });
            DropIndex("dbo.PlanesPaquetes", new[] { "PaqueteId" });
            DropIndex("dbo.PlanesPaquetes", new[] { "PlanId" });
            DropIndex("dbo.Plans", new[] { "ServicioId" });
            DropIndex("dbo.Paquetes", new[] { "TipoPaqueteId" });
            DropTable("dbo.ValorAgregadoPaquetes");
            DropTable("dbo.ValorAgregadoes");
            DropTable("dbo.PlanesPaquetes");
            DropTable("dbo.Servicios");
            DropTable("dbo.Plans");
            DropTable("dbo.Paquetes");
        }
    }
}
