namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addgestionvdn_presenceId_telefono : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "TelefonoLlamada", c => c.String());
            AddColumn("dbo.Gestions", "productoIdLlamada", c => c.Int());
            AddColumn("dbo.Gestions", "vdnLlamada", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "vdnLlamada");
            DropColumn("dbo.Gestions", "productoIdLlamada");
            DropColumn("dbo.Gestions", "TelefonoLlamada");
        }
    }
}
