namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Creaciondelcampologin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "login", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "login");
        }
    }
}
