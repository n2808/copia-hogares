namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarTipificacionalagestion : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Gestions", name: "Tipificacion_Id", newName: "TipificacionId");
            RenameIndex(table: "dbo.Gestions", name: "IX_Tipificacion_Id", newName: "IX_TipificacionId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Gestions", name: "IX_TipificacionId", newName: "IX_Tipificacion_Id");
            RenameColumn(table: "dbo.Gestions", name: "TipificacionId", newName: "Tipificacion_Id");
        }
    }
}
