namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiar_estado_plan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Plans", "EstadoId", c => c.Boolean(nullable: false));
            DropColumn("dbo.Plans", "Estado");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Plans", "Estado", c => c.Boolean(nullable: false));
            DropColumn("dbo.Plans", "EstadoId");
        }
    }
}
