namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tipificacionBack_CargaId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "TipificacionRecuperacionId", c => c.Int());
            AddColumn("dbo.Gestions", "CargaId", c => c.Int());
            CreateIndex("dbo.Gestions", "TipificacionRecuperacionId");
            CreateIndex("dbo.Gestions", "CargaId");
            AddForeignKey("dbo.Gestions", "CargaId", "dbo.Cargas", "Id");
            AddForeignKey("dbo.Gestions", "TipificacionRecuperacionId", "dbo.Tipificacions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "TipificacionRecuperacionId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "CargaId", "dbo.Cargas");
            DropIndex("dbo.Gestions", new[] { "CargaId" });
            DropIndex("dbo.Gestions", new[] { "TipificacionRecuperacionId" });
            DropColumn("dbo.Gestions", "CargaId");
            DropColumn("dbo.Gestions", "TipificacionRecuperacionId");
        }
    }
}
