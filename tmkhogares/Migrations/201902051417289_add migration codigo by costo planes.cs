namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmigrationcodigobycostoplanes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PlanesEstratos", "Costo", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlanesEstratos", "Costo", c => c.Int());
        }
    }
}
