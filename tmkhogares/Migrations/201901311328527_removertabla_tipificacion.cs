namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removertabla_tipificacion : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions");
            DropIndex("dbo.Gestions", new[] { "TipificacionId" });
            DropIndex("dbo.Gestions", new[] { "TipificacioBackofficeId" });
            DropTable("dbo.Tipificacions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Tipificacions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Nombre2 = c.String(),
                        Nombre3 = c.String(),
                        Descripcion = c.String(),
                        Estado = c.Boolean(nullable: false),
                        GestionEfectiva = c.Boolean(nullable: false),
                        order = c.Int(nullable: false),
                        codigo = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Gestions", "TipificacioBackofficeId");
            CreateIndex("dbo.Gestions", "TipificacionId");
            AddForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions", "Id");
            AddForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions", "Id");
        }
    }
}
