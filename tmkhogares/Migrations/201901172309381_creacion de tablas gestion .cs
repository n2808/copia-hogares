namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciondetablasgestion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cargas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        NumeroRegistros = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Nombre2 = c.String(),
                        Apellido1 = c.String(),
                        Apellido2 = c.String(),
                        CargaId = c.Int(nullable: false),
                        EstadoId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cargas", t => t.CargaId, cascadeDelete: false)
                .ForeignKey("dbo.Configurations", t => t.EstadoId, cascadeDelete: false)
                .Index(t => t.CargaId)
                .Index(t => t.EstadoId);
            
            CreateTable(
                "dbo.Gestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        EstadoId = c.Guid(nullable: false),
                        ClientId = c.Int(nullable: false),
                        Nombre1 = c.String(),
                        Direccion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.EstadoId, cascadeDelete: false)
                .Index(t => t.EstadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "EstadoId", "dbo.Configurations");
            DropForeignKey("dbo.Clientes", "EstadoId", "dbo.Configurations");
            DropForeignKey("dbo.Clientes", "CargaId", "dbo.Cargas");
            DropIndex("dbo.Gestions", new[] { "EstadoId" });
            DropIndex("dbo.Clientes", new[] { "EstadoId" });
            DropIndex("dbo.Clientes", new[] { "CargaId" });
            DropTable("dbo.Gestions");
            DropTable("dbo.Clientes");
            DropTable("dbo.Cargas");
        }
    }
}
