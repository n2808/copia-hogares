namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tipificacionBackofficeId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Gestions", "TipificacioBackofficeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gestions", "TipificacioBackofficeId", c => c.Int());
        }
    }
}
