namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agrega_fecha_instalacion_a_la_gestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "seguimiento_fechaInstalacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "seguimiento_fechaInstalacion");
        }
    }
}
