namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ventacapturarentamensual : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Gestions", "VENTA_captura_rentaMensual", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Gestions", "VENTA_captura_rentaMensual", c => c.Int());
        }
    }
}
