namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class checkcambios_planes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Plans", "TipoPaqueteId", "dbo.Configurations");
            DropIndex("dbo.Plans", new[] { "TipoPaqueteId" });
            DropColumn("dbo.Plans", "TipoPaqueteId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Plans", "TipoPaqueteId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Plans", "TipoPaqueteId");
            AddForeignKey("dbo.Plans", "TipoPaqueteId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
    }
}
