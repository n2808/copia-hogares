namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addserviciosprincipalesProductos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ServiciosPrincipalesProductos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductoOfrecidoId = c.Int(nullable: false),
                        ServicioId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductosOfrecidos", t => t.ProductoOfrecidoId, cascadeDelete: false)
                .ForeignKey("dbo.Servicios", t => t.ServicioId, cascadeDelete: false)
                .Index(t => t.ProductoOfrecidoId)
                .Index(t => t.ServicioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiciosPrincipalesProductos", "ServicioId", "dbo.Servicios");
            DropForeignKey("dbo.ServiciosPrincipalesProductos", "ProductoOfrecidoId", "dbo.ProductosOfrecidos");
            DropIndex("dbo.ServiciosPrincipalesProductos", new[] { "ServicioId" });
            DropIndex("dbo.ServiciosPrincipalesProductos", new[] { "ProductoOfrecidoId" });
            DropTable("dbo.ServiciosPrincipalesProductos");
        }
    }
}
