namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seguimiento_legalizaciongestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "seguimiento_legalizacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "seguimiento_legalizacion");
        }
    }
}
