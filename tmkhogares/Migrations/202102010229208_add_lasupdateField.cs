namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_lasupdateField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "UltimoUpdate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "UltimoUpdate");
        }
    }
}
