namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fecharecuperarventa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "RecuperadaEn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "RecuperadaEn");
        }
    }
}
