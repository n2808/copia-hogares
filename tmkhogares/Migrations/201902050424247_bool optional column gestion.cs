namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class booloptionalcolumngestion : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Gestions", "VENTA_captura_cobroInstalacion", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Gestions", "VENTA_captura_cobroInstalacion", c => c.Boolean(nullable: false));
        }
    }
}
