// <auto-generated />
namespace tmkhogares.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class caida_recuperacionAgente : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(caida_recuperacionAgente));
        
        string IMigrationMetadata.Id
        {
            get { return "201908202052037_caida_recuperacionAgente"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
