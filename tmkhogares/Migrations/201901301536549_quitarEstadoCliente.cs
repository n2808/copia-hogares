namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class quitarEstadoCliente : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Clientes", "EstadoId", "dbo.Configurations");
            DropIndex("dbo.Clientes", new[] { "EstadoId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Clientes", "EstadoId");
            AddForeignKey("dbo.Clientes", "EstadoId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
    }
}
