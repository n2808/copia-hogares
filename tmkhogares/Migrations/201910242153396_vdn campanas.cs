namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vdncampanas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampanasVdns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreMostrarCanal = c.String(),
                        Descripcion = c.String(),
                        CanalIngreso = c.String(),
                        Producto = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CampanasVdns");
        }
    }
}
