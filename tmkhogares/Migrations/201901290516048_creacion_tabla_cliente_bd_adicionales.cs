namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creacion_tabla_cliente_bd_adicionales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "adicionales_APELLIDO", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_NOMBRE", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_ESTRATEGIA", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_TIPO_PAQUETE", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_TELEFONO_TELMEX", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_FECHA_ULTIMA_OT", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_RENTA_ESTIMADA", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_TRABAJOREVISTA", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_PROBABILIDAD_VENTA", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_CTA_MATRIZ", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_CORTE", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_CUENTAS_ALTERNAS", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_DIRECCIONES_ALTERNAS", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Oferta_1", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Valor_Oferta_1", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Diferencia_OFerta_1", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_1", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Politica_1", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Oferta_2", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Valor_Oferta_2", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Diferencia_OFerta_2", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_2", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Politica_2", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Oferta_3", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Valor_Oferta_3", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Diferencia_OFerta_3", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_3", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Politica_3", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Oferta_1_Adicional", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Oferta_4", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Valor_Oferta_4", c => c.String());
            AddColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_4", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_4");
            DropColumn("dbo.Clientes", "adicionales_Valor_Oferta_4");
            DropColumn("dbo.Clientes", "adicionales_Oferta_4");
            DropColumn("dbo.Clientes", "adicionales_Oferta_1_Adicional");
            DropColumn("dbo.Clientes", "adicionales_Codigo_Politica_3");
            DropColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_3");
            DropColumn("dbo.Clientes", "adicionales_Diferencia_OFerta_3");
            DropColumn("dbo.Clientes", "adicionales_Valor_Oferta_3");
            DropColumn("dbo.Clientes", "adicionales_Oferta_3");
            DropColumn("dbo.Clientes", "adicionales_Codigo_Politica_2");
            DropColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_2");
            DropColumn("dbo.Clientes", "adicionales_Diferencia_OFerta_2");
            DropColumn("dbo.Clientes", "adicionales_Valor_Oferta_2");
            DropColumn("dbo.Clientes", "adicionales_Oferta_2");
            DropColumn("dbo.Clientes", "adicionales_Codigo_Politica_1");
            DropColumn("dbo.Clientes", "adicionales_Codigo_Tarifa_1");
            DropColumn("dbo.Clientes", "adicionales_Diferencia_OFerta_1");
            DropColumn("dbo.Clientes", "adicionales_Valor_Oferta_1");
            DropColumn("dbo.Clientes", "adicionales_Oferta_1");
            DropColumn("dbo.Clientes", "adicionales_DIRECCIONES_ALTERNAS");
            DropColumn("dbo.Clientes", "adicionales_CUENTAS_ALTERNAS");
            DropColumn("dbo.Clientes", "adicionales_CORTE");
            DropColumn("dbo.Clientes", "adicionales_CTA_MATRIZ");
            DropColumn("dbo.Clientes", "adicionales_PROBABILIDAD_VENTA");
            DropColumn("dbo.Clientes", "adicionales_TRABAJOREVISTA");
            DropColumn("dbo.Clientes", "adicionales_RENTA_ESTIMADA");
            DropColumn("dbo.Clientes", "adicionales_FECHA_ULTIMA_OT");
            DropColumn("dbo.Clientes", "adicionales_TELEFONO_TELMEX");
            DropColumn("dbo.Clientes", "adicionales_TIPO_PAQUETE");
            DropColumn("dbo.Clientes", "adicionales_ESTRATEGIA");
            DropColumn("dbo.Clientes", "adicionales_NOMBRE");
            DropColumn("dbo.Clientes", "adicionales_APELLIDO");
        }
    }
}
