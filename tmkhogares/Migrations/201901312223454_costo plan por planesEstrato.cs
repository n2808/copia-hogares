namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class costoplanporplanesEstrato : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanesEstratos", "Costo", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanesEstratos", "Costo");
        }
    }
}
