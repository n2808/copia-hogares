namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddetalleventa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DetalleVentas", "costo", c => c.Int());
            AddColumn("dbo.DetalleVentas", "estrato", c => c.Guid());
            AddColumn("dbo.DetalleVentas", "ValorAgregadoId", c => c.Guid());
            AddColumn("dbo.DetalleVentas", "Tipo", c => c.Int(nullable: false));
            AlterColumn("dbo.DetalleVentas", "planId", c => c.Guid());
            CreateIndex("dbo.DetalleVentas", "planId");
            CreateIndex("dbo.DetalleVentas", "ValorAgregadoId");
            AddForeignKey("dbo.DetalleVentas", "planId", "dbo.Plans", "Id");
            AddForeignKey("dbo.DetalleVentas", "ValorAgregadoId", "dbo.ValorAgregadoes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleVentas", "ValorAgregadoId", "dbo.ValorAgregadoes");
            DropForeignKey("dbo.DetalleVentas", "planId", "dbo.Plans");
            DropIndex("dbo.DetalleVentas", new[] { "ValorAgregadoId" });
            DropIndex("dbo.DetalleVentas", new[] { "planId" });
            AlterColumn("dbo.DetalleVentas", "planId", c => c.Guid(nullable: false));
            DropColumn("dbo.DetalleVentas", "Tipo");
            DropColumn("dbo.DetalleVentas", "ValorAgregadoId");
            DropColumn("dbo.DetalleVentas", "estrato");
            DropColumn("dbo.DetalleVentas", "costo");
        }
    }
}
