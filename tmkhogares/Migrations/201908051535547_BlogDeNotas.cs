namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlogDeNotas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "BlogNotas", c => c.String(unicode: false, storeType: "text"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "BlogNotas");
        }
    }
}
