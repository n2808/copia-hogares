namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class doccliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "captura_docCliente", c => c.String());
            CreateIndex("dbo.Gestions", "ClientId");
            AddForeignKey("dbo.Gestions", "ClientId", "dbo.Clientes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "ClientId", "dbo.Clientes");
            DropIndex("dbo.Gestions", new[] { "ClientId" });
            DropColumn("dbo.Gestions", "captura_docCliente");
        }
    }
}
