namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correcciondetalleventa : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductosDetalleVentas", "DetalleventaId", "dbo.Gestions");
            DropIndex("dbo.ProductosDetalleVentas", new[] { "DetalleventaId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.ProductosDetalleVentas", "DetalleventaId");
            AddForeignKey("dbo.ProductosDetalleVentas", "DetalleventaId", "dbo.Gestions", "Id", cascadeDelete: true);
        }
    }
}
