namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_cantidad_a_valorAgregado : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ValorAgregadoes", "ServicioId", "dbo.Servicios");
            DropIndex("dbo.ValorAgregadoes", new[] { "ServicioId" });
            AddColumn("dbo.ValorAgregadoes", "EstadoId", c => c.Boolean(nullable: false));
            AddColumn("dbo.ValorAgregadoes", "cantidad", c => c.Int(nullable: false));
            DropColumn("dbo.ValorAgregadoes", "Estado");
            DropColumn("dbo.ValorAgregadoes", "ServicioId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ValorAgregadoes", "ServicioId", c => c.Guid(nullable: false));
            AddColumn("dbo.ValorAgregadoes", "Estado", c => c.Boolean(nullable: false));
            DropColumn("dbo.ValorAgregadoes", "cantidad");
            DropColumn("dbo.ValorAgregadoes", "EstadoId");
            CreateIndex("dbo.ValorAgregadoes", "ServicioId");
            AddForeignKey("dbo.ValorAgregadoes", "ServicioId", "dbo.Servicios", "Id", cascadeDelete: true);
        }
    }
}
