namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ayudaventa_cambiodeetalleVentas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DetalleVentas", "planId", "dbo.Plans");
            DropForeignKey("dbo.DetalleVentas", "ValorAgregadoId", "dbo.ValorAgregadoes");
            DropIndex("dbo.DetalleVentas", new[] { "planId" });
            DropIndex("dbo.DetalleVentas", new[] { "ValorAgregadoId" });
            AddColumn("dbo.DetalleVentas", "NombreProducto", c => c.String());
            AddColumn("dbo.DetalleVentas", "CampanaPague1", c => c.String());
            AddColumn("dbo.DetalleVentas", "CampanaPromocion", c => c.String());
            AddColumn("dbo.DetalleVentas", "Descripcion", c => c.String());
            AddColumn("dbo.DetalleVentas", "Valor1", c => c.String());
            AddColumn("dbo.DetalleVentas", "ValorPromocion", c => c.String());
            AddColumn("dbo.DetalleVentas", "MegasPromocion", c => c.String());
            AddColumn("dbo.DetalleVentas", "TVPromocion", c => c.String());
            AddColumn("dbo.DetalleVentas", "TipoProducto", c => c.Int(nullable: false));
            AddColumn("dbo.DetalleVentas", "productoId", c => c.Int(nullable: false));
            AddColumn("dbo.DetalleVentas", "CantidadBase", c => c.Int(nullable: false));
            DropColumn("dbo.DetalleVentas", "planId");
            DropColumn("dbo.DetalleVentas", "ValorAgregadoId");
            DropColumn("dbo.DetalleVentas", "NombrePlan");
            DropColumn("dbo.DetalleVentas", "Codigo");
            DropColumn("dbo.DetalleVentas", "estrato");
            DropColumn("dbo.DetalleVentas", "Tipo");
            DropColumn("dbo.DetalleVentas", "campana");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DetalleVentas", "campana", c => c.String());
            AddColumn("dbo.DetalleVentas", "Tipo", c => c.Int(nullable: false));
            AddColumn("dbo.DetalleVentas", "estrato", c => c.String());
            AddColumn("dbo.DetalleVentas", "Codigo", c => c.String());
            AddColumn("dbo.DetalleVentas", "NombrePlan", c => c.String());
            AddColumn("dbo.DetalleVentas", "ValorAgregadoId", c => c.Guid());
            AddColumn("dbo.DetalleVentas", "planId", c => c.Guid());
            DropColumn("dbo.DetalleVentas", "CantidadBase");
            DropColumn("dbo.DetalleVentas", "productoId");
            DropColumn("dbo.DetalleVentas", "TipoProducto");
            DropColumn("dbo.DetalleVentas", "TVPromocion");
            DropColumn("dbo.DetalleVentas", "MegasPromocion");
            DropColumn("dbo.DetalleVentas", "ValorPromocion");
            DropColumn("dbo.DetalleVentas", "Valor1");
            DropColumn("dbo.DetalleVentas", "Descripcion");
            DropColumn("dbo.DetalleVentas", "CampanaPromocion");
            DropColumn("dbo.DetalleVentas", "CampanaPague1");
            DropColumn("dbo.DetalleVentas", "NombreProducto");
            CreateIndex("dbo.DetalleVentas", "ValorAgregadoId");
            CreateIndex("dbo.DetalleVentas", "planId");
            AddForeignKey("dbo.DetalleVentas", "ValorAgregadoId", "dbo.ValorAgregadoes", "Id");
            AddForeignKey("dbo.DetalleVentas", "planId", "dbo.Plans", "Id");
        }
    }
}
