namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ayudaventa_cambioscostostringint : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DetalleVentas", "Valor1", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.DetalleVentas", "ValorPromocion", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DetalleVentas", "ValorPromocion", c => c.String());
            AlterColumn("dbo.DetalleVentas", "Valor1", c => c.String());
        }
    }
}
