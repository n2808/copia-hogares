namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtarifaplenaproductoOfrecido : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ServiciosPrincipalesProductos", "ProductoOfrecidoId", "dbo.ProductosOfrecidos");
            DropForeignKey("dbo.ServiciosPrincipalesProductos", "ServicioId", "dbo.Servicios");
            DropForeignKey("dbo.TarifaAdicionales", "TiposServiciosAplicaId", "dbo.Servicios");
            DropIndex("dbo.ServiciosPrincipalesProductos", new[] { "ProductoOfrecidoId" });
            DropIndex("dbo.ServiciosPrincipalesProductos", new[] { "ServicioId" });
            DropIndex("dbo.TarifaAdicionales", new[] { "TiposServiciosAplicaId" });
            AddColumn("dbo.TarifaAdicionales", "ProductosOfrecidosId", c => c.Int());
            CreateIndex("dbo.TarifaAdicionales", "ProductosOfrecidosId");
            AddForeignKey("dbo.TarifaAdicionales", "ProductosOfrecidosId", "dbo.ProductosOfrecidos", "Id");
            DropColumn("dbo.TarifaAdicionales", "TiposServiciosAplicaId");
            DropTable("dbo.ServiciosPrincipalesProductos");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ServiciosPrincipalesProductos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductoOfrecidoId = c.Int(nullable: false),
                        ServicioId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.TarifaAdicionales", "TiposServiciosAplicaId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.TarifaAdicionales", "ProductosOfrecidosId", "dbo.ProductosOfrecidos");
            DropIndex("dbo.TarifaAdicionales", new[] { "ProductosOfrecidosId" });
            DropColumn("dbo.TarifaAdicionales", "ProductosOfrecidosId");
            CreateIndex("dbo.TarifaAdicionales", "TiposServiciosAplicaId");
            CreateIndex("dbo.ServiciosPrincipalesProductos", "ServicioId");
            CreateIndex("dbo.ServiciosPrincipalesProductos", "ProductoOfrecidoId");
            AddForeignKey("dbo.TarifaAdicionales", "TiposServiciosAplicaId", "dbo.Servicios", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ServiciosPrincipalesProductos", "ServicioId", "dbo.Servicios", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ServiciosPrincipalesProductos", "ProductoOfrecidoId", "dbo.ProductosOfrecidos", "Id", cascadeDelete: true);
        }
    }
}
