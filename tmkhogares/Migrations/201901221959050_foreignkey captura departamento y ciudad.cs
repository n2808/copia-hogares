namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foreignkeycapturadepartamentoyciudad : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "captura_departamentoId", c => c.Guid());
            AddColumn("dbo.Gestions", "captura_ciudadId", c => c.Guid());
            CreateIndex("dbo.Gestions", "captura_departamentoId");
            CreateIndex("dbo.Gestions", "captura_ciudadId");
            AddForeignKey("dbo.Gestions", "captura_ciudadId", "dbo.Tipificacions", "Id");
            AddForeignKey("dbo.Gestions", "captura_departamentoId", "dbo.Tipificacions", "Id");
            DropColumn("dbo.Gestions", "captura_departamento");
            DropColumn("dbo.Gestions", "captura_ciudad");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gestions", "captura_ciudad", c => c.Guid());
            AddColumn("dbo.Gestions", "captura_departamento", c => c.Guid());
            DropForeignKey("dbo.Gestions", "captura_departamentoId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "captura_ciudadId", "dbo.Tipificacions");
            DropIndex("dbo.Gestions", new[] { "captura_ciudadId" });
            DropIndex("dbo.Gestions", new[] { "captura_departamentoId" });
            DropColumn("dbo.Gestions", "captura_ciudadId");
            DropColumn("dbo.Gestions", "captura_departamentoId");
        }
    }
}
