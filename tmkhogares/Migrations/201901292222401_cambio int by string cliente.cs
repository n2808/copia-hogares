namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiointbystringcliente : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clientes", "CUENTA_CO_ID", c => c.String());
            AlterColumn("dbo.Clientes", "TELEFONO_1", c => c.String());
            AlterColumn("dbo.Clientes", "TELEFONO_2", c => c.String());
            AlterColumn("dbo.Clientes", "TELEFONO_3", c => c.String());
            AlterColumn("dbo.Clientes", "CELULAR_1", c => c.String());
            AlterColumn("dbo.Clientes", "CELULAR_2", c => c.String());
            AlterColumn("dbo.Clientes", "CFM_PLAN", c => c.String());
            AlterColumn("dbo.Clientes", "CFM_PAQ", c => c.String());
            AlterColumn("dbo.Clientes", "ARPU", c => c.String());
            AlterColumn("dbo.Clientes", "TARIFA", c => c.String());
            AlterColumn("dbo.Clientes", "RENTA_RR", c => c.String());
            AlterColumn("dbo.Clientes", "RENTA_BASICOS", c => c.String());
            AlterColumn("dbo.Clientes", "RENTA_ADICIONALES", c => c.String());
            AlterColumn("dbo.Clientes", "RENTA_TOTAL_IVA", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "RENTA_TOTAL_IVA", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_ADICIONALES", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_BASICOS", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_RR", c => c.Int());
            AlterColumn("dbo.Clientes", "TARIFA", c => c.Int());
            AlterColumn("dbo.Clientes", "ARPU", c => c.Int());
            AlterColumn("dbo.Clientes", "CFM_PAQ", c => c.Int());
            AlterColumn("dbo.Clientes", "CFM_PLAN", c => c.Int());
            AlterColumn("dbo.Clientes", "CELULAR_2", c => c.Int());
            AlterColumn("dbo.Clientes", "CELULAR_1", c => c.Int());
            AlterColumn("dbo.Clientes", "TELEFONO_3", c => c.Int());
            AlterColumn("dbo.Clientes", "TELEFONO_2", c => c.Int());
            AlterColumn("dbo.Clientes", "TELEFONO_1", c => c.Int());
            AlterColumn("dbo.Clientes", "CUENTA_CO_ID", c => c.Int());
        }
    }
}
