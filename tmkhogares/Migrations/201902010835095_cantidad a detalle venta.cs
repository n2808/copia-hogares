namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cantidadadetalleventa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DetalleVentas", "cantidad", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DetalleVentas", "cantidad");
        }
    }
}
