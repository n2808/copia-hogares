namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relaciones_usuariosGestions : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Gestions", "ValidadoPor");
            CreateIndex("dbo.Gestions", "DigitadaPor");
            CreateIndex("dbo.Gestions", "InstaladaActulizadoPor");
            CreateIndex("dbo.Gestions", "soportadaPor");
            CreateIndex("dbo.Gestions", "cantadaPor");
            AddForeignKey("dbo.Gestions", "cantadaPor", "dbo.Users", "Id");
            AddForeignKey("dbo.Gestions", "DigitadaPor", "dbo.Users", "Id");
            AddForeignKey("dbo.Gestions", "InstaladaActulizadoPor", "dbo.Users", "Id");
            AddForeignKey("dbo.Gestions", "soportadaPor", "dbo.Users", "Id");
            AddForeignKey("dbo.Gestions", "ValidadoPor", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "ValidadoPor", "dbo.Users");
            DropForeignKey("dbo.Gestions", "soportadaPor", "dbo.Users");
            DropForeignKey("dbo.Gestions", "InstaladaActulizadoPor", "dbo.Users");
            DropForeignKey("dbo.Gestions", "DigitadaPor", "dbo.Users");
            DropForeignKey("dbo.Gestions", "cantadaPor", "dbo.Users");
            DropIndex("dbo.Gestions", new[] { "cantadaPor" });
            DropIndex("dbo.Gestions", new[] { "soportadaPor" });
            DropIndex("dbo.Gestions", new[] { "InstaladaActulizadoPor" });
            DropIndex("dbo.Gestions", new[] { "DigitadaPor" });
            DropIndex("dbo.Gestions", new[] { "ValidadoPor" });
        }
    }
}
