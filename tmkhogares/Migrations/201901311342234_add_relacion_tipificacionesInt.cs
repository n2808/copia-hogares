namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_relacion_tipificacionesInt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "TipificacionId", c => c.Int());
            AddColumn("dbo.Gestions", "TipificacioBackofficeId", c => c.Int());
            CreateIndex("dbo.Gestions", "TipificacionId");
            CreateIndex("dbo.Gestions", "TipificacioBackofficeId");
            AddForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions", "Id");
            AddForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions");
            DropIndex("dbo.Gestions", new[] { "TipificacioBackofficeId" });
            DropIndex("dbo.Gestions", new[] { "TipificacionId" });
            DropColumn("dbo.Gestions", "TipificacioBackofficeId");
            DropColumn("dbo.Gestions", "TipificacionId");
        }
    }
}
