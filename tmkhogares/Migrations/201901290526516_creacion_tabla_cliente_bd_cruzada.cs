namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creacion_tabla_cliente_bd_cruzada : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "cruzada_NUM_DECOS_ADI", c => c.String());
            AddColumn("dbo.Clientes", "cruzada_DIVISION_COMERCIAL", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "cruzada_DIVISION_COMERCIAL");
            DropColumn("dbo.Clientes", "cruzada_NUM_DECOS_ADI");
        }
    }
}
