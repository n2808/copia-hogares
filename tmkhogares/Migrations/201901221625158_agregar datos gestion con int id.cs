namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregardatosgestionconintid : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Gestions", "EstadoId", "dbo.Configurations");
            //DropForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions");
            //DropIndex("dbo.Gestions", new[] { "EstadoId" });
            //DropIndex("dbo.Gestions", new[] { "TipificacionId" });
            DropTable("dbo.Gestions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Gestions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        EstadoId = c.Guid(nullable: false),
                        ClientId = c.Int(nullable: false),
                        Nombre1 = c.String(),
                        Direccion = c.String(),
                        TipificacionId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Gestions", "TipificacionId");
            CreateIndex("dbo.Gestions", "EstadoId");
            AddForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions", "Id");
            AddForeignKey("dbo.Gestions", "EstadoId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
    }
}
