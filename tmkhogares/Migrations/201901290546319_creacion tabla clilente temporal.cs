namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciontablaclilentetemporal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente_temporal",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CONTRATO_CUSTCODE = c.String(),
                        CUENTA_CO_ID = c.Int(nullable: false),
                        TELE_NUMB = c.String(),
                        ESTADO = c.String(),
                        NOMBRE_CLIENTE = c.String(),
                        TIPO_IDENT = c.String(),
                        NUMERO_IDENTIFICACION = c.String(),
                        TIPO_CLIENTE = c.String(),
                        DIRECCION = c.String(),
                        CIUDAD = c.String(),
                        ESTRATO = c.String(),
                        TELEFONO_1 = c.Int(nullable: false),
                        TELEFONO_2 = c.Int(nullable: false),
                        TELEFONO_3 = c.Int(nullable: false),
                        CELULAR_1 = c.Int(nullable: false),
                        CELULAR_2 = c.Int(nullable: false),
                        EMAIL = c.String(),
                        FECHA_VINCULACION = c.String(),
                        ANTIGUEDAD = c.String(),
                        CALIFICACION_CLIENTE = c.String(),
                        TMCODE_ACTUAL = c.String(),
                        TIPO_PRODUCTO = c.String(),
                        SEGMENTO_PLAN = c.String(),
                        TECNOLOGIA_PLAN = c.String(),
                        DES_TMCODE = c.String(),
                        CFM_PLAN = c.Int(nullable: false),
                        SPCODE = c.String(),
                        DES_SPCODE = c.String(),
                        TECNOLOGIA_PAQUETE = c.String(),
                        CFM_PAQ = c.Int(nullable: false),
                        ARPU = c.Int(nullable: false),
                        DETALLE_SERVICIOS_ADICIONALES = c.String(),
                        COMPORTAMIENTO_PAGO = c.String(),
                        CONSUMO_DATOS_MES_1 = c.String(),
                        CONSUMO_DATOS_MES_2 = c.String(),
                        CONSUMO_DATOS_MES_3 = c.String(),
                        MOU_MES = c.String(),
                        TIPO_EQUIPO = c.String(),
                        TICKLER = c.String(),
                        DESACTIV = c.String(),
                        PLAN_PAR = c.String(),
                        AJUSTES = c.String(),
                        minutos_incluidos = c.String(),
                        megas_incluidas = c.String(),
                        Campo_Movil_3 = c.String(),
                        Campo_Movil_4 = c.String(),
                        Campo_Movil_5 = c.String(),
                        NODO = c.String(),
                        COMUNIDAD = c.String(),
                        DIVISION = c.String(),
                        TARIFA = c.Int(nullable: false),
                        RENTA_RR = c.Int(nullable: false),
                        SERVICIO_TV = c.String(),
                        SERVICIO_INTERNET = c.String(),
                        MINTIC = c.String(),
                        SERVICIO_VOZ = c.String(),
                        TIPO_HD = c.String(),
                        TIPO_HBO = c.String(),
                        TIPO_FOX = c.String(),
                        TIPO_ADULTO = c.String(),
                        DECOS_ADICIONALES_HD_PVR = c.String(),
                        TIPO_CLAROVIDEO = c.String(),
                        NUM_DECOS_ADICIONALES_TV = c.String(),
                        TIPO_REVISTA = c.String(),
                        TIPO_OTROS = c.String(),
                        SEGMENTO_CLIENTE = c.String(),
                        DIVISION_COMERCIAL = c.String(),
                        AREA_COMERCIAL = c.String(),
                        ZONA_COMERCIAL = c.String(),
                        Empaquetamiento = c.String(),
                        PAQUETE_ACTUAL = c.String(),
                        RENTA_BASICOS = c.Int(nullable: false),
                        RENTA_ADICIONALES = c.Int(nullable: false),
                        RENTA_TOTAL_IVA = c.Int(nullable: false),
                        BENEFICIO = c.String(),
                        DESCUENTO_OFERTA_COMERCIAL = c.String(),
                        ABS_DESCUENTO = c.String(),
                        VALOR_COMERCIAL_OFERTA = c.String(),
                        VALOR_OFERTA_CON_DESC = c.String(),
                        VENTA_TECNOLOGIA = c.String(),
                        EQUIPO_ACTUAL = c.String(),
                        OFERTA_MULTIPLAY = c.String(),
                        BLINDAJE_MOVIL = c.String(),
                        Campo_Fijo_4 = c.String(),
                        adicionales_APELLIDO = c.String(),
                        adicionales_NOMBRE = c.String(),
                        adicionales_ESTRATEGIA = c.String(),
                        adicionales_TIPO_PAQUETE = c.String(),
                        adicionales_TELEFONO_TELMEX = c.String(),
                        adicionales_FECHA_ULTIMA_OT = c.String(),
                        adicionales_RENTA_ESTIMADA = c.String(),
                        adicionales_TRABAJOREVISTA = c.String(),
                        adicionales_PROBABILIDAD_VENTA = c.String(),
                        adicionales_CTA_MATRIZ = c.String(),
                        adicionales_CORTE = c.String(),
                        adicionales_CUENTAS_ALTERNAS = c.String(),
                        adicionales_DIRECCIONES_ALTERNAS = c.String(),
                        adicionales_Oferta_1 = c.String(),
                        adicionales_Valor_Oferta_1 = c.String(),
                        adicionales_Diferencia_OFerta_1 = c.String(),
                        adicionales_Codigo_Tarifa_1 = c.String(),
                        adicionales_Codigo_Politica_1 = c.String(),
                        adicionales_Oferta_2 = c.String(),
                        adicionales_Valor_Oferta_2 = c.String(),
                        adicionales_Diferencia_OFerta_2 = c.String(),
                        adicionales_Codigo_Tarifa_2 = c.String(),
                        adicionales_Codigo_Politica_2 = c.String(),
                        adicionales_Oferta_3 = c.String(),
                        adicionales_Valor_Oferta_3 = c.String(),
                        adicionales_Diferencia_OFerta_3 = c.String(),
                        adicionales_Codigo_Tarifa_3 = c.String(),
                        adicionales_Codigo_Politica_3 = c.String(),
                        adicionales_Oferta_1_Adicional = c.String(),
                        adicionales_Oferta_4 = c.String(),
                        adicionales_Valor_Oferta_4 = c.String(),
                        adicionales_Codigo_Tarifa_4 = c.String(),
                        cruzada_NUM_DECOS_ADI = c.String(),
                        cruzada_DIVISION_COMERCIAL = c.String(),
                        CargaId = c.Int(nullable: false),
                        EstadoId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cargas", t => t.CargaId, cascadeDelete: true)
                .ForeignKey("dbo.Configurations", t => t.EstadoId, cascadeDelete: true)
                .Index(t => t.CargaId)
                .Index(t => t.EstadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cliente_temporal", "EstadoId", "dbo.Configurations");
            DropForeignKey("dbo.Cliente_temporal", "CargaId", "dbo.Cargas");
            DropIndex("dbo.Cliente_temporal", new[] { "EstadoId" });
            DropIndex("dbo.Cliente_temporal", new[] { "CargaId" });
            DropTable("dbo.Cliente_temporal");
        }
    }
}
