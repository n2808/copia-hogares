namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tipificacionservicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TipificacionServicios",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        TipificacionId = c.Int(nullable: false),
                        Vdn = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Tipificacions", t => t.TipificacionId, cascadeDelete: true)
                .Index(t => t.TipificacionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TipificacionServicios", "TipificacionId", "dbo.Tipificacions");
            DropIndex("dbo.TipificacionServicios", new[] { "TipificacionId" });
            DropTable("dbo.TipificacionServicios");
        }
    }
}
