namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addprecioaplanes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanesEstratos", "precio", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanesEstratos", "precio");
        }
    }
}
