namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcantidadupgradeycantidadnuevos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DetalleVentas", "CantidadUpgrade", c => c.Int(nullable: false));
            AddColumn("dbo.DetalleVentas", "CantidadNuevos", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DetalleVentas", "CantidadNuevos");
            DropColumn("dbo.DetalleVentas", "CantidadUpgrade");
        }
    }
}
