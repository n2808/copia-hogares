namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarGestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "Tipificacion_Id", c => c.Guid());
            CreateIndex("dbo.Gestions", "Tipificacion_Id");
            AddForeignKey("dbo.Gestions", "Tipificacion_Id", "dbo.Tipificacions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "Tipificacion_Id", "dbo.Tipificacions");
            DropIndex("dbo.Gestions", new[] { "Tipificacion_Id" });
            DropColumn("dbo.Gestions", "Tipificacion_Id");
        }
    }
}
