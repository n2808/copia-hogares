namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Creaciondelatabladedetalleventa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DetalleVentas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        GestionId = c.Int(nullable: false),
                        Costo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PlanesVentaId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: false)
                .ForeignKey("dbo.PlanesVentas", t => t.PlanesVentaId, cascadeDelete: false)
                .Index(t => t.GestionId)
                .Index(t => t.PlanesVentaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleVentas", "PlanesVentaId", "dbo.PlanesVentas");
            DropForeignKey("dbo.DetalleVentas", "GestionId", "dbo.Gestions");
            DropIndex("dbo.DetalleVentas", new[] { "PlanesVentaId" });
            DropIndex("dbo.DetalleVentas", new[] { "GestionId" });
            DropTable("dbo.DetalleVentas");
        }
    }
}
