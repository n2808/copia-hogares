namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtarifaadicionales_cantidaddeventa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TarifaAdicionales", "cantidadDeVenta", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TarifaAdicionales", "cantidadDeVenta");
        }
    }
}
