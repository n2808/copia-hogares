namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class se_agregan_datos_del_seguimiento_gestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "seguimiento_OT", c => c.String());
            AddColumn("dbo.Gestions", "seguimiento_prox_fecha_inst", c => c.DateTime());
            AddColumn("dbo.Gestions", "seguimiento_obs", c => c.String());
            AddColumn("dbo.Gestions", "seguimiento_can_Obser", c => c.String());
            AddColumn("dbo.Gestions", "captura_OperadorActual", c => c.String());
            AddColumn("dbo.Gestions", "captura_MontoActual", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Gestions", "captura_fechaPermanencia", c => c.DateTime());
            AddColumn("dbo.Gestions", "seguimiento_cancelacion_Observacion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "seguimiento_cancelacion_Observacion");
            DropColumn("dbo.Gestions", "captura_fechaPermanencia");
            DropColumn("dbo.Gestions", "captura_MontoActual");
            DropColumn("dbo.Gestions", "captura_OperadorActual");
            DropColumn("dbo.Gestions", "seguimiento_can_Obser");
            DropColumn("dbo.Gestions", "seguimiento_obs");
            DropColumn("dbo.Gestions", "seguimiento_prox_fecha_inst");
            DropColumn("dbo.Gestions", "seguimiento_OT");
        }
    }
}
