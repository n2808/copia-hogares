namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_servico_codigo_carga : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cargas", "servicio_codigo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cargas", "servicio_codigo");
        }
    }
}
