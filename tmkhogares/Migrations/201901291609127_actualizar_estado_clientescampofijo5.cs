namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizar_estado_clientescampofijo5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "Campo_Fijo_5", c => c.String());
            AddColumn("dbo.Cliente_temporal", "Campo_Fijo_5", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cliente_temporal", "Campo_Fijo_5");
            DropColumn("dbo.Clientes", "Campo_Fijo_5");
        }
    }
}
