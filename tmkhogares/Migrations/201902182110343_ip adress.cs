namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ipadress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "Ip", c => c.String());
            AddColumn("dbo.Gestions", "MacchineName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "MacchineName");
            DropColumn("dbo.Gestions", "Ip");
        }
    }
}
