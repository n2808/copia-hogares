namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_campos_gestion_captura : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "captura_planActual", c => c.String());
            AddColumn("dbo.Gestions", "captura_fechaRellamada", c => c.DateTime());
            AddColumn("dbo.Gestions", "captura_horaRellamada", c => c.String());
            AddColumn("dbo.Gestions", "captura_Estrato", c => c.Guid());
            AddColumn("dbo.Gestions", "Captura_Email", c => c.String());
            AddColumn("dbo.Gestions", "captura_ent_correspondencia", c => c.String());
            AddColumn("dbo.Gestions", "VentaReferido", c => c.Boolean());
            AddColumn("dbo.Gestions", "captura_tipodoc", c => c.String());
            AddColumn("dbo.Gestions", "captura_fechaExped", c => c.DateTime());
            AddColumn("dbo.Gestions", "captura_serviciosActuales", c => c.String());
            AddColumn("dbo.Gestions", "captura_rentaMensual", c => c.Int());
            AddColumn("dbo.Gestions", "captura_ActualizarTarifa", c => c.Boolean());
            AddColumn("dbo.Gestions", "captura_cobroInstalacion", c => c.Boolean());
            AddColumn("dbo.Gestions", "captura_fechaAgendamiento", c => c.DateTime());
            AddColumn("dbo.Gestions", "captura_franja", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "captura_franja");
            DropColumn("dbo.Gestions", "captura_fechaAgendamiento");
            DropColumn("dbo.Gestions", "captura_cobroInstalacion");
            DropColumn("dbo.Gestions", "captura_ActualizarTarifa");
            DropColumn("dbo.Gestions", "captura_rentaMensual");
            DropColumn("dbo.Gestions", "captura_serviciosActuales");
            DropColumn("dbo.Gestions", "captura_fechaExped");
            DropColumn("dbo.Gestions", "captura_tipodoc");
            DropColumn("dbo.Gestions", "VentaReferido");
            DropColumn("dbo.Gestions", "captura_ent_correspondencia");
            DropColumn("dbo.Gestions", "Captura_Email");
            DropColumn("dbo.Gestions", "captura_Estrato");
            DropColumn("dbo.Gestions", "captura_horaRellamada");
            DropColumn("dbo.Gestions", "captura_fechaRellamada");
            DropColumn("dbo.Gestions", "captura_planActual");
        }
    }
}
