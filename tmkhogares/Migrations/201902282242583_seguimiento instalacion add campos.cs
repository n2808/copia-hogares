namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seguimientoinstalacionaddcampos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions");
            DropIndex("dbo.Gestions", new[] { "TipificacioBackofficeId" });
            AddColumn("dbo.Gestions", "Instalacion_EstadoInicial", c => c.Guid());
            AddColumn("dbo.Gestions", "Instalacion_EstadoFinal", c => c.Guid());
            AddColumn("dbo.Gestions", "Instalacion_RazonAgendamiento", c => c.Guid());
            AddColumn("dbo.Gestions", "fechaReprogramacion", c => c.DateTime());
            AddColumn("dbo.Gestions", "Digitacion_RegistroNumero", c => c.String());
            AddColumn("dbo.Gestions", "Digitacion_Contrato", c => c.String());
            AddColumn("dbo.Gestions", "Digitacion_Cuenta", c => c.String());
            AddColumn("dbo.Gestions", "Franja", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "Franja");
            DropColumn("dbo.Gestions", "Digitacion_Cuenta");
            DropColumn("dbo.Gestions", "Digitacion_Contrato");
            DropColumn("dbo.Gestions", "Digitacion_RegistroNumero");
            DropColumn("dbo.Gestions", "fechaReprogramacion");
            DropColumn("dbo.Gestions", "Instalacion_RazonAgendamiento");
            DropColumn("dbo.Gestions", "Instalacion_EstadoFinal");
            DropColumn("dbo.Gestions", "Instalacion_EstadoInicial");
            CreateIndex("dbo.Gestions", "TipificacioBackofficeId");
            AddForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions", "Id");
        }
    }
}
