namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregancamposdatetimeparaestadosgestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "cantadaEn", c => c.DateTime());
            AddColumn("dbo.Gestions", "soportadaEn", c => c.DateTime());
            AddColumn("dbo.Gestions", "DigitadaEn", c => c.DateTime());
            AddColumn("dbo.Gestions", "instaladaEn", c => c.DateTime());
            CreateIndex("dbo.Gestions", "captura_departamentoId");
            CreateIndex("dbo.Gestions", "captura_ciudadId");
            AddForeignKey("dbo.Gestions", "captura_ciudadId", "dbo.Cities", "Id");
            AddForeignKey("dbo.Gestions", "captura_departamentoId", "dbo.States", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "captura_departamentoId", "dbo.States");
            DropForeignKey("dbo.Gestions", "captura_ciudadId", "dbo.Cities");
            DropIndex("dbo.Gestions", new[] { "captura_ciudadId" });
            DropIndex("dbo.Gestions", new[] { "captura_departamentoId" });
            DropColumn("dbo.Gestions", "instaladaEn");
            DropColumn("dbo.Gestions", "DigitadaEn");
            DropColumn("dbo.Gestions", "soportadaEn");
            DropColumn("dbo.Gestions", "cantadaEn");
        }
    }
}
