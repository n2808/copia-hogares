namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class planes_simplesparacrear : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlanesEstratos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlanId = c.Guid(nullable: false),
                        EstratoId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.EstratoId, cascadeDelete: false)
                .ForeignKey("dbo.Plans", t => t.PlanId, cascadeDelete: true)
                .Index(t => t.PlanId)
                .Index(t => t.EstratoId);
            
            AddColumn("dbo.Plans", "Descripcion", c => c.String(unicode: false, storeType: "text"));
            AddColumn("dbo.Plans", "CodigoPlan", c => c.String());
            AddColumn("dbo.Plans", "TipoPaqueteId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Plans", "TipoPaqueteId");
            AddForeignKey("dbo.Plans", "TipoPaqueteId", "dbo.Configurations", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Plans", "TipoPaqueteId", "dbo.Configurations");
            DropForeignKey("dbo.PlanesEstratos", "PlanId", "dbo.Plans");
            DropForeignKey("dbo.PlanesEstratos", "EstratoId", "dbo.Configurations");
            DropIndex("dbo.PlanesEstratos", new[] { "EstratoId" });
            DropIndex("dbo.PlanesEstratos", new[] { "PlanId" });
            DropIndex("dbo.Plans", new[] { "TipoPaqueteId" });
            DropColumn("dbo.Plans", "TipoPaqueteId");
            DropColumn("dbo.Plans", "CodigoPlan");
            DropColumn("dbo.Plans", "Descripcion");
            DropTable("dbo.PlanesEstratos");
        }
    }
}
