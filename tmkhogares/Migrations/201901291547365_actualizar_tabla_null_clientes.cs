namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizar_tabla_null_clientes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cliente_temporal", "CargaId", "dbo.Cargas");
            DropIndex("dbo.Cliente_temporal", new[] { "CargaId" });
            AlterColumn("dbo.Clientes", "CUENTA_CO_ID", c => c.Int());
            AlterColumn("dbo.Clientes", "TELEFONO_1", c => c.Int());
            AlterColumn("dbo.Clientes", "TELEFONO_2", c => c.Int());
            AlterColumn("dbo.Clientes", "TELEFONO_3", c => c.Int());
            AlterColumn("dbo.Clientes", "CELULAR_1", c => c.Int());
            AlterColumn("dbo.Clientes", "CELULAR_2", c => c.Int());
            AlterColumn("dbo.Clientes", "CFM_PLAN", c => c.Int());
            AlterColumn("dbo.Clientes", "CFM_PAQ", c => c.Int());
            AlterColumn("dbo.Clientes", "ARPU", c => c.Int());
            AlterColumn("dbo.Clientes", "TARIFA", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_RR", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_BASICOS", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_ADICIONALES", c => c.Int());
            AlterColumn("dbo.Clientes", "RENTA_TOTAL_IVA", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CUENTA_CO_ID", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_1", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_2", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_3", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CELULAR_1", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CELULAR_2", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CFM_PLAN", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CFM_PAQ", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "ARPU", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TARIFA", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_RR", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_BASICOS", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_ADICIONALES", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_TOTAL_IVA", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CargaId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cliente_temporal", "CargaId", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "RENTA_TOTAL_IVA", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "RENTA_ADICIONALES", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "RENTA_BASICOS", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "RENTA_RR", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "TARIFA", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "ARPU", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "CFM_PAQ", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "CFM_PLAN", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "CELULAR_2", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "CELULAR_1", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_3", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_2", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_1", c => c.Int(nullable: false));
            AlterColumn("dbo.Cliente_temporal", "CUENTA_CO_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "RENTA_TOTAL_IVA", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "RENTA_ADICIONALES", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "RENTA_BASICOS", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "RENTA_RR", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "TARIFA", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "ARPU", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "CFM_PAQ", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "CFM_PLAN", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "CELULAR_2", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "CELULAR_1", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "TELEFONO_3", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "TELEFONO_2", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "TELEFONO_1", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "CUENTA_CO_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.Cliente_temporal", "CargaId");
            AddForeignKey("dbo.Cliente_temporal", "CargaId", "dbo.Cargas", "Id", cascadeDelete: true);
        }
    }
}
