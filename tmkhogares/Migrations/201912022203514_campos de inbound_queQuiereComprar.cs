namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camposdeinbound_queQuiereComprar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "QueQuiereComprar", c => c.String());
            AddColumn("dbo.Gestions", "TeamLeader", c => c.String());
            AddColumn("dbo.Gestions", "Gerente", c => c.String());
            AddColumn("dbo.Users", "GerenteId", c => c.Guid());
            AddColumn("dbo.Users", "campanaId", c => c.Int());
            CreateIndex("dbo.Users", "campanaId");
            AddForeignKey("dbo.Users", "campanaId", "dbo.Productos", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "campanaId", "dbo.Productos");
            DropIndex("dbo.Users", new[] { "campanaId" });
            DropColumn("dbo.Users", "campanaId");
            DropColumn("dbo.Users", "GerenteId");
            DropColumn("dbo.Gestions", "Gerente");
            DropColumn("dbo.Gestions", "TeamLeader");
            DropColumn("dbo.Gestions", "QueQuiereComprar");
        }
    }
}
