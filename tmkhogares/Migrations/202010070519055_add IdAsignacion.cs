namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIdAsignacion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "ID_ASIGNACION", c => c.String());
            AddColumn("dbo.Clientes", "ID_ASIGNACION", c => c.String());
            AddColumn("dbo.Cliente_temporal", "ID_ASIGNACION", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cliente_temporal", "ID_ASIGNACION");
            DropColumn("dbo.Clientes", "ID_ASIGNACION");
            DropColumn("dbo.Gestions", "ID_ASIGNACION");
        }
    }
}
