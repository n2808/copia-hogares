namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiosguidayudaventas : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ProductosDetalleVentas", "DetalleventaId");
            AddForeignKey("dbo.ProductosDetalleVentas", "DetalleventaId", "dbo.DetalleVentas", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductosDetalleVentas", "DetalleventaId", "dbo.DetalleVentas");
            DropIndex("dbo.ProductosDetalleVentas", new[] { "DetalleventaId" });
        }
    }
}
