namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class auditadoporforeignkey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Gestions", "AuditadoPor");
            AddForeignKey("dbo.Gestions", "AuditadoPor", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "AuditadoPor", "dbo.Users");
            DropIndex("dbo.Gestions", new[] { "AuditadoPor" });
        }
    }
}
