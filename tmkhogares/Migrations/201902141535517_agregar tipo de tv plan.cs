namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregartipodetvplan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Plans", "TipoTv", c => c.String());
            AddColumn("dbo.Plans", "tipoBa", c => c.String());
            DropColumn("dbo.Plans", "CodigoPlan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Plans", "CodigoPlan", c => c.String());
            DropColumn("dbo.Plans", "tipoBa");
            DropColumn("dbo.Plans", "TipoTv");
        }
    }
}
