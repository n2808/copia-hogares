namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_id_cliente_cargado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "car_ProductoId", c => c.Int());
            AddColumn("dbo.Cliente_temporal", "id_clienteCargado", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cliente_temporal", "id_clienteCargado");
            DropColumn("dbo.Clientes", "car_ProductoId");
        }
    }
}
