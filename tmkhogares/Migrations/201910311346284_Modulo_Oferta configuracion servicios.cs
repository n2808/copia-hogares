namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modulo_Ofertaconfiguracionservicios : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductosOfrecidos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        TipoVentaId = c.Guid(),
                        EsProductoPrincipal = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.TipoVentaId)
                .Index(t => t.TipoVentaId);
            
            AddColumn("dbo.Servicios", "ServicioPrincipal", c => c.Boolean());
            AddColumn("dbo.Servicios", "ProductoOfrecidoId", c => c.Int());
            CreateIndex("dbo.Servicios", "ProductoOfrecidoId");
            AddForeignKey("dbo.Servicios", "ProductoOfrecidoId", "dbo.ProductosOfrecidos", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Servicios", "ProductoOfrecidoId", "dbo.ProductosOfrecidos");
            DropForeignKey("dbo.ProductosOfrecidos", "TipoVentaId", "dbo.Configurations");
            DropIndex("dbo.ProductosOfrecidos", new[] { "TipoVentaId" });
            DropIndex("dbo.Servicios", new[] { "ProductoOfrecidoId" });
            DropColumn("dbo.Servicios", "ProductoOfrecidoId");
            DropColumn("dbo.Servicios", "ServicioPrincipal");
            DropTable("dbo.ProductosOfrecidos");
        }
    }
}
