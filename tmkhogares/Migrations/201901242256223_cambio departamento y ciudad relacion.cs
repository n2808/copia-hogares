namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiodepartamentoyciudadrelacion : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Gestions", "captura_ciudadId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "captura_departamentoId", "dbo.Tipificacions");
            DropIndex("dbo.Gestions", new[] { "captura_departamentoId" });
            DropIndex("dbo.Gestions", new[] { "captura_ciudadId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Gestions", "captura_ciudadId");
            CreateIndex("dbo.Gestions", "captura_departamentoId");
            AddForeignKey("dbo.Gestions", "captura_departamentoId", "dbo.Tipificacions", "Id");
            AddForeignKey("dbo.Gestions", "captura_ciudadId", "dbo.Tipificacions", "Id");
        }
    }
}
