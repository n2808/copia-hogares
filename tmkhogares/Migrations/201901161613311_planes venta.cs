namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class planesventa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlanesVentas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Estado = c.Boolean(nullable: false),
                        PaqueteId = c.Guid(nullable: false),
                        EstratoId = c.Guid(nullable: false),
                        costo = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.EstratoId, cascadeDelete: false)
                .ForeignKey("dbo.Servicios", t => t.PaqueteId, cascadeDelete: false)
                .Index(t => t.PaqueteId)
                .Index(t => t.EstratoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Servicios");
            DropForeignKey("dbo.PlanesVentas", "EstratoId", "dbo.Configurations");
            DropIndex("dbo.PlanesVentas", new[] { "EstratoId" });
            DropIndex("dbo.PlanesVentas", new[] { "PaqueteId" });
            DropTable("dbo.PlanesVentas");
        }
    }
}
