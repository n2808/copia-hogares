namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class configurationorder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Configurations", "OrderId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Configurations", "OrderId");
        }
    }
}
