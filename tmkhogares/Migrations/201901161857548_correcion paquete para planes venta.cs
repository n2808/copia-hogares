namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correcionpaqueteparaplanesventa : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Servicios");
            AddForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Paquetes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Paquetes");
            AddForeignKey("dbo.PlanesVentas", "PaqueteId", "dbo.Servicios", "Id", cascadeDelete: false);
        }
    }
}
