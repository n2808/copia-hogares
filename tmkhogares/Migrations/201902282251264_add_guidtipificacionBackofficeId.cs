namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_guidtipificacionBackofficeId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "TipificacioBackofficeId", c => c.Guid());
            CreateIndex("dbo.Gestions", "TipificacioBackofficeId");
            AddForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Configurations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Configurations");
            DropIndex("dbo.Gestions", new[] { "TipificacioBackofficeId" });
            DropColumn("dbo.Gestions", "TipificacioBackofficeId");
        }
    }
}
