namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizarcliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "tipodoc", c => c.String());
            AddColumn("dbo.Clientes", "Documento", c => c.String());
            AddColumn("dbo.Clientes", "Genero", c => c.String());
            AddColumn("dbo.Clientes", "Telefono1", c => c.String());
            AddColumn("dbo.Clientes", "Telefono2", c => c.String());
            AddColumn("dbo.Clientes", "Telefono3", c => c.String());
            AddColumn("dbo.Clientes", "Telefono4", c => c.String());
            AddColumn("dbo.Clientes", "Celular1", c => c.String());
            AddColumn("dbo.Clientes", "Celular2", c => c.String());
            AddColumn("dbo.Clientes", "Celular3", c => c.String());
            AddColumn("dbo.Clientes", "Direccion", c => c.String());
            AddColumn("dbo.Clientes", "Departamento", c => c.String());
            AddColumn("dbo.Clientes", "Ciudad", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "Ciudad");
            DropColumn("dbo.Clientes", "Departamento");
            DropColumn("dbo.Clientes", "Direccion");
            DropColumn("dbo.Clientes", "Celular3");
            DropColumn("dbo.Clientes", "Celular2");
            DropColumn("dbo.Clientes", "Celular1");
            DropColumn("dbo.Clientes", "Telefono4");
            DropColumn("dbo.Clientes", "Telefono3");
            DropColumn("dbo.Clientes", "Telefono2");
            DropColumn("dbo.Clientes", "Telefono1");
            DropColumn("dbo.Clientes", "Genero");
            DropColumn("dbo.Clientes", "Documento");
            DropColumn("dbo.Clientes", "tipodoc");
        }
    }
}
