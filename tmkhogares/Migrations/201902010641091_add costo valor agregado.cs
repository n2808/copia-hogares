namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcostovaloragregado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ValorAgregadoes", "costo", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ValorAgregadoes", "costo");
        }
    }
}
