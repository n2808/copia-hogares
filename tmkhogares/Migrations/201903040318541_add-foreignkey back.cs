namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addforeignkeyback : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Gestions", "seguimiento_AuditadoTipificacion");
            CreateIndex("dbo.Gestions", "seguimiento_BackofficeTipificacion");
            CreateIndex("dbo.Gestions", "Instalacion_EstadoInicial");
            CreateIndex("dbo.Gestions", "Instalacion_EstadoFinal");
            CreateIndex("dbo.Gestions", "Instalacion_RazonAgendamiento");
            AddForeignKey("dbo.Gestions", "Instalacion_EstadoFinal", "dbo.Configurations", "Id");
            AddForeignKey("dbo.Gestions", "Instalacion_EstadoInicial", "dbo.Configurations", "Id");
            AddForeignKey("dbo.Gestions", "Instalacion_RazonAgendamiento", "dbo.Configurations", "Id");
            AddForeignKey("dbo.Gestions", "seguimiento_AuditadoTipificacion", "dbo.Configurations", "Id");
            AddForeignKey("dbo.Gestions", "seguimiento_BackofficeTipificacion", "dbo.Configurations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "seguimiento_BackofficeTipificacion", "dbo.Configurations");
            DropForeignKey("dbo.Gestions", "seguimiento_AuditadoTipificacion", "dbo.Configurations");
            DropForeignKey("dbo.Gestions", "Instalacion_RazonAgendamiento", "dbo.Configurations");
            DropForeignKey("dbo.Gestions", "Instalacion_EstadoInicial", "dbo.Configurations");
            DropForeignKey("dbo.Gestions", "Instalacion_EstadoFinal", "dbo.Configurations");
            DropIndex("dbo.Gestions", new[] { "Instalacion_RazonAgendamiento" });
            DropIndex("dbo.Gestions", new[] { "Instalacion_EstadoFinal" });
            DropIndex("dbo.Gestions", new[] { "Instalacion_EstadoInicial" });
            DropIndex("dbo.Gestions", new[] { "seguimiento_BackofficeTipificacion" });
            DropIndex("dbo.Gestions", new[] { "seguimiento_AuditadoTipificacion" });
        }
    }
}
