// <auto-generated />
namespace tmkhogares.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addinboundraonllamada : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addinboundraonllamada));
        
        string IMigrationMetadata.Id
        {
            get { return "201909262055363_add inbound raon llamada"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
