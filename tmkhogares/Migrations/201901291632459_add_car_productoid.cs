namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_car_productoid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cliente_temporal", "car_ProductoId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cliente_temporal", "car_ProductoId");
        }
    }
}
