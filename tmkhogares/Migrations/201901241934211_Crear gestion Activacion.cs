namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreargestionActivacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GestionActivacions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        fechaVenta = c.DateTime(nullable: false),
                        AgenteId = c.Guid(),
                        GestionId = c.Int(nullable: false),
                        EstadosId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.EstadosId)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: true)
                .Index(t => t.GestionId)
                .Index(t => t.EstadosId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GestionActivacions", "GestionId", "dbo.Gestions");
            DropForeignKey("dbo.GestionActivacions", "EstadosId", "dbo.Configurations");
            DropIndex("dbo.GestionActivacions", new[] { "EstadosId" });
            DropIndex("dbo.GestionActivacions", new[] { "GestionId" });
            DropTable("dbo.GestionActivacions");
        }
    }
}
