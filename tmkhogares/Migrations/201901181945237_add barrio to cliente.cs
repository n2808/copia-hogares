namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addbarriotocliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "Barrio", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "Barrio");
        }
    }
}
