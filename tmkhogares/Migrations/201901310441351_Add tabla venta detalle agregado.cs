namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addtablaventadetalleagregado : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgregadosVentas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GestionId = c.Int(nullable: false),
                        AgregadoId = c.Guid(nullable: false),
                        NombreAgregado = c.String(),
                        cantidad = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: false)
                .Index(t => t.GestionId);
            
            CreateTable(
                "dbo.DetalleVentas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GestionId = c.Int(nullable: false),
                        planId = c.Guid(nullable: false),
                        NombrePlan = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: false)
                .Index(t => t.GestionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleVentas", "GestionId", "dbo.Gestions");
            DropForeignKey("dbo.AgregadosVentas", "GestionId", "dbo.Gestions");
            DropIndex("dbo.DetalleVentas", new[] { "GestionId" });
            DropIndex("dbo.AgregadosVentas", new[] { "GestionId" });
            DropTable("dbo.DetalleVentas");
            DropTable("dbo.AgregadosVentas");
        }
    }
}
