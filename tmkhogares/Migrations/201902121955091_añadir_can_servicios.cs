namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class añadir_can_servicios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Servicios", "NumeroServicios", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Servicios", "NumeroServicios");
        }
    }
}
