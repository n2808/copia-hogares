namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregardatosgestionconint_iddatos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gestions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EstadoId = c.Guid(nullable: false),
                        ClientId = c.Int(nullable: false),
                        captura_Nombre = c.String(),
                        captura_Nombre1 = c.String(),
                        captura_Direccion = c.String(),
                        captura_departamento = c.Guid(),
                        captura_ciudad = c.Guid(),
                        captura_barrio = c.Guid(),
                        captura_telefono1 = c.Guid(),
                        captura_telefono2 = c.Guid(),
                        TipificacionId = c.Guid(),
                        TipificacioBackofficeId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.EstadoId, cascadeDelete: true)
                .ForeignKey("dbo.Tipificacions", t => t.TipificacioBackofficeId)
                .ForeignKey("dbo.Tipificacions", t => t.TipificacionId)
                .Index(t => t.EstadoId)
                .Index(t => t.TipificacionId)
                .Index(t => t.TipificacioBackofficeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestions", "TipificacionId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "TipificacioBackofficeId", "dbo.Tipificacions");
            DropForeignKey("dbo.Gestions", "EstadoId", "dbo.Configurations");
            DropIndex("dbo.Gestions", new[] { "TipificacioBackofficeId" });
            DropIndex("dbo.Gestions", new[] { "TipificacionId" });
            DropIndex("dbo.Gestions", new[] { "EstadoId" });
            DropTable("dbo.Gestions");
        }
    }
}
