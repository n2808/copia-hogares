namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class se_agrega_presenceId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "presence_Id", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "presence_Id");
        }
    }
}
