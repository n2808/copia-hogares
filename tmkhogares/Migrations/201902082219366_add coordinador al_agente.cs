namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcoordinadoral_agente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "CoordinadorId", c => c.Guid());
            CreateIndex("dbo.Users", "CoordinadorId");
            AddForeignKey("dbo.Users", "CoordinadorId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "CoordinadorId", "dbo.Users");
            DropIndex("dbo.Users", new[] { "CoordinadorId" });
            DropColumn("dbo.Users", "CoordinadorId");
        }
    }
}
