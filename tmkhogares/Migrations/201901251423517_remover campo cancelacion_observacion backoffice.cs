namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removercampocancelacion_observacionbackoffice : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Gestions", "seguimiento_cancelacion_Observacion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gestions", "seguimiento_cancelacion_Observacion", c => c.String());
        }
    }
}
