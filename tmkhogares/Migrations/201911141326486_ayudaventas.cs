namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ayudaventas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        DescripcionCorta = c.String(),
                        DescripcionLarga = c.String(),
                        Estado = c.Boolean(nullable: false),
                        TipoCiudadesId = c.Guid(nullable: false),
                        TipoProductoId = c.Guid(nullable: false),
                        Estratos = c.String(),
                        ClienteFijo = c.Boolean(nullable: false),
                        ClienteMovil = c.Boolean(nullable: false),
                        ClienteNuevo = c.Boolean(nullable: false),
                        TipoOfertaId = c.Guid(nullable: false),
                        percentValue = c.Decimal(precision: 18, scale: 2),
                        percentBaVelocity = c.Decimal(precision: 18, scale: 2),
                        percentBaVelocityClienteMovil = c.Decimal(precision: 18, scale: 2),
                        percentValueClienteMovil = c.Decimal(precision: 18, scale: 2),
                        Campana_producto = c.Int(),
                        Campana_Tipo = c.Guid(),
                        TipoPtar = c.Guid(),
                        Ptars = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.TipoCiudadesId, cascadeDelete: false)
                .ForeignKey("dbo.Configurations", t => t.TipoOfertaId, cascadeDelete: false)
                .ForeignKey("dbo.Configurations", t => t.TipoProductoId, cascadeDelete: false)
                .Index(t => t.TipoCiudadesId)
                .Index(t => t.TipoProductoId)
                .Index(t => t.TipoOfertaId);
            
            CreateTable(
                "dbo.CiudadCampanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CiudadId = c.Guid(nullable: false),
                        CiudadNombre = c.String(),
                        CampanaId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: false)
                .ForeignKey("dbo.Cities", t => t.CiudadId, cascadeDelete: false)
                .Index(t => t.CiudadId)
                .Index(t => t.CampanaId);
            
            CreateTable(
                "dbo.CiudadDTHs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CiudadId = c.Guid(nullable: false),
                        CiudadNombre = c.String(),
                        Tv = c.String(),
                        Internet = c.String(),
                        Linea = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CiudadId, cascadeDelete: false)
                .Index(t => t.CiudadId);
            
            CreateTable(
                "dbo.DescuentoSecuencials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampanaId = c.Int(nullable: false),
                        mes = c.Int(nullable: false),
                        porcentajeDescuento = c.Decimal(precision: 18, scale: 2),
                        valorDescuento = c.Decimal(precision: 18, scale: 2),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: false)
                .Index(t => t.CampanaId);
            
            CreateTable(
                "dbo.PaquetesEspecificos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampanaId = c.Int(nullable: false),
                        tv = c.String(),
                        ba = c.String(),
                        Line = c.String(),
                        Cod1 = c.String(),
                        Cod2 = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: false)
                .Index(t => t.CampanaId);
            
            CreateTable(
                "dbo.PlanesEspecificos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TarifaPlenaId = c.Int(nullable: false),
                        CampanaId = c.Int(nullable: false),
                        CEstrato1 = c.String(),
                        CEstrato2 = c.String(),
                        CEstrato3 = c.String(),
                        CEstrato4 = c.String(),
                        CEstrato5 = c.String(),
                        CEstrato6 = c.String(),
                        CSEstrato1 = c.String(),
                        CSEstrato2 = c.String(),
                        CSEstrato3 = c.String(),
                        CSEstrato4 = c.String(),
                        CSEstrato5 = c.String(),
                        CSEstrato6 = c.String(),
                        VEstrato1 = c.Decimal(precision: 18, scale: 2),
                        VEstrato2 = c.Decimal(precision: 18, scale: 2),
                        VEstrato3 = c.Decimal(precision: 18, scale: 2),
                        VEstrato4 = c.Decimal(precision: 18, scale: 2),
                        VEstrato5 = c.Decimal(precision: 18, scale: 2),
                        VEstrato6 = c.Decimal(precision: 18, scale: 2),
                        velocidad = c.Int(),
                        Nombre_plan = c.String(),
                        Codigo_plan = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: false)
                .ForeignKey("dbo.TarifaPlenas", t => t.TarifaPlenaId, cascadeDelete: false)
                .Index(t => t.TarifaPlenaId)
                .Index(t => t.CampanaId);
            
            CreateTable(
                "dbo.TarifaPlenas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        TvId = c.Guid(),
                        LineId = c.Guid(),
                        BaId = c.Guid(),
                        TvName = c.String(),
                        LineName = c.String(),
                        BaName = c.String(),
                        TipoPlanId = c.Guid(),
                        BaVelocity = c.Decimal(precision: 18, scale: 2),
                        CEstrato1 = c.String(),
                        CEstrato2 = c.String(),
                        CEstrato3 = c.String(),
                        CEstrato4 = c.String(),
                        CEstrato5 = c.String(),
                        CEstrato6 = c.String(),
                        CSEstrato1 = c.String(),
                        CSEstrato2 = c.String(),
                        CSEstrato3 = c.String(),
                        CSEstrato4 = c.String(),
                        CSEstrato5 = c.String(),
                        CSEstrato6 = c.String(),
                        VEstrato1 = c.Decimal(precision: 18, scale: 2),
                        VEstrato2 = c.Decimal(precision: 18, scale: 2),
                        VEstrato3 = c.Decimal(precision: 18, scale: 2),
                        VEstrato4 = c.Decimal(precision: 18, scale: 2),
                        VEstrato5 = c.Decimal(precision: 18, scale: 2),
                        VEstrato6 = c.Decimal(precision: 18, scale: 2),
                        Estado = c.Boolean(nullable: false),
                        TarifaEspecial = c.Int(),
                        ProductoId = c.Int(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Configurations", t => t.BaId)
                .ForeignKey("dbo.Configurations", t => t.LineId)
                .ForeignKey("dbo.Productos", t => t.ProductoId)
                .ForeignKey("dbo.Servicios", t => t.TipoPlanId)
                .ForeignKey("dbo.Configurations", t => t.TvId)
                .Index(t => t.TvId)
                .Index(t => t.LineId)
                .Index(t => t.BaId)
                .Index(t => t.TipoPlanId)
                .Index(t => t.ProductoId);
            
            CreateTable(
                "dbo.TarifaAdicionales",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        TiposServiciosAplicaId = c.Guid(nullable: false),
                        ServicioAlqueaplica = c.String(),
                        Codigo = c.String(),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor1 = c.Decimal(precision: 18, scale: 2),
                        valor2 = c.Decimal(precision: 18, scale: 2),
                        valor3 = c.Decimal(precision: 18, scale: 2),
                        valor4 = c.Decimal(precision: 18, scale: 2),
                        valor5 = c.Decimal(precision: 18, scale: 2),
                        valor6 = c.Decimal(precision: 18, scale: 2),
                        valor7 = c.Decimal(precision: 18, scale: 2),
                        productoId = c.Int(),
                        cantidadMaxima = c.Int(nullable: false),
                        orden = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Servicios", t => t.TiposServiciosAplicaId, cascadeDelete: false)
                .Index(t => t.TiposServiciosAplicaId);
            
            CreateTable(
                "dbo.VentaAdicionals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GestionId = c.Int(nullable: false),
                        ValorNormal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorDescuento = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cantidad = c.Int(nullable: false),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        CodigoHogar = c.String(),
                        CodigoSoho = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: false)
                .Index(t => t.GestionId);
            
            CreateTable(
                "dbo.VentaCampanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GestionId = c.Int(nullable: false),
                        NombreCampana = c.String(),
                        Codigo = c.String(),
                        Ba_Velocidad = c.String(),
                        precio = c.String(),
                        plan_seleccionado = c.String(),
                        Descripcion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: false)
                .Index(t => t.GestionId);
            
            CreateTable(
                "dbo.VentaTarifas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GestionId = c.Int(nullable: false),
                        NombrePlan = c.String(),
                        CodigoPlan = c.String(),
                        ValorPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DescripcionPlan = c.String(),
                        Tv = c.String(),
                        Ba = c.String(),
                        Linea = c.String(),
                        CodigoHogar = c.String(),
                        CodigoSoho = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gestions", t => t.GestionId, cascadeDelete: false)
                .Index(t => t.GestionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VentaTarifas", "GestionId", "dbo.Gestions");
            DropForeignKey("dbo.VentaCampanas", "GestionId", "dbo.Gestions");
            DropForeignKey("dbo.VentaAdicionals", "GestionId", "dbo.Gestions");
            DropForeignKey("dbo.TarifaAdicionales", "TiposServiciosAplicaId", "dbo.Servicios");
            DropForeignKey("dbo.PlanesEspecificos", "TarifaPlenaId", "dbo.TarifaPlenas");
            DropForeignKey("dbo.TarifaPlenas", "TvId", "dbo.Configurations");
            DropForeignKey("dbo.TarifaPlenas", "TipoPlanId", "dbo.Servicios");
            DropForeignKey("dbo.TarifaPlenas", "ProductoId", "dbo.Productos");
            DropForeignKey("dbo.TarifaPlenas", "LineId", "dbo.Configurations");
            DropForeignKey("dbo.TarifaPlenas", "BaId", "dbo.Configurations");
            DropForeignKey("dbo.PlanesEspecificos", "CampanaId", "dbo.Campanas");
            DropForeignKey("dbo.PaquetesEspecificos", "CampanaId", "dbo.Campanas");
            DropForeignKey("dbo.DescuentoSecuencials", "CampanaId", "dbo.Campanas");
            DropForeignKey("dbo.CiudadDTHs", "CiudadId", "dbo.Cities");
            DropForeignKey("dbo.CiudadCampanas", "CiudadId", "dbo.Cities");
            DropForeignKey("dbo.CiudadCampanas", "CampanaId", "dbo.Campanas");
            DropForeignKey("dbo.Campanas", "TipoProductoId", "dbo.Configurations");
            DropForeignKey("dbo.Campanas", "TipoOfertaId", "dbo.Configurations");
            DropForeignKey("dbo.Campanas", "TipoCiudadesId", "dbo.Configurations");
            DropIndex("dbo.VentaTarifas", new[] { "GestionId" });
            DropIndex("dbo.VentaCampanas", new[] { "GestionId" });
            DropIndex("dbo.VentaAdicionals", new[] { "GestionId" });
            DropIndex("dbo.TarifaAdicionales", new[] { "TiposServiciosAplicaId" });
            DropIndex("dbo.TarifaPlenas", new[] { "ProductoId" });
            DropIndex("dbo.TarifaPlenas", new[] { "TipoPlanId" });
            DropIndex("dbo.TarifaPlenas", new[] { "BaId" });
            DropIndex("dbo.TarifaPlenas", new[] { "LineId" });
            DropIndex("dbo.TarifaPlenas", new[] { "TvId" });
            DropIndex("dbo.PlanesEspecificos", new[] { "CampanaId" });
            DropIndex("dbo.PlanesEspecificos", new[] { "TarifaPlenaId" });
            DropIndex("dbo.PaquetesEspecificos", new[] { "CampanaId" });
            DropIndex("dbo.DescuentoSecuencials", new[] { "CampanaId" });
            DropIndex("dbo.CiudadDTHs", new[] { "CiudadId" });
            DropIndex("dbo.CiudadCampanas", new[] { "CampanaId" });
            DropIndex("dbo.CiudadCampanas", new[] { "CiudadId" });
            DropIndex("dbo.Campanas", new[] { "TipoOfertaId" });
            DropIndex("dbo.Campanas", new[] { "TipoProductoId" });
            DropIndex("dbo.Campanas", new[] { "TipoCiudadesId" });
            DropTable("dbo.VentaTarifas");
            DropTable("dbo.VentaCampanas");
            DropTable("dbo.VentaAdicionals");
            DropTable("dbo.TarifaAdicionales");
            DropTable("dbo.TarifaPlenas");
            DropTable("dbo.PlanesEspecificos");
            DropTable("dbo.PaquetesEspecificos");
            DropTable("dbo.DescuentoSecuencials");
            DropTable("dbo.CiudadDTHs");
            DropTable("dbo.CiudadCampanas");
            DropTable("dbo.Campanas");
        }
    }
}
