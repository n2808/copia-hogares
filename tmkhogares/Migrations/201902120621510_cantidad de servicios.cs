namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cantidaddeservicios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "Venta_CantidadDeServicios", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "Venta_CantidadDeServicios");
        }
    }
}
