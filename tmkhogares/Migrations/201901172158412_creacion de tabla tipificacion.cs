namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciondetablatipificacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tipificacions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Nombre2 = c.String(),
                        Nombre3 = c.String(),
                        Descripcion = c.String(),
                        Estado = c.Boolean(nullable: false),
                        GestionEfectiva = c.Boolean(nullable: false),
                        order = c.Int(nullable: false),
                        codigo = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tipificacions");
        }
    }
}
