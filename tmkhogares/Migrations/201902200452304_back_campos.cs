namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class back_campos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "back_campo1", c => c.String());
            AddColumn("dbo.Gestions", "back_campo2", c => c.String());
            AddColumn("dbo.Gestions", "back_campo3", c => c.String());
            AddColumn("dbo.Gestions", "back_campo4", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "back_campo4");
            DropColumn("dbo.Gestions", "back_campo3");
            DropColumn("dbo.Gestions", "back_campo2");
            DropColumn("dbo.Gestions", "back_campo1");
        }
    }
}
