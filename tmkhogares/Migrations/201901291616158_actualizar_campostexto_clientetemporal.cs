namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizar_campostexto_clientetemporal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cliente_temporal", "CUENTA_CO_ID", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_1", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_2", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_3", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "CELULAR_1", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "CELULAR_2", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "CFM_PLAN", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "CFM_PAQ", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "ARPU", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "TARIFA", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "RENTA_RR", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "RENTA_BASICOS", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "RENTA_ADICIONALES", c => c.String());
            AlterColumn("dbo.Cliente_temporal", "RENTA_TOTAL_IVA", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cliente_temporal", "RENTA_TOTAL_IVA", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_ADICIONALES", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_BASICOS", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "RENTA_RR", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TARIFA", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "ARPU", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CFM_PAQ", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CFM_PLAN", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CELULAR_2", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CELULAR_1", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_3", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_2", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "TELEFONO_1", c => c.Int());
            AlterColumn("dbo.Cliente_temporal", "CUENTA_CO_ID", c => c.Int());
        }
    }
}
