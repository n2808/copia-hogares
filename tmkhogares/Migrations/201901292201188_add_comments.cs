namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_comments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cliente_temporal", "COMMENTS", c => c.String());
            AddColumn("dbo.Cliente_temporal", "ClI_FILA", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cliente_temporal", "ClI_FILA");
            DropColumn("dbo.Cliente_temporal", "COMMENTS");
        }
    }
}
