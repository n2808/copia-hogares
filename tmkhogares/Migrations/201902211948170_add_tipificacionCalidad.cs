namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tipificacionCalidad : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "AuditadoPor", c => c.Guid());
            AddColumn("dbo.Gestions", "AuditadoEn", c => c.DateTime());
            AddColumn("dbo.Gestions", "seguimiento_AuditadoTipificacion", c => c.Guid());
            AddColumn("dbo.Gestions", "seguimiento_BackofficeTipificacion", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "seguimiento_BackofficeTipificacion");
            DropColumn("dbo.Gestions", "seguimiento_AuditadoTipificacion");
            DropColumn("dbo.Gestions", "AuditadoEn");
            DropColumn("dbo.Gestions", "AuditadoPor");
        }
    }
}
