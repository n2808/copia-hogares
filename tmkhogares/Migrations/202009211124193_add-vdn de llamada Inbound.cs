namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addvdndellamadaInbound : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "vdnLlamadaInbound", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "vdnLlamadaInbound");
        }
    }
}
