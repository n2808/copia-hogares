// <auto-generated />
namespace tmkhogares.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class seadicionalatabladeusuariosyconfiguraciondevalidador : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(seadicionalatabladeusuariosyconfiguraciondevalidador));
        
        string IMigrationMetadata.Id
        {
            get { return "201901222023379_se adiciona la tabla de usuarios y configuracion de validador"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
