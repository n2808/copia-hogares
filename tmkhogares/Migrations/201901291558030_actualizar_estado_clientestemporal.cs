namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizar_estado_clientestemporal : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cliente_temporal", "EstadoId", "dbo.Configurations");
            DropIndex("dbo.Cliente_temporal", new[] { "EstadoId" });
            DropColumn("dbo.Cliente_temporal", "EstadoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cliente_temporal", "EstadoId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Cliente_temporal", "EstadoId");
            AddForeignKey("dbo.Cliente_temporal", "EstadoId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
    }
}
