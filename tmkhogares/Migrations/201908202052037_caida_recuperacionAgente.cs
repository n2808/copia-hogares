namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class caida_recuperacionAgente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gestions", "CaidaRecuperacionAgenteEn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gestions", "CaidaRecuperacionAgenteEn");
        }
    }
}
