namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addproductosubproductobatvalcotizador : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DetalleVentas", "CategoriaProducto", c => c.String());
            AddColumn("dbo.DetalleVentas", "SubCategoriaProducto", c => c.String());
            AddColumn("dbo.ProductosDetalleVentas", "Ba", c => c.String());
            AddColumn("dbo.ProductosDetalleVentas", "Tv", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductosDetalleVentas", "Tv");
            DropColumn("dbo.ProductosDetalleVentas", "Ba");
            DropColumn("dbo.DetalleVentas", "SubCategoriaProducto");
            DropColumn("dbo.DetalleVentas", "CategoriaProducto");
        }
    }
}
