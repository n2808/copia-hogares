﻿using Core.Models.Gestion;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
 
    public class GestionVM
    {
        public Gestion Gestion { get; set;  }
        public Cliente cliente { get; set; }
        public User Asesor { get; set; }

    }

    public class BackofficeGestionVM
    {
        [Required]
        public int Id { get; set; }
        
        public Guid? EstadoId { get; set; }
        [Required]
        public string seguimiento_OT { get; set; }
        public string seguimiento_obs { get; set; }
        public Guid? TipificacionBackId { get; set; }
        public DateTime? seguimiento_prox_fecha_inst { get; set; }
        // datos para digitacion.
        public string digitacion_NRegistros { get; set; }
        public string digitacion_contrato { get; set; }
        public string digitacion_cuenta { get; set; }
        public string digitacion_franja { get; set; }
        // datos para la instalacion 
        public Guid? Instalacion_EstadoInicial { get; set; }
        public Guid? Instalacion_estadoFinal { get; set; }
        public Guid? Instalacion_RazonAgendamiento { get; set; }
        public DateTime? Instalacion_FechaReprogramacion { get; set; }

    }



}