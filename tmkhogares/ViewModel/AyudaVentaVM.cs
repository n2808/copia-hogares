﻿using Core.Models.Gestion;
using Core.Models.Sales;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
 
    public class AyudaVentaVM
    {
        public List<TarifaPlena> TarifasPlenas { get; set;  }
        public List<Campana> Campanas { get; set; }
        public List<CiudadCampana> CiudadesCampanas { get; set; }
        public List<PlanesEspecificos> PlanesEspecificos { get; set; }
        public List<PaquetesEspecificos> PaquetesEspecificos { get; set; }
        public List<DescuentoSecuencial> DescuentoSecuencial { get; set; }
        public List<CiudadDTH> ciudadesDTH { get; set; }
        public List<TarifaAdicionales> TarifaAdicionales { get; set; }
        public List<Core.Models.configuration.Configuration> listaTv { get; set; }
        public List<Core.Models.configuration.Configuration> listaBa { get; set; }
        public List<Core.Models.configuration.Configuration> listaLinea { get; set; }
        

    }


}