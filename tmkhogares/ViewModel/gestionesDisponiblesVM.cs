﻿using System;

namespace Core.ViewModel
{
    public class gestionesDisponiblesVM
    {


        public DateTime fechaVenta { get; set; }
        public Guid? AgenteId { get; set; }
        public string Login { get; set; }
        public string Id { get; set; }
        public int GestionId { get; set; }

        public int GestionActivacionId { get; set;  }
        public string Estado { get; set; }
        public string Campana { get; set; }

        public string Producto { get; set; }
        public string vdn { get; set; }
        public string TipoLLamada { get; set; }


        public string Plataforma { get; set; }

    }

    public class solicitarGestionVM
    {
        public int asigno { get; set; }
    }
}