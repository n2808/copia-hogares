﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Oferta;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    public class OfertaProductosController : Controller
    {
        private Contexto db = new Contexto();







        // GET: OfertaProductos
        public ActionResult Index()
        {
            var productosOfrecidos = db.ProductosOfrecidos.Include(p => p.TipoVenta);
            return View(productosOfrecidos.ToList());
        }

        // GET: OfertaProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductosOfrecidos productosOfrecidos = db.ProductosOfrecidos.Find(id);
            if (productosOfrecidos == null)
            {
                return HttpNotFound();
            }
            return View(productosOfrecidos);
        }

        // GET: OfertaProductos/Create
        public ActionResult Create()
        {
            ViewBag.TipoVentaId = new SelectList(db.Configurations.Where( c => c.CategoriesId == _Configuraciones.PRODUCTOS_TipoDeVenta), "Id", "Name");
            return View();
        }

        // POST: OfertaProductos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,TipoVentaId,EsProductoPrincipal,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ProductosOfrecidos productosOfrecidos)
        {
            if (ModelState.IsValid)
            {
                db.ProductosOfrecidos.Add(productosOfrecidos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoVentaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.PRODUCTOS_TipoDeVenta), "Id", "Name", productosOfrecidos.TipoVentaId);
            return View(productosOfrecidos);
        }

        // GET: OfertaProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductosOfrecidos productosOfrecidos = db.ProductosOfrecidos.Find(id);
            if (productosOfrecidos == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoVentaId = new SelectList(db.Configurations, "Id", "Name", productosOfrecidos.TipoVentaId);
            return View(productosOfrecidos);
        }

        // POST: OfertaProductos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,TipoVentaId,EsProductoPrincipal,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ProductosOfrecidos productosOfrecidos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productosOfrecidos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoVentaId = new SelectList(db.Configurations, "Id", "Name", productosOfrecidos.TipoVentaId);
            return View(productosOfrecidos);
        }

        //// GET: OfertaProductos/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ProductosOfrecidos productosOfrecidos = db.ProductosOfrecidos.Find(id);
        //    if (productosOfrecidos == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(productosOfrecidos);
        //}

        //// POST: OfertaProductos/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ProductosOfrecidos productosOfrecidos = db.ProductosOfrecidos.Find(id);
        //    db.ProductosOfrecidos.Remove(productosOfrecidos);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
