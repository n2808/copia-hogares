﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using Core.Models.Security;
using tmkhogares.Models;
using tmkhogares.ViewModel;


namespace tmkhogares.Controllers
{

    [CustomAuthorize(Roles = "Administrador")]
    public class TarifaPlenaController : Controller
    {
        private Contexto db = new Contexto();

        // GET: TarifaPlena
        // Producto pordefecto 2
        public ActionResult Index(int? ProductoId = 2)
        {
            var tarifaPlena = db.TarifaPlena.Include(t => t.Ba).Include(t => t.Line).Include(t => t.Producto).Include(t => t.TipoPlan).Include(t => t.Tv).Where(c => c.ProductoId == ProductoId);
            return View(tarifaPlena.ToList());
        }

        // GET: TarifaPlena/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TarifaPlena tarifaPlena = db.TarifaPlena.Find(id);
            if (tarifaPlena == null)
            {
                return HttpNotFound();
            }
            return View(tarifaPlena);
        }

        // GET: TarifaPlena/Create
        public ActionResult Create(int? ProductoId = 2)
        {
            ViewBag.BaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Megasdevelocidad && c.IntId == ProductoId), "Id", "Name");
            ViewBag.LineId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.planesMinutos && c.IntId == ProductoId), "Id", "Name");
            ViewBag.ProductoId = new SelectList(db.Productos.Where( j => j.Id == ProductoId), "Id", "Nombre");
            ViewBag.TipoPlanId = new SelectList(db.Servicio, "Id", "Nombre");
            ViewBag.TvId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.PlanesdeTv && c.IntId == ProductoId), "Id", "Name");
            
            return View();
        }

        // POST: TarifaPlena/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,TvId,LineId,BaId,TvName,LineName,BaName,TipoPlanId,BaVelocity,CEstrato1,CEstrato2,CEstrato3,CEstrato4,CEstrato5,CEstrato6,CSEstrato1,CSEstrato2,CSEstrato3,CSEstrato4,CSEstrato5,CSEstrato6,VEstrato1,VEstrato2,VEstrato3,VEstrato4,VEstrato5,VEstrato6,Estado,TarifaEspecial,ProductoId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] TarifaPlena tarifaPlena)
        {
            if (ModelState.IsValid)
            {
                db.TarifaPlena.Add(tarifaPlena);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Megasdevelocidad && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.BaId);
            ViewBag.LineId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.planesMinutos && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.LineId);
            ViewBag.ProductoId = new SelectList(db.Productos.Where(c => c.Id == tarifaPlena.ProductoId), "Id", "Nombre", tarifaPlena.ProductoId);
            ViewBag.TipoPlanId = new SelectList(db.Servicio, "Id", "Nombre", tarifaPlena.TipoPlanId);
            ViewBag.TvId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.PlanesdeTv && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.TvId);
            return View(tarifaPlena);
        }

        // GET: TarifaPlena/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TarifaPlena tarifaPlena = db.TarifaPlena.Find(id);
            if (tarifaPlena == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Megasdevelocidad && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.BaId);
            ViewBag.LineId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.planesMinutos && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.LineId);
            ViewBag.ProductoId = new SelectList(db.Productos.Where(c=> c.Id == tarifaPlena.ProductoId), "Id", "Nombre", tarifaPlena.ProductoId);
            ViewBag.TipoPlanId = new SelectList(db.Servicio, "Id", "Nombre", tarifaPlena.TipoPlanId);
            ViewBag.TvId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.PlanesdeTv && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.TvId);
            return View(tarifaPlena);
        }

        // POST: TarifaPlena/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,TvId,LineId,BaId,TvName,LineName,BaName,TipoPlanId,BaVelocity,CEstrato1,CEstrato2,CEstrato3,CEstrato4,CEstrato5,CEstrato6,CSEstrato1,CSEstrato2,CSEstrato3,CSEstrato4,CSEstrato5,CSEstrato6,VEstrato1,VEstrato2,VEstrato3,VEstrato4,VEstrato5,VEstrato6,Estado,TarifaEspecial,ProductoId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] TarifaPlena tarifaPlena)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tarifaPlena).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Megasdevelocidad && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.BaId);
            ViewBag.LineId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.planesMinutos && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.LineId);
            ViewBag.ProductoId = new SelectList(db.Productos.Where(c => c.Id == tarifaPlena.ProductoId), "Id", "Nombre", tarifaPlena.ProductoId);
            ViewBag.TipoPlanId = new SelectList(db.Servicio, "Id", "Nombre", tarifaPlena.TipoPlanId);
            ViewBag.TvId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.PlanesdeTv && c.IntId == tarifaPlena.ProductoId), "Id", "Name", tarifaPlena.TvId);
            return View(tarifaPlena);
        }

        // GET: TarifaPlena/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TarifaPlena tarifaPlena = db.TarifaPlena.Find(id);
            if (tarifaPlena == null)
            {
                return HttpNotFound();
            }
            return View(tarifaPlena);
        }

        // POST: TarifaPlena/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TarifaPlena tarifaPlena = db.TarifaPlena.Find(id);
            db.TarifaPlena.Remove(tarifaPlena);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
