﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using Core.Models.User;
using Core.Models.Gestion;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    public class ProgramacionController : Controller
    {
        private Contexto db = new Contexto();
        // GET: gestion de misventas
        public ActionResult Programacion(string id = "")
        {

            Core.Models.User.User User = db.Usuario.Where(c => c.login == id && c.Status == true).FirstOrDefault();
            ViewBag.agent = id;
            ViewBag.Mensaje = User?.BlogNotas != null ? User.BlogNotas:"";
            List<Gestion> gestion = null;
            if (User != null)
            {


                gestion = db.Gestion.Include(c => c.Estado)
                .Where(c =>
                _EstadosGestion.LEstadosRecuperarAgente.Contains(c.EstadoId)
                && c.cantadaPor == User.Id
                ).ToList()
               .Where(c => c.CaidaRecuperacionAgenteEn.Value.AddDays(1) >= DateTime.Now).ToList();
            }
            // ViewBag.gestion = gestion;
            return View(gestion);
        }


        public JsonResult MiBloc(string id = "",string Mensaje ="")
        {
            
            Core.Models.User.User User = db.Usuario.Where(c => c.login == id && c.Status == true).FirstOrDefault() ;
            User.BlogNotas = Mensaje;
            db.Entry(User).State = EntityState.Modified;
            db.SaveChanges();

            return Json(new { codigo = 0});
        }


    }
}