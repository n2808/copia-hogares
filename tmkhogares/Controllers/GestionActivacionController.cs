﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Gestion;
using tmkhogares.Models;
using tmkhogares.ViewModel;
using Core.Models.Security;
using Core.ViewModel;
namespace tmkhogares.Controllers
{
    public class GestionActivacionController : Controller
    {
        private Contexto db = new Contexto();

        /*------------------------------------------------------*
        * VENTAS POR PORAUDITAR
        *------------------------------------------------------*/

        // GET: PendientesDigitar
        [CustomAuthorize(Roles = "Administrador,Calidad")]
        public ActionResult PorAuditar(string mensaje = null)
        {

            ViewBag.titulo = "Ventas por Auditar";
            ViewBag.Estado = _EstadosGestion.VentaCantada;
            ViewBag.Tipo = 1;
            ViewBag.mensaje = mensaje;
            ViewBag._Panel = "Calidad";
            ViewBag._Active = "porAuditar";
            ViewBag.ventas = db.GestionActivacion.Where(c => c.EstadosId == _EstadosGestion.VentaCantada).Count();
            var gestionActivacion = db.GestionActivacion
                .Include(g => g.Estado)
                .Include(g => g.Gestion)
                .Where(c => c.EstadosId == _EstadosGestion.VentaCantada);


            ViewBag.GestionVdns = db.CampanasVdns.ToList();

            return View("~/Views/GestionActivacion/Index.cshtml", gestionActivacion.ToList());
        }



        // GET: PendientesDigitar
        [CustomAuthorize(Roles = "Administrador,Validador_ClaroColombia")]
        public ActionResult PorAuditarTotal(string mensaje = null)
        {
            ViewBag.titulo = "Ventas por Auditar";
            ViewBag.Estado = _EstadosGestion.VentaCantada;
            ViewBag.Tipo = 1;
            ViewBag.mensaje = mensaje;
            ViewBag._Panel = "Calidad";
            ViewBag._Active = "porAuditar";
            ViewBag.ventas = db.GestionActivacion.Where(c => c.EstadosId == _EstadosGestion.VentaCantada).Count();

            // solicitar gestion
            Guid idGuid = (Guid)SessionPersister.Id;
            var idConsultar = db.Usuario.Find(idGuid).Phone3 != null ? db.Usuario.Find(idGuid).Phone3 : "0";
            int Id =   idConsultar != null ? Int32.Parse(idConsultar) : 0;
            List<gestionesDisponiblesVM> Gestiones = db.Database.SqlQuery<gestionesDisponiblesVM>("Listar_GestionesDisponibles @idguid = {0},@id={1}", idGuid, Id ).ToList();

            ViewBag.GestionVdns = db.CampanasVdns.ToList();
            return View("~/Views/GestionActivacion/PorAuditarTotal.cshtml", Gestiones);
        }





        /*------------------------------------------------------*
        * VENTAS POR DIGITAR
        *------------------------------------------------------*/

        // GET: PendientesDigitar
        [CustomAuthorize(Roles = "BackOffice,Administrador")]
        public ActionResult PendientesDigitar(string mensaje = null)
        {
            ViewBag.titulo = "Ventas por digitar";
            ViewBag.Estado = _EstadosGestion.VentaDigitada;
            ViewBag.mensaje = mensaje;
            ViewBag.ventas = db.GestionActivacion.Where(c => c.EstadosId == _EstadosGestion.AuditadaOk && c.AgenteId == null).Count();
            var gestionActivacion = db.GestionActivacion
                .Include(g => g.Estado)
                .Include(g => g.Gestion)
                .Where(c => c.EstadosId == _EstadosGestion.AuditadaOk
                //&& SessionPersister.Id == c.AgenteId
                //&& c.AgenteId != null
                );
            ViewBag.GestionVdns = db.CampanasVdns.ToList();
            return View("~/Views/GestionActivacion/Index.cshtml", gestionActivacion.ToList());
        }

        /*------------------------------------------------------*
        * VENTAS PENDIENTES GESTIONAR
        *------------------------------------------------------*/
        [CustomAuthorize(Roles = "BackOffice,Administrador")]
        public ActionResult PendientesInstalar()
        {
            ViewBag.titulo = "Ventas por Instalar";
            ViewBag.GestionVdns = db.CampanasVdns.ToList();
            var gestionActivacion = db.GestionActivacion.Include(g => g.Estado).Include(g => g.Gestion).Where(c => c.EstadosId == _EstadosGestion.VentaDigitada);
            return View("~/Views/GestionActivacion/Index.cshtml",gestionActivacion.ToList());
        }

        /*------------------------------------------------------*
        * VENTAS  SOPORTADAS
        *------------------------------------------------------*/
        [CustomAuthorize(Roles = "BackOffice,Administrador")]
        public ActionResult Soportadas()
        {
            ViewBag.titulo = "Ventas Soportadas";
            ViewBag.GestionVdns = db.CampanasVdns.ToList();
            var gestionActivacion = db.GestionActivacion.Include(g => g.Estado).Include(g => g.Gestion).Where(c => c.EstadosId == _EstadosGestion.VentaSoportada);
            return View("~/Views/GestionActivacion/Index.cshtml", gestionActivacion.ToList());
        }

        /*------------------------------------------------------*
        * SOLICITAR UNA NUEVA GESTION
        *------------------------------------------------------*/

        [CustomAuthorize(Roles = "BackOffice,Administrador")]
        public ActionResult solicitarGestion()
        {
            GestionActivacion gs = db.GestionActivacion
                .Where(c => c.AgenteId == null
                && c.EstadosId == _EstadosGestion.VentaCantada
                )
                .FirstOrDefault();

            if (gs == null)
            {
                return RedirectToAction("PendientesDigitar", new { mensaje = "no hay gestiones para activar" });
            }
            else
            {
                gs.AgenteId = SessionPersister.Id;
                db.Entry(gs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("PendientesDigitar");
            }
            
           
        }        
        
        /*------------------------------------------------------*
        * SOLICITAR UNA NUEVA GESTION EN TODO CLARO COLOMBIA
        *------------------------------------------------------*/

        [CustomAuthorize(Roles = "BackOffice,Validador_ClaroColombia")]
        public ActionResult solicitarGestionAuditado()
        {


            // solicitar gestion
            Guid idGuid = (Guid)SessionPersister.Id;
            var idConsultar = db.Usuario.Find(idGuid).Phone3 != null ? db.Usuario.Find(idGuid).Phone3 : "0";
            int Id = idConsultar != null ? Int32.Parse(idConsultar) : 0;


            solicitarGestionVM Gestiones = db.Database.SqlQuery<solicitarGestionVM>("Sp_SolicitarGestion @idguid = {0},@id={1}", idGuid, Id).FirstOrDefault();
            if (Gestiones == null || Gestiones?.asigno != 1)
            {
                return RedirectToAction("PorAuditarTotal", new { mensaje = "no hay gestiones para activar" });
            }
            else
            {
                return RedirectToAction("PorAuditarTotal");
            }
            
           
        }





        /*------------------------------------------------------*
        * 
        * TRABAJO DE BACKOFFICE GESTION DE TIPIFICACION
        * 
        *------------------------------------------------------*/
        [CustomAuthorize(Roles = "BackOffice,Administrador,Validador_ClaroColombia")]
        public ActionResult Gestionar(int? id = null )
        {


            
            GestionActivacion gsActivacion = db.GestionActivacion.Find(id);
            List<Guid> estadosTipificar = new List<Guid>();
            GestionVM venta;
            Gestion gestion = new Gestion();





            /*--------| TRABAJANDO CON LA GESTION. |----------*/
            if (gsActivacion != null) {

                switch (gsActivacion.EstadosId.ToString().ToLower())
                {


                    /*------------------------------------------------------*
                     *   1.VENTAS POR AUDITAR
                    *------------------------------------------------------*/
                    case "e820e674-e7cd-4087-bcd2-e68a0281511d":
                        ViewBag.titulo = "Ventas por Auditar";
                        ViewBag.Tipo = 0;
                        estadosTipificar = new List<Guid>() { _EstadosGestion.AuditadaOk, _EstadosGestion.CaidaTextoLegal };
                        ViewBag.Tipificacion = db.Configurations
                            .Where(c => c.Status == true && c.CategoriesId == _Configuraciones.RechazosCalidad)
                            .OrderBy(c => c.OrderId)
                            .ToList();
                        break;

                    /*------------------------------------------------------*
                     *   2.VENTAS POR DIGITAR
                    *------------------------------------------------------*/
                    case "80245538-98ea-4c52-86d3-8468aa798e60":
                        ViewBag.titulo = "Ventas Auditadas";
                        ViewBag.Tipo = 1;
                        estadosTipificar = new List<Guid>() { _EstadosGestion.CaidaDigitacion, _EstadosGestion.VentaDigitada/*,_EstadosGestion.CaidaDigitacionAgente*/};
                        ViewBag.Tipificacion = db.Configurations
                                                .Where(c => c.Status == true && c.CategoriesId == _Configuraciones.RechazosDigitacion)
                                                .OrderBy(c => c.OrderId)
                                                .ToList();
                        break;
                    /*------------------------------------------------------*
                     *   3.VENTAS POR INSTALAR
                    *------------------------------------------------------*/

                    case "785e5148-50f7-4c43-a7f3-23a8c9c68f0b":
                        ViewBag.titulo = "Ventas por Instalar";
                        ViewBag.Tipo = 2;
                        estadosTipificar = new List<Guid>() { _EstadosGestion.VentaSoportada, _EstadosGestion.CaidaInstalacion };
                        ViewBag.EstadoI = db.Configurations
                                                .Where(c => c.Status == true && c.CategoriesId == _Configuraciones.Instalacion_EstadoInicial)
                                                .OrderBy(c => c.OrderId)
                                                .ToList();
                        ViewBag.EstadoF = db.Configurations
                                                .Where(c => c.Status == true && c.CategoriesId == _Configuraciones.instalacion_EstadoFinal)
                                                .OrderBy(c => c.OrderId)
                                                .ToList();
                        ViewBag.RazonAgendamiento = db.Configurations
                                                .Where(c => c.Status == true && c.CategoriesId == _Configuraciones.Instalacion_RazonAgendamiento)
                                                .OrderBy(c => c.OrderId)
                                                .ToList();
                        ViewBag.Tipificacion = db.Configurations
                                                .Where(c => c.Status == true && c.CategoriesId == _Configuraciones.RechazosInstalacion)
                                                .OrderBy(c => c.OrderId)
                                                .ToList();


                        break;

                    /*------------------------------------------------------*
                     *   4.VENTAS POR LEGALIZAR
                    *------------------------------------------------------*/

                    case "1046e22e-abef-4ea2-beda-8cae01620534":
                        ViewBag.titulo = "Ventas por Legalizar";
                        ViewBag.Tipo = 3;
                        estadosTipificar = new List<Guid>() { _EstadosGestion.VentaLegalizada, _EstadosGestion.CaidaLegalizacion};
                        break;
                    default:
                        break;
                }

                    gestion = db.Gestion
                   .Include(c => c.Estado)
                   .Include(c => c.captura_ciudad)
                   .Include(c => c.captura_ciudad.State)
                   .Include(c => c.Tipificacion)
                   .Include(c => c.cantadaPor__Agente)
                   .Include(c => c.AuditadoPor_Agente)
                   .Include(c => c.DigitadaPor_Agente)
                   .Include(c => c.InstaladaActulizadoPor_Agente)
                   .Include(c => c.soportadaPor_Agente)
                   .Include(c => c.seguimiento_AuditadoTipificacion_llave)
                   .Include(c => c.seguimiento_BackofficeTipificacion_llave)
                   .Include(c => c.Instalacion_EstadoFinal_llave)
                   .Include(c => c.Instalacion_EstadoInicial_llave)
                   .Include(c => c.Instalacion_RazonAgendamiento_llave)
                   .Where(c => c.Id == gsActivacion.GestionId).FirstOrDefault();


                ViewBag.GestionVdns = db.CampanasVdns.Where(c => c.Id.ToString() == gestion.vdnLlamada).FirstOrDefault()?.NombreMostrarCanal;

                Cliente cliente = db.cliente
                    .Include(j => j.Carga)
                    .Where(c => c.Id == gestion.ClientId)
                    .FirstOrDefault();
                venta = new GestionVM()
                {
                    Gestion = gestion,
                    cliente = cliente,
                    Asesor = db.Usuario.Include(c => c.Coordinador).Where(c => c.Id == gestion.cantadaPor).FirstOrDefault()
                };
            }
            else { venta = null; };
            

            ViewBag.Id = id;
            ViewBag.EstadoId = new SelectList(db.Configurations.Where(c => estadosTipificar.Contains(c.Id)), "Id", "Name");
            ViewBag.productos = db.Productos.ToList();
            ViewBag.ItemsVenta = db.DetalleVenta.Where(c => c.GestionId == gestion.Id).ToList();
            return View("~/Views/GestionActivacion/Details.cshtml", venta);
        }


        /*------------------------------------------------------*
        * GESTIONAR VENTA POR DIGITAR.
        *------------------------------------------------------*/


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Gestionar(BackofficeGestionVM gestionActivacion)
        {
            List<CustoErrors> Errors = new List<CustoErrors>();
            List<Guid?> EstadosError = new List<Guid?>()
            {
                _EstadosGestion.CaidaDigitacion,
                _EstadosGestion.CaidaValidacion,
                _EstadosGestion.CaidaInstalacion,
                _EstadosGestion.CaidaLegalizacion,
                _EstadosGestion.RechazadaAgente
            };



            if (gestionActivacion.EstadoId == null)
            {
                Errors.Add(new CustoErrors() { Name = "", Message = "Por favor seleccione una tipificación", Type = CustomErrorType.Simple });
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(Errors);
            }

            /*------------------------------------------
             * VALIDACIONES PARA EL FILTRO DE CALIDAD.
             *--------------------------------------*/
            if (gestionActivacion.EstadoId == _EstadosGestion.CaidaTextoLegal)
            {
                if (String.IsNullOrEmpty(gestionActivacion.seguimiento_obs)) Errors.Add(new CustoErrors() { Name = "", Message = "Por favor ingrese la observación del rechazo", Type = CustomErrorType.Simple });
            }
            if(gestionActivacion.EstadoId == _EstadosGestion.VentaDigitada)
            {
                if (String.IsNullOrEmpty(gestionActivacion.seguimiento_OT ) ) Errors.Add(new CustoErrors() { Name = "", Message = "El código de seguimiento es obligatorio", Type = CustomErrorType.Simple });
                if (gestionActivacion.seguimiento_prox_fecha_inst == null) Errors.Add(new CustoErrors() { Name = "", Message = "La fecha es obligatoria", Type = CustomErrorType.Simple });

            }
            if(EstadosError.Contains(gestionActivacion.EstadoId))
            {
                if (String.IsNullOrEmpty(gestionActivacion.seguimiento_obs)) Errors.Add(new CustoErrors() { Name = "", Message = "Por favor ingrese la razon de la caida de la venta", Type = CustomErrorType.Simple});
            }

            /*------------------------------------------
             * SE GUARDA LA INSTALACION DE LA VENTA.
             *--------------------------------------*/

            if (Errors.Count == 0)
            {
                GestionActivacion gsActivacion = db.GestionActivacion.Find(gestionActivacion.Id);
                if(gsActivacion == null )
                {
                    Errors.Add(new CustoErrors() {  Message = "Error De validacion de formulario", Type = CustomErrorType.Simple });

                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json(Errors);
                }

                Gestion gs = db.Gestion.Find(gsActivacion.GestionId);

                // update de gestion activacion.    
                using(Contexto db1  = new Contexto()){
                    // VENTA AUDITADA
                    if(gestionActivacion.EstadoId == _EstadosGestion.AuditadaOk)
                    {
                        gsActivacion.EstadosId = _EstadosGestion.AuditadaOk;
                        db1.Entry(gsActivacion).State = EntityState.Modified;
                        db1.SaveChanges();
                    }
                    //VENTA DIGITADA
                    else if(gestionActivacion.EstadoId == _EstadosGestion.VentaDigitada)
                    {
                        gsActivacion.EstadosId = _EstadosGestion.VentaDigitada;
                        db1.Entry(gsActivacion).State = EntityState.Modified;
                        db1.SaveChanges();
                    }
                    // VENTA INSTALADA
                    else if (gestionActivacion.EstadoId == _EstadosGestion.VentaSoportada)
                    {
                        gsActivacion.EstadosId = _EstadosGestion.VentaSoportada;
                        db1.Entry(gsActivacion).State = EntityState.Modified;
                        db1.SaveChanges();
                    }

                    else
                    {
                        db.GestionActivacion.Remove(gsActivacion);
                        db.SaveChanges();
                    }


                }

                using (Contexto db2 = new Contexto())
                {
                    //VENTA AUDITADA
                    if (gestionActivacion.EstadoId == _EstadosGestion.AuditadaOk || gestionActivacion.EstadoId == _EstadosGestion.RechazadaAgente || gestionActivacion.EstadoId == _EstadosGestion.CaidaTextoLegal)
                    {
                        gs.AuditadoPor = SessionPersister.Id;
                        gs.AuditadoEn = DateTime.Now;
                        gs.back_campo1 = gestionActivacion.seguimiento_obs;
                        gs.seguimiento_AuditadoTipificacion = gestionActivacion.TipificacionBackId;
                        gs.CaidaRecuperacionAgenteEn = DateTime.Now;
                    }
                    //VENTA DIGITADA
                    if (gestionActivacion.EstadoId == _EstadosGestion.VentaDigitada || gestionActivacion.EstadoId == _EstadosGestion.CaidaDigitacion /*|| gestionActivacion.EstadoId == _EstadosGestion.CaidaDigitacionAgente*/ )
                    {
                        gs.seguimiento_OT = gestionActivacion.seguimiento_OT;
                        gs.seguimiento_obs = gestionActivacion.seguimiento_obs;
                        gs.seguimiento_prox_fecha_inst = gestionActivacion.seguimiento_prox_fecha_inst;
                        gs.Digitacion_RegistroNumero = gestionActivacion.digitacion_NRegistros;
                        gs.Digitacion_Contrato = gestionActivacion.digitacion_contrato;
                        gs.Digitacion_Cuenta = gestionActivacion.digitacion_cuenta;
                        gs.Franja = gestionActivacion.digitacion_franja;
                        gs.seguimiento_BackofficeTipificacion = gestionActivacion.TipificacionBackId;
                        gs.DigitadaEn = DateTime.Now;
                        gs.DigitadaPor = SessionPersister.Id;
                        gs.CaidaRecuperacionAgenteEn = DateTime.Now;
                    }
                    ////VENTA INSTALADA
                    //if (gestionActivacion.EstadoId == _EstadosGestion.CaidaInstalacion || gestionActivacion.EstadoId == _EstadosGestion.VentaSoportada)
                    //{
                        
                    //    gs.back_campo2 = gestionActivacion.seguimiento_obs;
                    //    gs.seguimiento_fechaInstalacion = gestionActivacion.seguimiento_prox_fecha_inst;

                    //    gs.Instalacion_EstadoInicial = gestionActivacion.Instalacion_EstadoInicial;
                    //    gs.Instalacion_EstadoFinal = gestionActivacion.Instalacion_estadoFinal;
                    //    gs.Instalacion_RazonAgendamiento = gestionActivacion.Instalacion_RazonAgendamiento;
                    //    gs.fechaReprogramacion = gestionActivacion.Instalacion_FechaReprogramacion;

                    //    gs.instaladaEn = DateTime.Now;
                    //    gs.InstaladaActulizadoPor = SessionPersister.Id;
                    //}
                    ////VENTA SOPORTADA
                    //if (gestionActivacion.EstadoId == _EstadosGestion.VentaLegalizada || gestionActivacion.EstadoId == _EstadosGestion.CaidaLegalizacion)
                    //{
                    //    gs.back_campo3 = gestionActivacion.seguimiento_obs;
                    //    gs.soportadaEn = DateTime.Now;
                    //    gs.soportadaPor = SessionPersister.Id;
                    //}



                    gs.EstadoId = Guid.Parse(gestionActivacion.EstadoId.ToString());

                    gs.UltimoUpdate = DateTime.Now;
                    db2.Entry(gs).State = EntityState.Modified;
                    db2.SaveChanges();

                }




                return Json("ok");
            }

            Response.StatusCode = (int)HttpStatusCode.Created;
            return Json(Errors);

        }



        /*------------------------------------------------------*
        * GESTIONAR VENTA POR INSTALAR.
        *------------------------------------------------------*/


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GestionarInstalacion(BackofficeGestionVM gestionActivacion)
        {
            List<CustoErrors> Errors = new List<CustoErrors>();

            if (gestionActivacion.EstadoId == null)
            {
                Errors.Add(new CustoErrors() { Name = "", Message = "Por favor seleccione una tipificación", Type = CustomErrorType.Simple });
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(Errors);
            }

            if (gestionActivacion.EstadoId == _EstadosGestion.VentaSoportada)
            {
                if (gestionActivacion.seguimiento_prox_fecha_inst == null) Errors.Add(new CustoErrors() { Name = "", Message = "Por favor seleccione la fecha de instalación o de reprogramación", Type = CustomErrorType.Simple });

            }
            else
            {
                if (String.IsNullOrEmpty(gestionActivacion.seguimiento_obs)) Errors.Add(new CustoErrors() { Name = "", Message = "Por favor ingrese la razon de la caida de la venta", Type = CustomErrorType.Simple });
            }

            // guardar la actualizacion de la venta.
            if (Errors.Count == 0)
            {
                GestionActivacion gsActivacion = db.GestionActivacion.Find(gestionActivacion.Id);
                if (gsActivacion == null)
                {
                    Errors.Add(new CustoErrors() { Message = "Error De validacion de formulario", Type = CustomErrorType.Simple });

                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json(Errors);
                }

                Gestion gs = db.Gestion.Find(gsActivacion.GestionId);

                // update de gestion activacion.    
                using (Contexto db1 = new Contexto())
                {
                    if (gestionActivacion.EstadoId == _EstadosGestion.VentaSoportada)
                    {

                        gsActivacion.EstadosId = _EstadosGestion.VentaSoportada;
                        db1.Entry(gsActivacion).State = EntityState.Modified;
                        db1.SaveChanges();
                    }
                    else
                    {
                        db.GestionActivacion.Remove(gsActivacion);
                        db.SaveChanges();
                    }


                }

                using (Contexto db2 = new Contexto())
                {
                    gs.EstadoId = Guid.Parse(gestionActivacion.EstadoId.ToString());
                    gs.seguimiento_fechaInstalacion = gestionActivacion.seguimiento_prox_fecha_inst;
                    gs.back_campo2 = gestionActivacion.seguimiento_obs;

                    gs.Instalacion_EstadoInicial = gestionActivacion.Instalacion_EstadoInicial;
                    gs.Instalacion_EstadoFinal = gestionActivacion.Instalacion_estadoFinal;
                    gs.Instalacion_RazonAgendamiento = gestionActivacion.Instalacion_RazonAgendamiento;
                    gs.fechaReprogramacion = gestionActivacion.Instalacion_FechaReprogramacion;

                    gs.instaladaEn = DateTime.Now;
                    gs.UltimoUpdate = DateTime.Now;
                    gs.InstaladaActulizadoPor = SessionPersister.Id;
                    db2.Entry(gs).State = EntityState.Modified;
                    db2.SaveChanges();

                }




                return Json("0k");
            }

            Response.StatusCode = (int)HttpStatusCode.Created;
            return Json(Errors);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ActualizarInstalacion(BackofficeGestionVM gestionActivacion)
        {
            List<CustoErrors> Errors = new List<CustoErrors>();

            // guardar la actualizacion de la venta.
            if (Errors.Count == 0)
            {
                GestionActivacion gsActivacion = db.GestionActivacion.Find(gestionActivacion.Id);
                if (gsActivacion == null)
                {
                    Errors.Add(new CustoErrors() { Message = "Error De validacion de formulario", Type = CustomErrorType.Simple });
                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json(Errors);
                }

                Gestion gs = db.Gestion.Find(gsActivacion.GestionId);
                using (Contexto db2 = new Contexto())
                {

                    gs.seguimiento_fechaInstalacion = gestionActivacion.seguimiento_prox_fecha_inst;
                    gs.back_campo2 = gestionActivacion.seguimiento_obs;
                    gs.Instalacion_EstadoInicial = gestionActivacion.Instalacion_EstadoInicial;
                    gs.Instalacion_EstadoFinal = gestionActivacion.Instalacion_estadoFinal;
                    gs.Instalacion_RazonAgendamiento = gestionActivacion.Instalacion_RazonAgendamiento;
                    gs.fechaReprogramacion = gestionActivacion.Instalacion_FechaReprogramacion;
                    gs.instaladaEn = DateTime.Now;
                    gs.UltimoUpdate = DateTime.Now;
                    gs.InstaladaActulizadoPor = SessionPersister.Id;
                    db2.Entry(gs).State = EntityState.Modified;
                    db2.SaveChanges();
                }




                return Json("0k");
            }

            Response.StatusCode = (int)HttpStatusCode.Created;
            return Json(Errors);

        }




        /*------------------------------------------------------*
        * GESTIONAR VENTA POR LEGALIZAR.
        *------------------------------------------------------*/



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GestionLegalizacion(BackofficeGestionVM gestionActivacion)
        {
            List<CustoErrors> Errors = new List<CustoErrors>();

            if (gestionActivacion.EstadoId == null)
            {
                Errors.Add(new CustoErrors() { Name = "", Message = "Por favor seleccione una tipificación", Type = CustomErrorType.Simple });
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(Errors);
            }

            if (gestionActivacion.EstadoId == _EstadosGestion.VentaLegalizada)
            {
                if (gestionActivacion.seguimiento_prox_fecha_inst == null) Errors.Add(new CustoErrors() { Name = "", Message = "La fecha de instalación es obligatoria", Type = CustomErrorType.Simple });

            }
            else
            {
                if (String.IsNullOrEmpty(gestionActivacion.seguimiento_obs)) Errors.Add(new CustoErrors() { Name = "", Message = "Por favor ingrese la razon de la caida de la venta", Type = CustomErrorType.Simple });
            }

            // guardar la actualizacion de la venta.
            if (Errors.Count == 0)
            {
                GestionActivacion gsActivacion = db.GestionActivacion.Find(gestionActivacion.Id);
                if (gsActivacion == null)
                {
                    Errors.Add(new CustoErrors() { Message = "Error De validacion de formulario", Type = CustomErrorType.Simple });

                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json(Errors);
                }

                Gestion gs = db.Gestion.Find(gsActivacion.GestionId);

                // update de gestion activacion.    
                using (Contexto db1 = new Contexto())
                {
                        db.GestionActivacion.Remove(gsActivacion);
                        db.SaveChanges();

                }

                using (Contexto db2 = new Contexto())
                {
                    gs.EstadoId = Guid.Parse(gestionActivacion.EstadoId.ToString());
                    gs.UltimoUpdate = DateTime.Now;
                    gs.back_campo3 = gestionActivacion.seguimiento_obs;
                    db2.Entry(gs).State = EntityState.Modified;
                    db2.SaveChanges();

                }

                return Json("0k");
            }

            Response.StatusCode = (int)HttpStatusCode.Created;
            return Json(Errors);

        }



        /*------------------------------------------------------*
        * GESTIONAR Vista para solicitar una gestion.
        *------------------------------------------------------*/



        public ActionResult Solicitar(Guid? id = null)
        {

            ViewBag.titulo = "Ventas por digitar";
            return View("~/Views/GestionActivacion/Details.cshtml");
        }





        // GET: GestionActivacions
        public ActionResult Index()
        {
            var gestionActivacion = db.GestionActivacion.Include(g => g.Estado).Include(g => g.Gestion);
            return View(gestionActivacion.ToList());
        }

        // GET: GestionActivacions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Created);
            }
            GestionActivacion gestionActivacion = db.GestionActivacion.Find(id);
            if (gestionActivacion == null)
            {
                return HttpNotFound();
            }
            return View(gestionActivacion);
        }

        // GET: GestionActivacions/Create
        public ActionResult Create()
        {
            ViewBag.EstadosId = new SelectList(db.Configurations, "Id", "Name");
            ViewBag.GestionId = new SelectList(db.Gestion, "Id", "captura_Nombre");
            return View();
        }

        // POST: GestionActivacions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,fechaVenta,AgenteId,GestionId,EstadosId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] GestionActivacion gestionActivacion)
        {
            if (ModelState.IsValid)
            {
                db.GestionActivacion.Add(gestionActivacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadosId = new SelectList(db.Configurations, "Id", "Name", gestionActivacion.EstadosId);
            ViewBag.GestionId = new SelectList(db.Gestion, "Id", "captura_Nombre", gestionActivacion.GestionId);
            return View(gestionActivacion);
        }

        // GET: GestionActivacions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Created);
            }
            GestionActivacion gestionActivacion = db.GestionActivacion.Find(id);
            if (gestionActivacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadosId = new SelectList(db.Configurations, "Id", "Name", gestionActivacion.EstadosId);
            ViewBag.GestionId = new SelectList(db.Gestion, "Id", "captura_Nombre", gestionActivacion.GestionId);
            return View(gestionActivacion);
        }

        // POST: GestionActivacions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,fechaVenta,AgenteId,GestionId,EstadosId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] GestionActivacion gestionActivacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gestionActivacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadosId = new SelectList(db.Configurations, "Id", "Name", gestionActivacion.EstadosId);
            ViewBag.GestionId = new SelectList(db.Gestion, "Id", "captura_Nombre", gestionActivacion.GestionId);
            return View(gestionActivacion);
        }

        // GET: GestionActivacions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Created);
            }
            GestionActivacion gestionActivacion = db.GestionActivacion.Find(id);
            if (gestionActivacion == null)
            {
                return HttpNotFound();
            }
            return View(gestionActivacion);
        }

        // POST: GestionActivacions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GestionActivacion gestionActivacion = db.GestionActivacion.Find(id);
            db.GestionActivacion.Remove(gestionActivacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
