﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class WS_CalidadController : Controller
    {
        private Contexto db = new Contexto();


        public JsonResult GetTipificationsNew(String Id)
        {
            Guid Auditada = new Guid("D58DEE68-9DE9-44C1-92E2-86C105BED571");
            Guid Categoria = new Guid("89B2B7D4-AD9B-44AF-83EB-B0FE36D30EBE");

            if (Id.Equals ("80245538-98ea-4c52-86d3-8468aa798e60"))//Auditada Ok pendiente digitación
            {
                return Json(db.Configurations.Where(c => c.Status == true && c.Id == Auditada && c.CategoriesId== Categoria).ToList());
            }


            if (Id.Equals("785e5148-50f7-4c43-a7f3-23a8c9c68f0b"))//Por digitar
            {
                Guid porDigitar = new Guid("9B7D6AB7-DDFA-4A03-8CED-FEF50FDFC7BE");
                Categoria = new Guid("4e08646b-9eaf-466a-9b89-25e5867684ac");
                return Json(db.Configurations.Where(c => c.Status == true && c.Id == porDigitar).ToList());
            }

            else // if(Id == "38d3c94f-9474-47c4-b208-f7629ac228dd")
            {
                return Json(db.Configurations.Where(c => c.Status == true && c.Id != Auditada && c.CategoriesId == Categoria).ToList());
            }

            

        }








    }
}





    
