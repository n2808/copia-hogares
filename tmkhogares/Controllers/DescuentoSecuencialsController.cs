﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using tmkhogares.Models;

namespace tmkhogares.Controllers

{
    public class DescuentoSecuencialsController : Controller
    {
        private Contexto db = new Contexto();


        // POST: DescuentoSecuencials/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,CampanaId,mes,porcentajeDescuento,valorDescuento,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] DescuentoSecuencial descuentoSecuencial)
        {
            if (ModelState.IsValid)
            {
                db.DescuentoSecuencial.Add(descuentoSecuencial);
                db.SaveChanges();
                
                return Json(new { status = 200, message = descuentoSecuencial });

            }

            var errorList = (from item in ModelState
                             where item.Value.Errors.Any()
                             select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();


            return Json(new { status = 400, message = errorList });
        }


        // POST: DescuentoSecuencials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            DescuentoSecuencial descuentoSecuencial = db.DescuentoSecuencial.Find(id);
            if(descuentoSecuencial == null)
            {
                return Json(new { status = 404 });
            }
            db.DescuentoSecuencial.Remove(descuentoSecuencial);
            db.SaveChanges();
            return Json(new { status = 200 });

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
