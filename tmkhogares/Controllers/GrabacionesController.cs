﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;


namespace tmkhogares.Controllers
{
    public class GrabacionesController : Controller
    {


        private string Ruta1 = "\\172.16.5.27\\Recording\\rec\\$nombreArchivo2";
        private string Ruta2 = "\\172.16.5.132\\Recording\\2019\\$nombreArchivo2";
        private string Ruta3 = "\\172.16.5.132\\Recording_S\\2019\\$nombreArchivo2";
        private string ConexionPresence = @"Data Source=172.16.5.21\PRESENCE; Initial Catalog=SQLPR1; Integrated Security=false;User ID=sa; Password=Pr3s3nc3";
        private Contexto db = new Contexto();
        private string nombreArchivo = "", nombreArchivoMp3 = "", rutaMP3 = "", path = "", CarpetaTemporal = "", nombreGrabacion = "", rutaDestino = "", RutaRecording;
        private bool Retorno = false;
        private int? _Tipo = null, _IdGestion = null;
        private decimal? _idPresence = 0, ContactId = 0;

        // GET: Grabaciones
        public JsonResult BuscarGrabacion(decimal? idPresence = null, int Tipo = 1, int? IdGestion = null)
        {
            ContactId = null;
            _Tipo = Tipo;
            _idPresence = idPresence;
            _IdGestion = IdGestion;
            //1 => recuperacion
            //2 => Agente
            //3 => recuperacion

            if (idPresence == null)
            {
                return Json(new { status = 404, message = "no found" });
            }


            if (Tipo == 1)
            {
                ContactId = BuscarEnPresence(idPresence);
                nombreArchivo = ContactId.ToString() + ".gsm";
                // actualizar las rutas de grabacion
                Ruta1 = @"\\172.16.5.27\Recording\rec\" + nombreArchivo;
                Ruta2 = @"\\172.16.5.132\Recording\2019\" + nombreArchivo;
                Ruta3 = @"\\172.16.5.132\Recording_S\2019\" + nombreArchivo;

            }


            nombreArchivoMp3 = IdGestion + ".mp3";
            rutaMP3 = @"\\172.16.5.132\Claro_Legalizaciones\Fija\" + nombreArchivoMp3;


            if (Tipo == 2) // Agente
            {

                if (copiarArchivoGsm(rutaMP3, nombreArchivoMp3) == 1)
                {
                    string audio = "<audio id='audio2' src='/audioLectura/" + nombreArchivoMp3 + "' preload='none' controls></audio>";
                    return Json(new { status = 200, message = audio });
                }



            }
            else if (copiarArchivoGsm(Ruta1, nombreArchivo) == 1)
            {
                string audio = "<audio id='audio1' src='/audioLectura/temp_" + ContactId + ".mp3' preload='none' controls></audio>";
                return Json(new { status = 200, message = audio });
            }
            else if (copiarArchivoGsm(Ruta2, nombreArchivo) == 1)
            {
                string audio = "<audio id='audio1' src='/audioLectura/temp_" + ContactId + ".mp3' preload='none' controls></audio>";
                return Json(new { status = 200, message = audio });
            }
            else if (copiarArchivoGsm(Ruta3, nombreArchivo) == 1)
            {
                string audio = "<audio id='audio1' src='/audioLectura/temp_" + ContactId + ".mp3' preload='none' controls></audio>";
                return Json(new { status = 200, message = audio });
            }
            else
            {
                return Json(new { status = 201, message = "audio no encontrado" });
            }



            return Json(new { status = 404, message = "no found" });


        }

        public decimal BuscarEnPresence(decimal? Id = 0, bool Comercializacion = true)
        {
            string Procedimiento = "SP_BuscarGrabacionesClaroColombiaContratoIndividual_SinFcha";

            if (Comercializacion)
            {
                Procedimiento = "SP_BuscarGrabacionesTMKHogarContratoIndividual";
            }


            SqlConnection connection = new SqlConnection(ConexionPresence);
            connection.Open();
            SqlCommand comando = new SqlCommand(Procedimiento, connection);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@CONTACTID", Id);
            decimal contactId = (decimal)comando.ExecuteScalar();
            connection.Close();
            return contactId;
        }

        //COPIA EL ARCHIVO GSM A LA CARPETA INDICADA.
        public int copiarArchivoGsm(string ruta, string NombreArchivo)
        {

            if (System.IO.File.Exists(ruta))
            {
                //System.IO.File.Copy(Evidencia_Evidente, path, true);
                if (_Tipo == 2)
                {
                    var path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(NombreArchivo));
                    System.IO.File.Copy(ruta, path, true);
                    return 1;


                }
                else
                {

                    NombreArchivo = Path.GetFileNameWithoutExtension(NombreArchivo);
                    //convertir en Gsm
                    path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreArchivo));
                    nombreGrabacion = "temp_" + NombreArchivo;
                    CarpetaTemporal = "tempConversion";
                    rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\tempConversion\" + nombreGrabacion + ".gsm";
                    System.IO.File.Copy(ruta, rutaDestino, true);

                    using (var client = new HttpClient())
                    {
                        var uri = new Uri("http://172.16.5.35/recording/convertirArchivo.php");

                        var parametros = new
                        Dictionary<string, string>

                        {
                            { "idGestion" , nombreGrabacion },
                            { "carpeta" , CarpetaTemporal },
                            { "nomgrabacion" , nombreGrabacion }
                        };

                        var encodedContent = new FormUrlEncodedContent(parametros);
                        var response = client.PostAsync(uri.ToString(), encodedContent).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            // codigo exitoso.
                            var customerJsonString = response.Content.ReadAsStringAsync().Result;



                            rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\tempConversion\" + nombreGrabacion + ".mp3";
                            path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreGrabacion + ".mp3"));
                            System.IO.File.Copy(rutaDestino, path, true);
                            System.IO.File.Delete(rutaDestino);
                            return 1;

                        }

                    }



                }
            }
            // \.fin the file exit
            else
            {
                Retorno = false;

                if (_Tipo == 2)
                {

                    // definir id de presence a buscar
                    if (_idPresence != 0 && _idPresence != null)
                    {
                        ContactId = BuscarEnPresence(_idPresence, false);
                        nombreArchivo = ContactId + ".gsm";

                        if (ContactId != null && ContactId != 0)
                        {
                            CarpetaTemporal = "Fija";
                            nombreGrabacion = _IdGestion.ToString();


                            rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\" + CarpetaTemporal + "\\" + nombreGrabacion + ".gsm";
                            RutaRecording = @"\\172.16.5.27\Recording\rec\" + nombreArchivo;

                            if (System.IO.File.Exists(RutaRecording))
                            {

                                var path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreArchivo));
                                System.IO.File.Copy(RutaRecording, rutaDestino, true);

                                // CODIGO PARA GUARDAR EL ID 
                                using (var client = new HttpClient())
                                {
                                    var uri = new Uri("http://172.16.5.35/recording/convertirArchivo.php");

                                    var parametros = new
                                    {
                                        idGestion = nombreGrabacion,
                                        carpeta = CarpetaTemporal,
                                        nomgrabacion = nombreGrabacion
                                    };


                                    var response = client.PostAsJsonAsync(uri.ToString(), parametros).Result;

                                    if (response.IsSuccessStatusCode)
                                    {
                                        // codigo exitoso.
                                        path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreGrabacion + ".mp3"));
                                        System.IO.File.Copy(nombreArchivo, path, true);
                                        System.IO.File.Delete(rutaDestino);
                                        return 1;

                                    }

                                }

                            }


                        }


                    }
                }
            }

            return 2;
        }

        // CARGA LA GRABACION PARA EL BACKKOFFICE
        public JsonResult CargarGrabacionAvaya(int idGestion)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var archivoGrabacion = Request.Files[0];
                    if (FormatoGrabacionAvaya(Request.Files[0]))
                    {
                        string nombreArchivo = Convert.ToString(idGestion);

                        string nombre_archivo = @"\\172.16.5.132\Claro_Legalizaciones\Fija\" + nombreArchivo + ".mp3";
                   //   string nombre_archivo = @"C:\Users\1053853797\Documents" + nombreArchivo + ".mp3";
                        ////guardar grabacion en ruta compartida
                        archivoGrabacion.SaveAs(nombre_archivo);
                      
                        rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\Fija\" + nombreArchivo + ".mp3";
                    //  rutaDestino = @"C:\Users\1053853797\Documents\" + nombreArchivo + ".mp3";
                        path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreArchivo + ".mp3"));
                        System.IO.File.Copy(rutaDestino, path, true);

                    }
                    else
                    {
                        return Json(new { type = "ERROR", message = "El formato de la grabación no es correcto." }, JsonRequestBehavior.DenyGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { type = "ERROR", message = "La grabación no se pudo cargar " }, JsonRequestBehavior.DenyGet);
            }
            string audio = "<audio id='audio1' src='/audioLectura/" + idGestion + ".mp3' preload='none' controls></audio>";
            return Json(new { status = 200, message = audio });
            //return Json(new { type = "OK", message = "La grabación se cargo correctamente" }, JsonRequestBehavior.DenyGet);
        }

        public bool FormatoGrabacionAvaya(HttpPostedFileBase formato)
        {
            bool formatoValido = false;

            string[] formatosSoportados = new string[1];
            formatosSoportados[0] = ".mp3";

            foreach (string x in formatosSoportados)
            {
                if (formato.FileName.ToLower().Contains(x))
                {
                    return true;
                }
            }

            return formatoValido;
        }

    }
}