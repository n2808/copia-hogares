﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Gestion;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class ClientesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Clientes
        public ActionResult Index()
        {
            var cliente = db.cliente.Include(c => c.Carga);
            return View(cliente.ToList());
        }

        // get client

        public JsonResult GetClient(string documento = "")
        {
            var cliente = db.cliente.Select(c => new { c.NUMERO_IDENTIFICACION, c.Id }).Where(c => c.NUMERO_IDENTIFICACION == documento).FirstOrDefault();
            if (cliente != null)
            {
                return Json(cliente ,JsonRequestBehavior.DenyGet);

            }
            else
            {
                return Json(new { Id = 0 });
            }
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.cliente.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            ViewBag.CargaId = new SelectList(db.Cargar, "Id", "Nombre");
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,CONTRATO_CUSTCODE,CUENTA_CO_ID,TELE_NUMB,ESTADO,NOMBRE_CLIENTE,TIPO_IDENT,NUMERO_IDENTIFICACION,TIPO_CLIENTE,DIRECCION,CIUDAD,ESTRATO,TELEFONO_1,TELEFONO_2,TELEFONO_3,CELULAR_1,CELULAR_2,EMAIL,FECHA_VINCULACION,ANTIGUEDAD,CALIFICACION_CLIENTE,TMCODE_ACTUAL,TIPO_PRODUCTO,SEGMENTO_PLAN,TECNOLOGIA_PLAN,DES_TMCODE,CFM_PLAN,SPCODE,DES_SPCODE,TECNOLOGIA_PAQUETE,CFM_PAQ,ARPU,DETALLE_SERVICIOS_ADICIONALES,COMPORTAMIENTO_PAGO,CONSUMO_DATOS_MES_1,CONSUMO_DATOS_MES_2,CONSUMO_DATOS_MES_3,MOU_MES,TIPO_EQUIPO,TICKLER,DESACTIV,PLAN_PAR,AJUSTES,minutos_incluidos,megas_incluidas,Campo_Movil_3,Campo_Movil_4,Campo_Movil_5,NODO,COMUNIDAD,DIVISION,TARIFA,RENTA_RR,SERVICIO_TV,SERVICIO_INTERNET,MINTIC,SERVICIO_VOZ,TIPO_HD,TIPO_HBO,TIPO_FOX,TIPO_ADULTO,DECOS_ADICIONALES_HD_PVR,TIPO_CLAROVIDEO,NUM_DECOS_ADICIONALES_TV,TIPO_REVISTA,TIPO_OTROS,SEGMENTO_CLIENTE,DIVISION_COMERCIAL,AREA_COMERCIAL,ZONA_COMERCIAL,Empaquetamiento,PAQUETE_ACTUAL,RENTA_BASICOS,RENTA_ADICIONALES,RENTA_TOTAL_IVA,BENEFICIO,DESCUENTO_OFERTA_COMERCIAL,ABS_DESCUENTO,VALOR_COMERCIAL_OFERTA,VALOR_OFERTA_CON_DESC,VENTA_TECNOLOGIA,EQUIPO_ACTUAL,OFERTA_MULTIPLAY,BLINDAJE_MOVIL,Campo_Fijo_4,Campo_Fijo_5,adicionales_APELLIDO,adicionales_NOMBRE,adicionales_ESTRATEGIA,adicionales_TIPO_PAQUETE,adicionales_TELEFONO_TELMEX,adicionales_FECHA_ULTIMA_OT,adicionales_RENTA_ESTIMADA,adicionales_TRABAJOREVISTA,adicionales_PROBABILIDAD_VENTA,adicionales_CTA_MATRIZ,adicionales_CORTE,adicionales_CUENTAS_ALTERNAS,adicionales_DIRECCIONES_ALTERNAS,adicionales_Oferta_1,adicionales_Valor_Oferta_1,adicionales_Diferencia_OFerta_1,adicionales_Codigo_Tarifa_1,adicionales_Codigo_Politica_1,adicionales_Oferta_2,adicionales_Valor_Oferta_2,adicionales_Diferencia_OFerta_2,adicionales_Codigo_Tarifa_2,adicionales_Codigo_Politica_2,adicionales_Oferta_3,adicionales_Valor_Oferta_3,adicionales_Diferencia_OFerta_3,adicionales_Codigo_Tarifa_3,adicionales_Codigo_Politica_3,adicionales_Oferta_1_Adicional,adicionales_Oferta_4,adicionales_Valor_Oferta_4,adicionales_Codigo_Tarifa_4,cruzada_NUM_DECOS_ADI,cruzada_DIVISION_COMERCIAL,CargaId,EstadoId,car_ProductoId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Cliente cliente)
        {
            var dbCliente = db.cliente.Where(c => c.NUMERO_IDENTIFICACION == cliente.NUMERO_IDENTIFICACION).FirstOrDefault();
            if (dbCliente == null)
            {
                cliente.CargaId = 1;
                cliente.car_ProductoId = 1;
                if(cliente.TELEFONO_1 != null && cliente.TELEFONO_1.Length == 10) { cliente.TELEFONO_1 = "3" + cliente.TELEFONO_1;}
                if(cliente.TELEFONO_2 != null &&  cliente.TELEFONO_2.Length == 10) { cliente.TELEFONO_2 = "3" + cliente.TELEFONO_2; }
                if(cliente.TELEFONO_3 != null &&  cliente.TELEFONO_3.Length == 10) { cliente.TELEFONO_3 = "3" + cliente.TELEFONO_3; }
                if(cliente.CELULAR_1 != null && cliente.CELULAR_1.Length == 10) { cliente.TELEFONO_1 = "3" + cliente.TELEFONO_1;}
                if(cliente.CELULAR_2 != null &&  cliente.CELULAR_2.Length == 10) { cliente.TELEFONO_1 = "3" + cliente.TELEFONO_1;}
                db.cliente.Add(cliente);
                db.SaveChanges();
                return Json(new { Id = cliente.Id , Error = ""});
            }
            else
            {
                return Json(new { Id = dbCliente.Id, Error = "El cliente ya existe" });

            }
            
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.cliente.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.CargaId = new SelectList(db.Cargar, "Id", "Nombre", cliente.CargaId);
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CONTRATO_CUSTCODE,CUENTA_CO_ID,TELE_NUMB,ESTADO,NOMBRE_CLIENTE,TIPO_IDENT,NUMERO_IDENTIFICACION,TIPO_CLIENTE,DIRECCION,CIUDAD,ESTRATO,TELEFONO_1,TELEFONO_2,TELEFONO_3,CELULAR_1,CELULAR_2,EMAIL,FECHA_VINCULACION,ANTIGUEDAD,CALIFICACION_CLIENTE,TMCODE_ACTUAL,TIPO_PRODUCTO,SEGMENTO_PLAN,TECNOLOGIA_PLAN,DES_TMCODE,CFM_PLAN,SPCODE,DES_SPCODE,TECNOLOGIA_PAQUETE,CFM_PAQ,ARPU,DETALLE_SERVICIOS_ADICIONALES,COMPORTAMIENTO_PAGO,CONSUMO_DATOS_MES_1,CONSUMO_DATOS_MES_2,CONSUMO_DATOS_MES_3,MOU_MES,TIPO_EQUIPO,TICKLER,DESACTIV,PLAN_PAR,AJUSTES,minutos_incluidos,megas_incluidas,Campo_Movil_3,Campo_Movil_4,Campo_Movil_5,NODO,COMUNIDAD,DIVISION,TARIFA,RENTA_RR,SERVICIO_TV,SERVICIO_INTERNET,MINTIC,SERVICIO_VOZ,TIPO_HD,TIPO_HBO,TIPO_FOX,TIPO_ADULTO,DECOS_ADICIONALES_HD_PVR,TIPO_CLAROVIDEO,NUM_DECOS_ADICIONALES_TV,TIPO_REVISTA,TIPO_OTROS,SEGMENTO_CLIENTE,DIVISION_COMERCIAL,AREA_COMERCIAL,ZONA_COMERCIAL,Empaquetamiento,PAQUETE_ACTUAL,RENTA_BASICOS,RENTA_ADICIONALES,RENTA_TOTAL_IVA,BENEFICIO,DESCUENTO_OFERTA_COMERCIAL,ABS_DESCUENTO,VALOR_COMERCIAL_OFERTA,VALOR_OFERTA_CON_DESC,VENTA_TECNOLOGIA,EQUIPO_ACTUAL,OFERTA_MULTIPLAY,BLINDAJE_MOVIL,Campo_Fijo_4,Campo_Fijo_5,adicionales_APELLIDO,adicionales_NOMBRE,adicionales_ESTRATEGIA,adicionales_TIPO_PAQUETE,adicionales_TELEFONO_TELMEX,adicionales_FECHA_ULTIMA_OT,adicionales_RENTA_ESTIMADA,adicionales_TRABAJOREVISTA,adicionales_PROBABILIDAD_VENTA,adicionales_CTA_MATRIZ,adicionales_CORTE,adicionales_CUENTAS_ALTERNAS,adicionales_DIRECCIONES_ALTERNAS,adicionales_Oferta_1,adicionales_Valor_Oferta_1,adicionales_Diferencia_OFerta_1,adicionales_Codigo_Tarifa_1,adicionales_Codigo_Politica_1,adicionales_Oferta_2,adicionales_Valor_Oferta_2,adicionales_Diferencia_OFerta_2,adicionales_Codigo_Tarifa_2,adicionales_Codigo_Politica_2,adicionales_Oferta_3,adicionales_Valor_Oferta_3,adicionales_Diferencia_OFerta_3,adicionales_Codigo_Tarifa_3,adicionales_Codigo_Politica_3,adicionales_Oferta_1_Adicional,adicionales_Oferta_4,adicionales_Valor_Oferta_4,adicionales_Codigo_Tarifa_4,cruzada_NUM_DECOS_ADI,cruzada_DIVISION_COMERCIAL,CargaId,EstadoId,car_ProductoId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CargaId = new SelectList(db.Cargar, "Id", "Nombre", cliente.CargaId);
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.cliente.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.cliente.Find(id);
            db.cliente.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
