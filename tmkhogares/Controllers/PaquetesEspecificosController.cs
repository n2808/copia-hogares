﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class PaquetesEspecificosController : Controller
    {
        private Contexto db = new Contexto();


        // POST: PaquetesEspecificos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,CampanaId,tv,ba,Line,Cod1,Cod2,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PaquetesEspecificos paquetesEspecificos)
        {
            if (ModelState.IsValid)
            {
                // delete previos packages
                db.PaquetesEspecificos.RemoveRange(db.PaquetesEspecificos.Where(c => c.CampanaId == paquetesEspecificos.CampanaId));
                db.SaveChanges();

                // add the new package
                db.PaquetesEspecificos.Add(paquetesEspecificos);
                db.SaveChanges();
                return Json(new { status = 200 });
            }
            return Json(new { status = 403, message = "no se ha podido crear el paquete" });
        }



        // POST: PaquetesEspecificos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            PaquetesEspecificos paquetesEspecificos = db.PaquetesEspecificos.Find(id);
            if(paquetesEspecificos == null)
            {
                return Json(new { status = 404 });

            }
            db.PaquetesEspecificos.Remove(paquetesEspecificos);
            db.SaveChanges();
            return Json(new { status = 200 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
