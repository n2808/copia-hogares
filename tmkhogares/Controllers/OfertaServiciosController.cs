﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Servicios;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class OfertaServiciosController : Controller
    {
        private Contexto db = new Contexto();

        // GET: OfertaServicios
        public ActionResult Index()
        {
            var servicio = db.Servicio.Include(s => s.productos);
            return View(servicio.ToList());
        }

        // GET: OfertaServicios/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servicio servicio = db.Servicio.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            return View(servicio);
        }

        // GET: OfertaServicios/Create
        public ActionResult Create()
        {
            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre");
            return View();
        }

        // POST: OfertaServicios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Estado,NumeroServicios,ServicioPrincipal,ProductoOfrecidoId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Servicio servicio)
        {
            if (ModelState.IsValid)
            {
                servicio.Id = Guid.NewGuid();
                db.Servicio.Add(servicio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre", servicio.ProductoOfrecidoId);
            return View(servicio);
        }

        // GET: OfertaServicios/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servicio servicio = db.Servicio.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre", servicio.ProductoOfrecidoId);
            return View(servicio);
        }

        // POST: OfertaServicios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Estado,NumeroServicios,ServicioPrincipal,ProductoOfrecidoId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Servicio servicio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre", servicio.ProductoOfrecidoId);
            return View(servicio);
        }

        // GET: OfertaServicios/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servicio servicio = db.Servicio.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            return View(servicio);
        }

        // POST: OfertaServicios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Servicio servicio = db.Servicio.Find(id);
            db.Servicio.Remove(servicio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
