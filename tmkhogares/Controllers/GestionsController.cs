﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Gestion;
using Core.Models.Security;
using Core.Models.Servicios;
using Core.Models.User;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    
    public class GestionsController : Controller
    {



        private Contexto db = new Contexto();

        // obtener la lista de ciudades
        public JsonResult getCities(Guid State)
        {
            return Json(new SelectList(db.Ciudad.Where(c => c.StateId == State), "Id", "Name"));
        }
                // obtener la lista de ciudades
        public JsonResult getMotivoLlamada(Guid Motivo)
        {
            return Json(new SelectList(db.Configurations.Where(c => c.CategoriesId== Motivo).OrderBy(c => c.OrderId), "Id", "Name"));
        }


        // obtener la lista de tipificaciones a aplicar.


        public JsonResult getTipificacion(int Id, int? Producto = null)
        {

            if (Producto != null)
            {
                return Json(db.Tipificacions.Where(c => c.Estado == true && c.padreId == Id && Producto == c.productoId).ToList());
            }
            else
            {
                return Json(db.Tipificacions.Where(c => c.Estado == true && c.padreId == Id).ToList());
            }


        }





        // GET: Gestiones en generar
        [CustomAuthorize]
        public ActionResult Index(int? id)
        { 

            var gestion = db.Gestion.Include(g => g.Estado)
                .Where(c =>  c.EstadoId != _EstadosGestion.EnGestion
                && c.Id == id
                && id != null
                );
            ViewBag.Id = id;
            return View(gestion.ToList());
        }

        //POST: RECUPERA UNA GESTION Y LA PONE EN EL ULTIMO ESTADO.

        
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult Recuperar(int id)
        {
            //verificar la gestion
            
            Gestion gestion = db.Gestion.Find(id);
            Guid NuevoEstado = Guid.Empty;


            /*-----------------------------------------
             * ACTUALIZAR PREVENTA A VENTA
             ------------------------------------------*/

            if (gestion.EstadoId == _EstadosGestion.NoVenta)
            {
                Tipificacion Tip = db.Tipificacions.Find(gestion.TipificacionId);

                var detalleventas = db.DetalleVenta.Where(c => c.GestionId == gestion.Id);

                if (Tip == null || (!_Tipificaciones.LPreventas.Contains(Tip.Id)  && Tip.Id != _Tipificaciones.ACW)  )
                {
                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json("La gestion no esta disponible");
                }
                gestion.TipificacionId = _Tipificaciones.VentaFijo2;
                gestion.UpdatedBy = new Guid("1BB73E37-9530-4794-B60C-A51403A36B4F");
                //gestion.CreatedAt = DateTime.Now;
                //gestion.cantadaEn = DateTime.Now;
                NuevoEstado = _EstadosGestion.VentaCantada;
                
            }


            /*-----------------------------------------
             * REINICIAR Y RECUPERAR VENTA.
             ------------------------------------------*/

            if (_EstadosGestion.LEstadosAprobados.Contains(gestion.EstadoId))
            {
                NuevoEstado = _EstadosGestion.VentaCantada;
            }
            else
            {

                // ESTADO DE GESTION PRIMERO AUDITADO, DESPUES RECUPERACION.
                //if (gestion.EstadoId == _EstadosGestion.CaidaDigitacion) { NuevoEstado = _EstadosGestion.AuditadaOk; }
                //if (gestion.EstadoId == _EstadosGestion.RechazadaAgente) { NuevoEstado = _EstadosGestion.VentaCantada; }
                //if (gestion.EstadoId == _EstadosGestion.CaidaInstalacion) { NuevoEstado = _EstadosGestion.VentaDigitada; }
                //if (gestion.EstadoId == _EstadosGestion.CaidaLegalizacion) { NuevoEstado = _EstadosGestion.VentaSoportada; }
                //if (gestion.EstadoId == _EstadosGestion.CaidaTextoLegal) { NuevoEstado = _EstadosGestion.VentaCantada; }

                

                if (gestion.EstadoId == _EstadosGestion.CaidaDigitacion) { NuevoEstado = _EstadosGestion.VentaCantada; }
                if (gestion.EstadoId == _EstadosGestion.RechazadaAgente) { NuevoEstado = _EstadosGestion.VentaCantada; }
                if (gestion.EstadoId == _EstadosGestion.CaidaInstalacion) { NuevoEstado = _EstadosGestion.VentaDigitada; }
                if (gestion.EstadoId == _EstadosGestion.CaidaLegalizacion) { NuevoEstado = _EstadosGestion.VentaSoportada; }
                if (gestion.EstadoId == _EstadosGestion.CaidaTextoLegal) { NuevoEstado = _EstadosGestion.VentaCantada; }
            //    if (gestion.EstadoId == _EstadosGestion.CaidaDigitacionAgente) { NuevoEstado = _EstadosGestion.VentaCantada; }


            }

            if (gestion == null || NuevoEstado ==  Guid.Empty)
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json("La gestion no esta disponible");
            }

            // ACTUALIZAR LA GESTION
            gestion.EstadoId = NuevoEstado;
            gestion.UltimoUpdate = DateTime.Now;
            //db.Entry(gestion).State = EntityState.Modified;
            db.SaveChanges();



            // ACTUALIZAR LA GESTION_ACTIVACION
            using (var db1 = new Contexto())
            {
                GestionActivacion gestionActivacion = db1.GestionActivacion.Where(c => c.GestionId == id).FirstOrDefault();
                // AÑADIR LA GESTION DE ACTIVACION.
                if(gestionActivacion == null)
                {
                    db1.GestionActivacion.Add(new GestionActivacion() {
                        GestionId = gestion.Id,
                        fechaVenta = gestion.CreatedAt.Value,
                        EstadosId = NuevoEstado

                    });
                    db1.SaveChanges();
                }else
                {
                    gestionActivacion.EstadosId = NuevoEstado;
                    db1.SaveChanges();
                }
            }


            return Json("la venta fue recuperada");
       
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        [CustomAuthorize(Roles = "Administrador")]
        public JsonResult DevolverAPreventa(int id)
        {
            //verificar la gestion

            Gestion gestion = db.Gestion.Find(id);

            /*-----------------------------------------
             * REINICIAR Y RECUPERAR VENTA.
             ------------------------------------------*/

            if (gestion == null)
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json("La gestion no esta disponible");
            }

            // ACTUALIZAR LA GESTION
            gestion.EstadoId = _EstadosGestion.NoVenta;
            gestion.TipificacionId = _Tipificaciones.PreventaHPP3;
            gestion.UltimoUpdate = DateTime.Now;
            //db.Entry(gestion).State = EntityState.Modified;
            db.SaveChanges();

            // ACTUALIZAR LA GESTION_ACTIVACION
            using (var db1 = new Contexto())
            {
                var GestionesActivacion = db1.GestionActivacion.Where(c => c.GestionId == id).ToList();
                var Count = db1.GestionActivacion.Where(c => c.GestionId == id).Count();

                if(Count > 0)
                {
                    db1.GestionActivacion.RemoveRange(GestionesActivacion);
                    db1.SaveChanges();

                }

            }


            return Json("la venta fue recuperada");

        }






        // GET: gestion de misventas
        public ActionResult misventas(string id = "")
        {
            User Usuario = db.Usuario
               .Where(c => c.login == id)
                .FirstOrDefault();
            List<Gestion> gestion = null;
            if (Usuario != null)
            {


                gestion = db.Gestion.Include(c => c.Estado)
                .Where(c =>
                c.EstadoId != _EstadosGestion.NoVenta
                && c.EstadoId != _EstadosGestion.EnGestion
                && c.CreatedAt != null
                && c.CreatedAt.Value.Day == DateTime.Now.Day
                && c.CreatedAt.Value.Month == DateTime.Now.Month
                && c.cantadaPor != null
                && c.cantadaPor == Usuario.Id
                ).ToList();
            }
            return View(gestion);
        }


        // GET: Gestions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gestion gestion = db.Gestion
                .Include(c => c.Estado)
                .Include(c => c.captura_ciudad)
                .Include(c => c.captura_departamento)
                .Include(c => c.Tipificacion)
                
                .Where(c => c.Id == id).FirstOrDefault();

                if (gestion == null)
                {
                    return HttpNotFound();
                }


                Cliente cliente = db.cliente
                    .Include(j => j.Carga)
                    .Where(c => c.Id == gestion.ClientId)
                    .FirstOrDefault();
            ViewBag.productos = db.Productos.ToList();

            GestionVM venta = new GestionVM()
                {
                    Gestion = gestion,
                    cliente = cliente,
                    Asesor = db.Usuario.Include(c => c.Coordinador).Where( c => c.Id == gestion.cantadaPor).FirstOrDefault()
                };
            ViewBag.ItemsVenta = db.DetalleVenta.Where(c => c.GestionId == gestion.Id).ToList();

            return View(venta);
        }

        public PartialViewResult Evidencias(int? id)
        {
            Gestion gestion = db.Gestion.Find(id);

    
            return PartialView(gestion);
        }



        // GET: Gestions/Create
        public ActionResult Create()
        {
            ViewBag.EstadoId = new SelectList(db.Configurations, "Id", "Name");
            return View();
        }

        // POST: Gestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
    public JsonResult CreateSale([Bind(Exclude = "CreatedAt,UpdatedAt")] Gestion gestion
            , string uVerificador
            , string pVerificador
            , Guid? AsuuId = null
            , int? acw = 0
            , int? procesar = 0
            , string CodigoTipificacion = null
        )
        {




            /*------------------------------------------------------*
            * DECLARACION DE VARIABLES.
            *------------------------------------------------------*/
            List<CustoErrors> Errors = new List<CustoErrors>();
            int? TiptificacionId = gestion.TipificacionId;
            Guid NuevoEstado = _EstadosGestion.NoVenta;
            Guid ValidadorId = Guid.Parse(AsuuId.ToString());
            Gestion dbGestion = db.Gestion.Find(gestion.Id);
            bool isInbound = dbGestion.TipoServicioCamapa == "INBOUND" ? true : false;

            User Usuario     = db.Usuario
                               .Include(c => c.UserRol)
                               .Where(c => c.login == uVerificador &&
                                    c.PassWord == pVerificador &&
                                    c.UserRol.Any(j => j.RolId == _Roles.Validador))
                                .FirstOrDefault();






            /*------------------------------------------------------*
            * CREACIÓN DEL CLIENTE NUEVO PARA INBOUND.
            *------------------------------------------------------*/
            var clienteNuevo =  CreateClient(gestion);
            if(clienteNuevo != null && isInbound == true)
            {
                dbGestion.ClientId = clienteNuevo.Id;
                dbGestion.captura_Nombre = clienteNuevo.NOMBRE_CLIENTE;
                
            }


            /*------------------------------------------------------*
            * TIPIFICAR GESTION COMO CLIENTE NO EXISTE.
            *------------------------------------------------------*/


            if (acw == -1 && gestion.TipificacionId == 30010)
            {
                dbGestion.cantadaPor = AsuuId;
                dbGestion.TipificacionId = 128;
                dbGestion.EstadoId = NuevoEstado;
                db.Entry(dbGestion).State = EntityState.Modified;
                db.SaveChanges();
                return Json(gestion.Id.ToString());
            }



            /*----------------------------------------------------------------*
            * VALIDAR LA GESTION
            *-----------------------------------------------------------------*/

            if (acw != -1)
            {
                int newVdn = 0;
                Int32.TryParse(dbGestion.vdnLlamada, out newVdn);
                // configuracion del vdn.
                CampanasVdns FilaCampanaVdn = db.CampanasVdns.Find(newVdn);



                // remover la validacion del usurio
                //if (Usuario == null && procesar == 1)
                //    Errors.Add(new CustoErrors() { Name = "usuario", Message = "Usuario no autorizado" });
                if (Usuario != null)
                    ValidadorId = Usuario.Id;
                if (AsuuId == null)
                    Errors.Add(new CustoErrors() { Name = "usuario", Message = "Asesor no encontrado" });
                if ((CodigoTipificacion == "90" || CodigoTipificacion == "30011") && (gestion.DeletedAt < DateTime.Now || gestion.DeletedAt == null))
                {
                    Errors.Add(new CustoErrors() { Name = "Fecha Programacion", Message = "Por favor seleccione la fecha de agendamiento correcta" });

                }

                // tipificacion
                if (procesar == 1)
                    NuevoEstado = _EstadosGestion.VentaCantada;


                if (isInbound == true && db.Configurations.Where(c => c.CategoriesId == _Configuraciones.TipificacionesExcluirInbound && c.Name == CodigoTipificacion && c.Status == true).FirstOrDefault() == null)

                {


                    
                        if (string.IsNullOrEmpty(gestion.VENTA_NOMBRE_COMPLETO_CLIENTE) || gestion.VENTA_NOMBRE_COMPLETO_CLIENTE.Trim() == "")
                        Errors.Add(new CustoErrors() { Name = "usuario", Message = "El nombre del cliente es obligatorio" });

                    if(string.IsNullOrEmpty(gestion.captura_telefono1))
                        Errors.Add(new CustoErrors() { Name = "telefono", Message = "El telefono de contacto 1 es obligatorio" });

                    if (FilaCampanaVdn.Producto == _CategoriaVdns.IN)
                    {


                    if(gestion.Inbound_RazonLlamadaId == null )
                        Errors.Add(new CustoErrors() { Name = "Inbound_RazonLlamadaId", Message = "la Razón de la llamada es obligatorio" });

                    if (string.IsNullOrWhiteSpace(gestion.back_campo4))
                        Errors.Add(new CustoErrors() { Name = "BackCampo4", Message = "por favor seleccion el tipo de cliente" });

                    if (string.IsNullOrWhiteSpace(gestion.VENTA_NUMERO_CEDULA))
                        Errors.Add(new CustoErrors() { Name = "cedula", Message = "la cedula es obligatoria" });
                    
                    
                    if (gestion.captura_ciudadId == null)
                        Errors.Add(new CustoErrors() { Name = "captura_ciudadId", Message = "La ciudad es obligatoria" });


                 
                        if (string.IsNullOrWhiteSpace(gestion.QueQuiereComprar))
                            Errors.Add(new CustoErrors() { Name = "QueQuiereComprar", Message = "El campo QueQuiereComprar es obligatorio" });



                    if (gestion.VENTA_POLITICA == "0")
                        Errors.Add(new CustoErrors() { Name = "VENTA_POLITICA", Message = "por favor seleccione el campo ¿Medio por el que se entero de la promoción?" });

                    }


                }

            }
            /* OMITIR LAS VALIDACIONES */
            else
            {
                TiptificacionId = 13;
            }




            /*------------------------------------------------------*
            * SE GUARDA LA GESTION EN LA BASE DE DATOS.
            *------------------------------------------------------*/

            if (Errors.Count == 0)
            {


                /*| SE ACTUALIZAN CON LOS DATOS CAPTURADOS|*/
                dbGestion.UltimoUpdate = DateTime.Now;
                dbGestion.captura_departamentoId = gestion.captura_departamentoId;
                dbGestion.captura_ciudadId = gestion.captura_ciudadId;
                dbGestion.captura_barrio = gestion.captura_barrio;
                dbGestion.captura_telefono1 = gestion.captura_telefono1;
                dbGestion.captura_telefono2 = gestion.captura_telefono2;
                dbGestion.captura_Direccion = gestion.captura_Direccion;
                dbGestion.ValidadoPor = ValidadorId;
                dbGestion.cantadaPor = AsuuId;
                dbGestion.cantadaEn = DateTime.Now;
                dbGestion.UpdatedAt = DateTime.Now;
                dbGestion.captura_Observacion = gestion.captura_Observacion;
                dbGestion.VENTA_PERMANENCIA = gestion.VENTA_PERMANENCIA;
                dbGestion.VENTA_TIPODE_DE_CLIENTE = gestion.VENTA_TIPODE_DE_CLIENTE;
                dbGestion.VENTA_TIPO_DE_SOLICITUD = gestion.VENTA_TIPO_DE_SOLICITUD;
                dbGestion.VENTA_MULTIPLAY = gestion.VENTA_MULTIPLAY;
                dbGestion.VENTA_CUENTA_VENTA = gestion.VENTA_CUENTA_VENTA;
                dbGestion.VENTA_NOMBRE_COMPLETO_CLIENTE = gestion.VENTA_NOMBRE_COMPLETO_CLIENTE;
                dbGestion.VENTA_captura_tipodoc = gestion.VENTA_captura_tipodoc;
                dbGestion.VENTA_NUMERO_CEDULA = gestion.VENTA_NUMERO_CEDULA;
                dbGestion.VENTA_captura_fechaExped = gestion.VENTA_captura_fechaExped;
                dbGestion.VENTA_TIPO_DE_RED = gestion.VENTA_TIPO_DE_RED;
                dbGestion.VENTA_CUENTA_MATRIZ = gestion.VENTA_CUENTA_MATRIZ;
                dbGestion.VENTA_captura_Estrato = gestion.VENTA_captura_Estrato;
                dbGestion.VENTA_OPERADOR_ACTUAL = gestion.VENTA_OPERADOR_ACTUAL;
                dbGestion.VENTA_TELEFONO_FIJO = gestion.VENTA_TELEFONO_FIJO;
                dbGestion.VENTA_CELULAR = gestion.VENTA_CELULAR;
                dbGestion.VENTA_captura_ent_correspondencia = gestion.VENTA_captura_ent_correspondencia;
                dbGestion.VENTA_Captura_Email = gestion.VENTA_Captura_Email;
                dbGestion.VENTA_CODIGO_DE_VENDEDOR = gestion.VENTA_CODIGO_DE_VENDEDOR;
                dbGestion.VENTA_CEDULA_DE_VENDEDOR = gestion.VENTA_CEDULA_DE_VENDEDOR;
                dbGestion.VENTA_CANAL = gestion.VENTA_CANAL;
                dbGestion.VENTA_captura_serviciosActuales = gestion.VENTA_captura_serviciosActuales;
                dbGestion.VENTA_SERVICIOS_VENDIDOS = gestion.VENTA_SERVICIOS_VENDIDOS;
                dbGestion.VENTA_AVANZADA = gestion.VENTA_AVANZADA;
                dbGestion.VENTA_VELOCIDAD_M = gestion.VENTA_VELOCIDAD_M;
                dbGestion.VENTA_TOMAS_ADICIONALES_TV = gestion.VENTA_TOMAS_ADICIONALES_TV;
                dbGestion.VENTA_captura_cobroInstalacion = gestion.VENTA_captura_cobroInstalacion;
                dbGestion.VENTA_CAMPANA = gestion.VENTA_CAMPANA;
                dbGestion.VENTA_POLITICA = gestion.VENTA_POLITICA;
                dbGestion.VENTA_captura_ActualizarTarifa = gestion.VENTA_captura_ActualizarTarifa;
                dbGestion.VENTA_RENTA_MENSUAL = gestion.VENTA_RENTA_MENSUAL;
                dbGestion.VENTA_captura_fechaAgendamiento = gestion.VENTA_captura_fechaAgendamiento;
                dbGestion.VENTA_captura_franja = gestion.VENTA_captura_franja;
                dbGestion.VENTA_captura_rentaMensual = gestion.VENTA_captura_rentaMensual;
                dbGestion.VENTA__ID_VISION = gestion.VENTA__ID_VISION;
                dbGestion.VENTA_RECHAZO_BACK = gestion.VENTA_RECHAZO_BACK;
                dbGestion.VENTA_NUMERO_A_ACTIVAR = gestion.VENTA_NUMERO_A_ACTIVAR;
                dbGestion.VENTA_PLAN_ACT = gestion.VENTA_PLAN_ACT;
                dbGestion.CODIGO_EVIDENTE = gestion.CODIGO_EVIDENTE;
                dbGestion.VENTA_DECOSADICIONALES = gestion.VENTA_DECOSADICIONALES;
                dbGestion.VENTA_LUGAREXPEDICION = gestion.VENTA_LUGAREXPEDICION;
                dbGestion.Venta_CantidadDeServicios = gestion.Venta_CantidadDeServicios;
                dbGestion.DeletedAt = gestion.DeletedAt;
                dbGestion.TipificacionId = TiptificacionId;
                dbGestion.EstadoId = NuevoEstado;
                dbGestion.Inbound_RazonLlamadaId = gestion.Inbound_RazonLlamadaId;
                dbGestion.vdnLlamadaInbound = gestion.vdnLlamadaInbound;
                dbGestion.QueQuiereComprar = gestion.QueQuiereComprar;
                dbGestion.back_campo4 = gestion.back_campo4;

                db.Entry(dbGestion).State = EntityState.Modified;
                db.SaveChanges();



                if (NuevoEstado == _EstadosGestion.VentaCantada)
                {


                    /* 
                     *SE GUARDA LA GESTIONACTIVACION PARA EL BACKOFFICE
                     */
                    GestionActivacion gestionActivacion = new GestionActivacion()
                    {
                        GestionId = dbGestion.Id,
                        EstadosId = _EstadosGestion.VentaCantada
                    };

                    db.GestionActivacion.Add(gestionActivacion);
                    db.SaveChanges();






                }// fin foreach
                    

                /* 
                 * SE BORRAN LAS SESSIONES DE TRABAJO
                 */
                System.Web.HttpContext.Current.Session["idGestion"] = null;
                System.Web.HttpContext.Current.Session["sourceId"] = null;

                return Json(gestion.Id.ToString());
            }// fin error == 0 If


            ViewBag.EstadoId = new SelectList(db.Configurations, "Id", "Name", gestion.EstadoId);
            Response.StatusCode = (int)HttpStatusCode.Created;
            return Json(Errors, JsonRequestBehavior.DenyGet);
        }








        // GET: Gestions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gestion gestion = db.Gestion.Find(id);
            if (gestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoId = new SelectList(db.Configurations, "Id", "Name", gestion.EstadoId);
            return View(gestion);
        }

        // POST: Gestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,EstadoId,ClientId,Nombre1,Direccion,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Gestion gestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoId = new SelectList(db.Configurations, "Id", "Name", gestion.EstadoId);
            return View(gestion);
        }

        // GET: Gestions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gestion gestion = db.Gestion.Find(id);
            if (gestion == null)
            {
                return HttpNotFound();
            }
            return View(gestion);
        }

        // POST: Gestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Gestion gestion = db.Gestion.Find(id);
            db.Gestion.Remove(gestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public Cliente CreateClient(Gestion gestion)
        {
            
            var clienteAntiguo = db.cliente.Where(c => c.NUMERO_IDENTIFICACION == gestion.VENTA_NUMERO_CEDULA.Trim()).FirstOrDefault();

            // si el cliente existe se devuelve un null;
            if (clienteAntiguo != null)
            {
                return clienteAntiguo;
            }

            Cliente cliente = new Cliente()
            {
                NOMBRE_CLIENTE = gestion.VENTA_NOMBRE_COMPLETO_CLIENTE,
                NUMERO_IDENTIFICACION = gestion.VENTA_NUMERO_CEDULA,
                CargaId = 29,
                car_ProductoId = 6,
                DIRECCION = gestion.VENTA_DIRECCION,
                TELEFONO_1 = gestion.VENTA_TELEFONO_FIJO,
                TELEFONO_2 = gestion.VENTA_CELULAR,
            };


            using(var db1 = new Contexto())
            {

                db1.cliente.Add(cliente);
                db1.SaveChanges();
            }
                return cliente;
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

      

    }
}
