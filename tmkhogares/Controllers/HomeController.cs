﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using tmkhogares.Models;
using Core.Models.Gestion;
using tmkhogares.ViewModel;
using System.Diagnostics;
using Core.Models.User;
using Core.Models.Security;

namespace tmkhogares.Controllers
{
    public class HomeController : Controller
    {
        private Contexto  db = new Contexto();
        //private EstadosGestion estadosGestion = new EstadosGestion();

        public ActionResult Index()
        {
            return View();
        }


        [CustomAuthorize]
        public ActionResult HomeAdmin()
        {
            return View();
        }





        public ActionResult Gestion(
            string id  // id del cliente
            , string login // login del agente
            , string vdn  // vdn
            , string telefono  // telefono del cliente
            , decimal? contactoId = 0
            , string  sourceId = null
            , string customdata1 = null
            , string customdata2 = null
            , string customdata3 = null)
        {


            string hostname = "sin nombre";

            if (customdata1 == "0")
                customdata1 = null;
            try
            {

                string[] computer_name = System.Net.Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName.Split(new Char[] { '.' });
                String ecname = System.Environment.MachineName;
                hostname = computer_name[0];
            }
            catch (Exception ex)
            {

            }

            string ip = Request.ServerVariables["REMOTE_ADDR"];

            ViewBag.productos = db.Productos.Where(c => c.Estado == true && c.CreatedAt != null).ToList();
            ViewBag.login = login;
            Gestion gestion = null;
            string sourceid = id;
            bool isInbound = false;
            decimal? idPresence = contactoId;
            DLog("ingresando a la gestion");
            DLog("sourceId " + sourceid);
            DLog("login " + login);


            int newVdn = 0;
            Int32.TryParse(vdn, out newVdn);

            CustoErrors Error = null;
            User Asesor = db.Usuario.Where(c => c.login == login && login != null && c.Status == true).FirstOrDefault();
            CampanasVdns FilaCampanaVdn = db.CampanasVdns.Find(newVdn);

            /*----------------------------------------*/
            /*-| VALIDAR QUE EL LOGIN EXISTA.
            /*----------------------------------------*/
            if (Asesor == null)
            {
                Error = new CustoErrors()
                {
                    Name = "Login",
                    Message = "El login no existe"
                };

                return View("index", Error);
            }


            if (FilaCampanaVdn == null)
            {
                Error = new CustoErrors()
                {
                    Name = "Vdn",
                    Message = "El VDN no existe"
                };

            }





            /*----------------------------------------*/
            /*-| CREO O OBTENGO LA VARIABLE DE SESSION
            /*----------------------------------------*/
            int IdGestion = 0;
            bool SessionActive = true;
            // creo la variable de session


            var idGestionSession = System.Web.HttpContext.Current.Session["idGestion"];
            var idsourceIdSession = System.Web.HttpContext.Current.Session["sourceId"];


            if ((idGestionSession == null
                || idsourceIdSession == null
                || idsourceIdSession != sourceid) 
                && string.IsNullOrEmpty(customdata1)
                )
            {
                System.Web.HttpContext.Current.Session["sourceId"] = sourceid;
                SessionActive = false;
            }
            //recupero la variable idgestion
            else
            {

                if(string.IsNullOrEmpty(customdata1))
                IdGestion = int.Parse(System.Web.HttpContext.Current.Session["idGestion"].ToString());
                else
                IdGestion = int.Parse(customdata1);
            }
            int Id = 1;
            Int32.TryParse(sourceid, out Id);


            Cliente cliente = null;

          
         
            // CUSTOMDATA2
            if (!string.IsNullOrEmpty(customdata2))
            {
                
                ViewBag.customData2 = customdata2.ToString();               
            }
          

            // busca el cliente si no existe.
            if ((sourceid != "0" && !String.IsNullOrEmpty(sourceid) ) || !string.IsNullOrEmpty(customdata3))
            {

                if (!string.IsNullOrEmpty(customdata3))
                    Int32.TryParse(customdata3, out Id);

                cliente = db.cliente.Include(J => J.Carga).Where(c => c.Id == Id).FirstOrDefault();
            }
               
            // SE TRAE EL CLIENTE PARA SER EDITADO.
            if (!string.IsNullOrEmpty(customdata1))
            {

                gestion = db.Gestion.Find(IdGestion);
                if(gestion != null)
                    cliente = db.cliente.Include(J => J.Carga).Where(c => c.Id == gestion.ClientId).FirstOrDefault();
            }
            // busca por telefono.
            if (cliente == null && !String.IsNullOrEmpty(telefono))
            {
                cliente = db.cliente.Include(J => J.Carga).Where(c =>
                 c.TELEFONO_1 == telefono
                 || c.TELEFONO_2 == telefono
                 || c.TELEFONO_3 == telefono
                 || c.CELULAR_1 == telefono
                 || c.CELULAR_2 == telefono
                 ).OrderByDescending(c => c.CargaId).FirstOrDefault();
            }

            if (cliente == null)
            {
                cliente = db.cliente.Include(J => J.Carga).Where(c => c.Id == 1).First();
            }


            /*-------------------------*/
            /*-| CREACIÓN DE LA GESTION
            /*-------------------------*/

            if(FilaCampanaVdn.CanalIngreso == "INBOUND")
            {
                isInbound = true;
            }
            
            string campana = FilaCampanaVdn.CanalIngreso;
            int cargaId = isInbound ? 29 : cliente.CargaId;

            

            if (SessionActive == false && gestion == null)
            {
                gestion = new Gestion()
                {
                    EstadoId = _EstadosGestion.EnGestion,
                    captura_Nombre = cliente.NOMBRE_CLIENTE,
                    captura_Nombre1 = "",
                    ClientId = cliente.Id,
                    vdnLlamada = vdn,
                    TelefonoLlamada = telefono,
                    VENTA_TOMAS_ADICIONALES_TV = sourceId,
                    presence_Id = idPresence,
                    productoIdLlamada = cliente.car_ProductoId,
                    CreatedBy = Asesor.Id,
                    captura_docCliente = cliente.NUMERO_IDENTIFICACION,
                    Ip = ip,
                    MacchineName = hostname,
                    TipoServicioCamapa = campana,
                    CargaId = cargaId
                };
                db.Gestion.Add(gestion);
                db.SaveChanges();
                System.Web.HttpContext.Current.Session["idGestion"] = gestion.Id.ToString();

            }
            else
            {
                if(gestion == null)
                gestion = db.Gestion.Find(IdGestion);
            }


            GestionVM Modelo = new GestionVM
            {
                cliente = cliente,
                Gestion = gestion,
                Asesor = Asesor
            };
            ViewBag.DetalleVentas = db.DetalleVenta.Where(c => c.GestionId == Modelo.Gestion.Id).ToList();
            ViewBag.sourceId = sourceId;



            if (cliente.Id == 1 && !isInbound)
            {
                return View("~/Views/Home/NoExiste.cshtml", Modelo);
            }
            if (cliente.EstadoId == _EstadoCliente.Inactivo)
            {
                return View("~/Views/Home/ClienteBloqueado.cshtml", Modelo);
            }
            /*-------------------------*/
            /*-| VARIABLE DE LA VISTA
            /*-------------------------*/
     

            ViewBag.ciudad = new SelectList(db.Ciudad.Include(c => c.State).Select(c => new { c.Id, Name = c.Name + " (" + c.State.Name + " )", c.Status })
                .Where(c => c.Status == true), "Id", "Name");


            //ViewBag.Estratos = db.Configurations
            //    .Where(c => c.Status == true && c.CategoriesId == new Guid("65F56BE4-1A43-438D-8F1C-1FEEBD6733EB"))
            //    .OrderBy(c => c.OrderId).ToList();


            ViewBag.Configuraciones = db.Configurations
                .Where(c => c.Status == true)
                .OrderBy(c => new { c.CategoriesId, c.OrderId })
                .ToList();

            ViewBag.vdnsConfiguracion = FilaCampanaVdn;


            // TIPIFICACIONES PARA EL SERVICIO DE INBOUND.
            if (isInbound && vdn == "7777")
            {
                ViewBag.TipificacionId = db.Tipificacions.Where(c => c.Estado == true && c.productoId == 6 && c.padreId == 0).ToList();
            }
            else if(vdn == "7621" || vdn == "7624" || vdn == "7625")
            {
                ViewBag.TipificacionId = db.Tipificacions.Where(c => c.Estado == true && c.productoId == 8 && c.padreId == 0).ToList();
            }else if(vdn == "7622" || vdn == "7623" || vdn == "7620" || vdn == "7626" || vdn == "7619")
            {
                ViewBag.TipificacionId = db.Tipificacions.Where(c => c.Estado == true && c.productoId == 9 && c.padreId == 0).ToList();
            }else
            {
                ViewBag.TipificacionId = db.Tipificacions.Where(c => c.Estado == true && c.productoId == Modelo.cliente.car_ProductoId && c.padreId == 0).ToList();
            }


            /*========================================================================*/
            /*-| PRODUCTOS A OFRESER
            /*========================================================================-*/





            if (!string.IsNullOrEmpty(customdata1) && 1 == 2)
            {
                // recuperar digitacion.
                // recuperar Rechazada.
                if (!_EstadosGestion.LEstadosRecuperarAgente.Contains(gestion.EstadoId) || gestion.CaidaRecuperacionAgenteEn.Value.AddDays(1) < DateTime.Now)
                {
                    return View("~/Views/Home/noRecuperable.cshtml", Modelo);
                }
                ViewBag.ItemsVenta = db.DetalleVenta.Where(c => c.GestionId == gestion.Id).ToList();

                return View("~/Views/Home/GestionEditable.cshtml", Modelo);
            }
            else
            {


                /*--------------------------------*/
                /*-| LISTA DE VENTAS POR RECUPERAR
                /*-------------------------------*/
                ViewBag.porRecuperar =  db.Gestion.Include(c => c.Estado)
                    .Where(c =>
                     _EstadosGestion.LEstadosRecuperarAgente.Contains(c.EstadoId)

                    && c.cantadaPor == Asesor.Id
                    ).ToList()
                   .Where(c => c.CaidaRecuperacionAgenteEn.Value.AddDays(1) >= DateTime.Now).Count();

                return View(Modelo);

            }
        }


        public ActionResult Fin(string id = "pendiente por generar", string tipificacion = "", string fechaProgramada = "20190206 11:43",bool Venta = false, int? Programado=0)
        {
            ViewBag.Message = id;
            ViewBag.Tipificacion = tipificacion;
            ViewBag.fechaProgramada = fechaProgramada;
            ViewBag.Venta = Venta;
            ViewBag.Programado = Programado;
            return View();
        }

        /*
         * DEBUG FUNTION.
         */
        private void DLog(string msg = "")
        {
            
            Debug.WriteLine("-------------------------------------------------------\n");
            Debug.WriteLine(msg + "\n");
            Debug.WriteLine("-------------------------------------------------------\n");
        }



    }
}