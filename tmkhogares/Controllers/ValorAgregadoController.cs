﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Security;
using Core.Models.Servicios;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class ValorAgregadoController : Controller
    {
        private Contexto db = new Contexto();

        // GET: ValorAgregado
        public ActionResult Index()
        {
            return View(db.ValorAgregado.ToList());
        }

        // GET: ValorAgregado/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValorAgregado valorAgregado = db.ValorAgregado.Find(id);
            if (valorAgregado == null)
            {
                return HttpNotFound();
            }
            return View(valorAgregado);
        }

        // GET: ValorAgregado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ValorAgregado/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,EstadoId,cantidad,costo,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ValorAgregado valorAgregado)
        {
            if (ModelState.IsValid)
            {
                valorAgregado.Id = Guid.NewGuid();
                db.ValorAgregado.Add(valorAgregado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(valorAgregado);
        }

        // GET: ValorAgregado/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValorAgregado valorAgregado = db.ValorAgregado.Find(id);
            if (valorAgregado == null)
            {
                return HttpNotFound();
            }
            return View(valorAgregado);
        }

        // POST: ValorAgregado/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,EstadoId,cantidad,costo,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ValorAgregado valorAgregado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(valorAgregado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(valorAgregado);
        }

        // GET: ValorAgregado/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValorAgregado valorAgregado = db.ValorAgregado.Find(id);
            if (valorAgregado == null)
            {
                return HttpNotFound();
            }
            return View(valorAgregado);
        }

        // POST: ValorAgregado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ValorAgregado valorAgregado = db.ValorAgregado.Find(id);
            db.ValorAgregado.Remove(valorAgregado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
