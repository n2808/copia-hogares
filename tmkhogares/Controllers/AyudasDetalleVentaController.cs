﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Gestion;
using Core.Models.Security;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class AyudasDetalleVentaController : Controller
    {
        private Contexto db = new Contexto();

        // GET: AyudasDetalleVenta
        public ActionResult Index()
        {
            var detalleVenta = db.DetalleVenta.Include(d => d.Gestion);
            return View(detalleVenta.ToList());
        }

        public JsonResult AdionarProducto(DetalleVenta detalleGestion, List<string> D_NombreServici, List<int> D_NumeroServicios, List<int> D_TipoServicio, List<string> D_Ba, List<string> D_Tv)
        {


            db.DetalleVenta.Add(detalleGestion);
            db.SaveChanges();

            /*
                * CODIGO PARA HABILITAR EL DETALLE DE LA GESTION.
             */
            var i = 0;
            if(D_NombreServici != null)
            {
                foreach (var D in D_NombreServici)
                {
                    db.ProductosDetalleVenta.Add(new ProductosDetalleVenta()
                    {
                        NombreServicio = D,
                        tipoServicio = D_TipoServicio[i],
                        DetalleventaId = detalleGestion.Id,
                        NumeroServicios = D_NumeroServicios[i],
                        Ba = D_Ba[i],
                        Tv = D_Tv[i]

                    });
                    db.SaveChanges();
                    i = i + 1;
                }


            }


            List<DetalleVenta> DetalleGestion = db.DetalleVenta.Where(c => c.GestionId == detalleGestion.GestionId).ToList();
            return Json(new { status = 201, message = DetalleGestion });
        }

        public JsonResult RemoverProducto(int Id)
        {

            var p = db.DetalleVenta.Find(Id);

            if (p != null)
            {

                var listproDetalle = db.ProductosDetalleVenta.Where(c => c.DetalleventaId == p.Id);

                db.ProductosDetalleVenta.RemoveRange(listproDetalle);
                db.SaveChanges();

                db.DetalleVenta.Remove(p);
                db.SaveChanges();
                List<DetalleVenta> DetalleGestion = db.DetalleVenta.Where(c => c.GestionId == p.GestionId).ToList();
                return Json(new { status = 201, message = DetalleGestion });

            }
            return Json(new { status = 400 });
        }



        /*
            * Carga la vista para modificar los datos del Agente
         */
        [CustomAuthorize(Roles = "BackOffice,Administrador")]
        public ActionResult ModificarGestionBack(int id = 0)
        {
            var Gestion =  db.Gestion.Include(c => c.cantadaPor__Agente).Where(c => c.Id == id).FirstOrDefault();
            
            ViewBag.ciudad = new SelectList(db.Ciudad.Include(c => c.State).Select(c => new { c.Id, Name = c.Name + " (" + c.State.Name + " )", c.Status }).Where(c => c.Status == true), "Id", "Name");

            ViewBag.Configuraciones = db.Configurations
                .Where(c => c.Status == true)
                .OrderBy(c => new { c.CategoriesId, c.OrderId })
                .ToList();
            ViewBag.DetalleVentas = db.DetalleVenta.Where(c => c.GestionId == id).ToList();
            return View(Gestion);
        }


        /*
            * Vista para 
         */
         [Route("app/cotizador")]
        public ActionResult CotizadorAgente(int id = 0)
        {

            ViewBag.ciudad = new SelectList(db.Ciudad.Include(c => c.State).Select(c => new { c.Id, Name = c.Name + " (" + c.State.Name + " )", c.Status }).Where(c => c.Status == true), "Id", "Name");

            ViewBag.Configuraciones = db.Configurations
                .Where(c => c.Status == true)
                .OrderBy(c => new { c.CategoriesId, c.OrderId })
                .ToList();
            return View();
        }



        /*
            * Carga la vista para modificar los datos del Agente
         */
        public JsonResult ActualizarFichaBack(Gestion gestion)
        {

            try
            {
                var dbGestion = db.Gestion.Find(gestion.Id);

                dbGestion.VENTA_NOMBRE_COMPLETO_CLIENTE = gestion.VENTA_NOMBRE_COMPLETO_CLIENTE;
                dbGestion.VENTA_NUMERO_CEDULA = gestion.VENTA_NUMERO_CEDULA;
                dbGestion.VENTA_CUENTA_VENTA = gestion.VENTA_CUENTA_VENTA;
                dbGestion.captura_Direccion = gestion.captura_Direccion;
                dbGestion.captura_telefono1 = gestion.captura_telefono1;
                dbGestion.captura_telefono2 = gestion.captura_telefono2;
                dbGestion.captura_barrio = gestion.captura_barrio;
                db.Entry(dbGestion).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { status = 200 });
            }
            catch (Exception e)
            {
                return Json(new { status = 200 });

            }


         
         
        }






        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
