﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class PlanesEspecificosController : Controller
    {
        private Contexto db = new Contexto();

        // POST: PlanesEspecificos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,TarifaPlenaId,CampanaId,CEstrato1,CEstrato2,CEstrato3,CEstrato4,CEstrato5,CEstrato6,CSEstrato1,CSEstrato2,CSEstrato3,CSEstrato4,CSEstrato5,CSEstrato6,VEstrato1,VEstrato2,VEstrato3,VEstrato4,VEstrato5,VEstrato6,velocidad,velocidadId,TvId,lineaId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PlanesEspecificos planesEspecificos)
        {
            if (ModelState.IsValid)
            {
                //if(db.PlanesEspecificos.Where( c=> c.TarifaPlenaId == planesEspecificos.TarifaPlenaId && c.CampanaId == planesEspecificos.CampanaId && c.DeletedAt == null ).ToList().Count > 0 )
                //{
                //    return Json(new { status = 409, message = "No se ha podido Agregar El plan" });
                //}
                db.PlanesEspecificos.Add(planesEspecificos);
                db.SaveChanges();
                return Json(new { status = 201, message = db.PlanesEspecificos.Include(plan => plan.TarifaPlena).Where(c => c.Id == planesEspecificos.Id).FirstOrDefault() });
            }
            var errorList = (from item in ModelState
                             where item.Value.Errors.Any()
                             select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
          
          
            
            return Json(new { status = 400, message = errorList });
        }



        // POST: PlanesEspecificos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(bool Estado ,int Id)
        {

            PlanesEspecificos Plan = db.PlanesEspecificos.Find(Id);

            if (Plan == null)
            {
                return Json(new { status = 404 });

            }
            else
            {
                if (Estado == false)
                    Plan.DeletedAt = DateTime.Now;
                else
                    Plan.DeletedAt = null;
                db.Entry(Plan).State = EntityState.Modified;
                db.SaveChanges();

            }

            return Json(new { status = 200 });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
