﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Security;
using Core.Models.User;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class UsersController : Controller
    {
        private Contexto db = new Contexto();

        /*------------------------------------------------------*
        * ADMINISTRACION DE USUARIOS
        *------------------------------------------------------*/

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Usuario.Where(c => c.Status == true).ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Document,Names,LastName,Phone1,Phone2,Phone3,Email,Status,PassWord,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Id = Guid.NewGuid();
                db.Usuario.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Document,Names,LastName,Phone1,Phone2,Phone3,Email,Status,PassWord,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            User user = db.Usuario.Find(id);
            db.Usuario.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*------------------------------------------------------*
        * ADMINISTRACION DE ROLES
        *------------------------------------------------------*/
        
        public ActionResult Roles(Guid Id)
        {
            User Usuario = db.Usuario.Find(Id);
            if (Id == null || Usuario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Name = Usuario.Names;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles([Bind(Include = "Id,RolId,UserId")] UserRol userRol, string Name)
        {
            if (ModelState.IsValid)
            {
                var Exist = db.UsuariosRoles
                    .Where(
                    c => c.UserId == userRol.UserId&&
                    c.RolId == userRol.RolId
                ).FirstOrDefault();

                if (Exist != null)
                {
                    ModelState.AddModelError("RolId", "este permiso ya fue asignado");
                }
                else
                {
                    userRol.Id = Guid.NewGuid();
                    db.UsuariosRoles.Add(userRol);
                    db.SaveChanges();
                    return RedirectToAction("Roles", new { Id = userRol.UserId});
                }


            }
            var Id = userRol.UserId;
            ViewBag.Name = Name;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View("Roles");
        }

        // POST: UserOfPointOfCares/Delete/5
        [HttpPost, ActionName("DeleteRol")]
        [ValidateAntiForgeryToken]
        public ActionResult RolesDeleteConfirmed(Guid id)
        {
            UserRol UsusarioRol = db.UsuariosRoles.Find(id);
            var UserId = UsusarioRol.UserId;
            db.UsuariosRoles.Remove(UsusarioRol);
            db.SaveChanges();
            return RedirectToAction("Roles", new { Id = UserId });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
