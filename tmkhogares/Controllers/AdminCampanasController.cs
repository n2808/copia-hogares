﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using Core.Models.Security;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class AdminCampanasController : Controller
    {
        private Contexto db = new Contexto();

        // GET: AdminCampanas
        public ActionResult Index(bool? Estado = true)
        {
            var campanas = db.Campanas.Include(c => c.TipoCiudades).Include(c => c.TipoOferta).Include(c => c.TipoProducto).Where(c => c.Estado == true).OrderBy( c => c.DescripcionCorta);
            return View(campanas.ToList());
        }


        public ActionResult duplicate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Duplicar la campña y entidades
            Campana Campana = db.Campanas.Find(id);
            Campana.UpdatedAt = DateTime.Now;
            db.Campanas.Add(Campana);
            db.SaveChanges();
            int NuevoId = Campana.Id;

            // duplicando la lista de ciudades
            List<CiudadCampana> ciudadesCampana = db.CiudadCampanas.Where(c => c.CampanaId == id).ToList();
            ciudadesCampana.ForEach(c => c.CampanaId = NuevoId);
            if (ciudadesCampana.Count > 0)
            {
                db.CiudadCampanas.AddRange(ciudadesCampana);
                db.SaveChanges();
            }

            // duplicando la lista de paquetes 

            List<PaquetesEspecificos> paquetes = db.PaquetesEspecificos.Where(c => c.CampanaId == id).ToList();
            paquetes.ForEach(c => c.CampanaId = NuevoId);
            if (paquetes.Count > 0)
            {
                db.PaquetesEspecificos.AddRange(paquetes);
                db.SaveChanges();
            }


            //duplicando la lista de planes especificos
            List<PlanesEspecificos> planesEspecificos = db.PlanesEspecificos.Where(c => c.CampanaId == id).ToList();
            planesEspecificos.ForEach(c => c.CampanaId = NuevoId);
            if (planesEspecificos.Count > 0)
            {
                db.PlanesEspecificos.AddRange(planesEspecificos);
                db.SaveChanges();
            }



            //duplicando la lista de descuentos secuenciales.
            List<DescuentoSecuencial> descuentoSecuencials = db.DescuentoSecuencial.Where(c => c.CampanaId == id).ToList();
            descuentoSecuencials.ForEach(c => c.CampanaId = NuevoId);
            if (descuentoSecuencials.Count > 0)
            {
                db.DescuentoSecuencial.AddRange(descuentoSecuencials);
                db.SaveChanges();
            }

            var modificarCampana = db.Campanas.Find(id);
            modificarCampana.Estado = false;
            db.Entry(modificarCampana).State = EntityState.Modified;
            db.SaveChanges();


            return RedirectToAction("Index");
        }



        // GET: AdminCampanas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campanas.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

        // GET: AdminCampanas/Create
        public ActionResult Create()
        {
            ViewBag.TipoCiudadesId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_tiposCiudades), "Id", "Name");
            ViewBag.TipoOfertaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoOferta), "Id", "Name");
            ViewBag.TipoProductoId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoProducto), "Id", "Name");
            ViewBag.Configuraciones = db.Configurations.Where(c => c.Status == true).ToList();
            return View();
        }

        // POST: AdminCampanas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,DescripcionCorta,DescripcionLarga,Estado,TipoCiudadesId,TipoProductoId,Estratos,ClienteFijo,ClienteMovil,ClienteNuevo,TipoOfertaId,percentValue,percentBaVelocity,percentBaVelocityClienteMovil,percentValueClienteMovil,Campana_producto,Campana_Tipo,TipoPtar,Ptars,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Campana campana)
        {
            if (ModelState.IsValid)
            {
                campana.Estado = true;
                db.Campanas.Add(campana);
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = campana.Id });
            }

            ViewBag.TipoCiudadesId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_tiposCiudades), "Id", "Name", campana.TipoCiudadesId);
            ViewBag.TipoOfertaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoOferta), "Id", "Name", campana.TipoOfertaId);
            ViewBag.TipoProductoId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoProducto), "Id", "Name", campana.TipoProductoId);
            ViewBag.Configuraciones = db.Configurations.Where(c => c.Status == true).ToList();
            return View(campana);
        }

        // GET: AdminCampanas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campanas.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoCiudadesId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_tiposCiudades), "Id", "Name", campana.TipoCiudadesId);
            ViewBag.TipoOfertaId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoOferta), "Id", "Name", campana.TipoOfertaId);
            ViewBag.TipoProductoId = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoProducto), "Id", "Name", campana.TipoProductoId);
            ViewBag.Configuraciones = db.Configurations.Where(c => c.Status == true).ToList();

            ViewBag.TarifaPlenaId = new SelectList(db.TarifaPlena, "Id", "Nombre", null);
            ViewBag.PaquetesEspecificos = db.PaquetesEspecificos.Where(paquete => paquete.CampanaId == id).FirstOrDefault();
            ViewBag.CiudadCampanas = db.CiudadCampanas.Include(c => c.Ciudad.State).Where(paquete => paquete.CampanaId == id).OrderBy(c => c.Ciudad.State.Name).ToList();
            ViewBag.DescuentoSecuencial = db.DescuentoSecuencial.Where(paquete => paquete.CampanaId == id).ToList();
            ViewBag.PlanesEspecificos = db.PlanesEspecificos.Include(planes => planes.TarifaPlena).Where(paquete => paquete.CampanaId == id).ToList();
            ViewBag.ciudad = new SelectList(db.Ciudad.Include(c => c.State).Select(c => new { c.Id, Name = c.Name + " (" + c.State.Name + " )", c.Status }).Where(c => c.Status == true), "Id", "Name", null);
            return View(campana);
        }

        // POST: AdminCampanas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,DescripcionCorta,DescripcionLarga,Estado,TipoCiudadesId,TipoProductoId,Estratos,ClienteFijo,ClienteMovil,ClienteNuevo,TipoOfertaId,percentValue,percentBaVelocity,percentBaVelocityClienteMovil,percentValueClienteMovil,Campana_producto,Campana_Tipo,TipoPtar,Ptars,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Campana campana)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campana).State = EntityState.Modified;
                db.SaveChanges();
                
            }
            ViewBag.TipoCiudadesId  = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_tiposCiudades), "Id", "Name", campana.TipoCiudadesId);
            ViewBag.TipoOfertaId    = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoOferta), "Id", "Name", campana.TipoOfertaId);
            ViewBag.TipoProductoId  = new SelectList(db.Configurations.Where(c => c.CategoriesId == _Configuraciones.Campana_TipoProducto), "Id", "Name", campana.TipoProductoId);
            ViewBag.Configuraciones = db.Configurations.Where(c => c.Status == true).ToList();
            ViewBag.PaquetesEspecificos = db.PaquetesEspecificos.Where(paquete => paquete.CampanaId == campana.Id).FirstOrDefault();
            ViewBag.TarifaPlenaId = new SelectList(db.TarifaPlena, "Id", "Nombre", null);
            ViewBag.CiudadCampanas = db.CiudadCampanas.Include(c => c.Ciudad.State).Where(paquete => paquete.CampanaId == campana.Id).OrderBy(c => c.Ciudad.State.Name).ToList();
            ViewBag.DescuentoSecuencial = db.DescuentoSecuencial.Where(paquete => paquete.CampanaId == campana.Id).ToList();
            ViewBag.PlanesEspecificos = db.PlanesEspecificos.Include(planes => planes.TarifaPlena).Where(paquete => paquete.CampanaId == campana.Id).OrderBy(c => c.TarifaPlena.Id).ToList();
            ViewBag.ciudad = new SelectList(db.Ciudad.Include(c => c.State).Select(c => new { c.Id, Name = c.Name + " (" + c.State.Name + " )", c.Status }).Where(c => c.Status == true), "Id", "Name", null);
            return View(campana);
        }

        // GET: AdminCampanas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campanas.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

        // POST: AdminCampanas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Campana campana = db.Campanas.Find(id);
            db.Campanas.Remove(campana);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
