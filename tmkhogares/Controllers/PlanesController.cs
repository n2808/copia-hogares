﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Security;
using Core.Models.Servicios;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class PlanesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Planes
        public ActionResult Index()
        {
            var planes = db.Planes.Include(p => p.Servicio).Where(c => c.EstadoId == true);
            return View(planes.ToList());
        }

        // GET: Planes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plan plan = db.Planes.Find(id);
            if (plan == null)
            {
                return HttpNotFound();
            }
            return View(plan);
        }

        // GET: Planes/Create
        public ActionResult Create()
        {
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre");
            ViewBag.EstratoId = db.Configurations.Where(c => c.CategoriesId == new Guid("65F56BE4-1A43-438D-8F1C-1FEEBD6733EB")).OrderBy(c => c.OrderId);
            return View();
        }

        // POST: Planes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,EstadoId,TipoTv,tipoBa,ServicioId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Plan plan , List<Guid> EstratoId = null ,List<string> valorId = null)
        {
            if(EstratoId == null)
            {
                ModelState.AddModelError("plan", "Se debe seleccionar un estrato");
            }

            if (ModelState.IsValid)
            {
                plan.Id = Guid.NewGuid();
                db.Planes.Add(plan);
                db.SaveChanges();
                int loop = 0;
                foreach (var Estrato in EstratoId )
                {
                    var valor = valorId[loop];
                    if (valor != "" )
                    using(Contexto db1 =  new Contexto()){
                        db1.PlanesEstratos.Add(new PlanesEstratos() { EstratoId = Estrato, PlanId = plan.Id , Costo = valor });
                        db1.SaveChanges();
                    }

                    loop++;

                }


                return RedirectToAction("Index");
            }

            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre", plan.ServicioId);
            ViewBag.EstratoId = db.Configurations.Where(c => c.CategoriesId == new Guid("65F56BE4-1A43-438D-8F1C-1FEEBD6733EB")).OrderBy(c => c.OrderId);
            return View(plan);
        }

        // GET: Planes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plan plan = db.Planes.Find(id);
            if (plan == null)
            {
                return HttpNotFound();
            }
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre", plan.ServicioId);
            ViewBag.EstratoId = db.Configurations.Where(c => c.CategoriesId == new Guid("65F56BE4-1A43-438D-8F1C-1FEEBD6733EB")).OrderBy(c => c.OrderId);
            ViewBag.planesEstrato = db.PlanesEstratos.Where(c => c.PlanId == id).ToList();
            return View(plan);
        }

        // POST: Planes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,EstadoId,TipoTv,tipoBa,ServicioId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Plan plan, List<Guid> EstratoId = null, List<string> valorId = null)
        {
            if (EstratoId == null)
            {
                ModelState.AddModelError("plan", "Se debe seleccionar un estrato");
            }


            if (ModelState.IsValid)
            {
                db.Entry(plan).State = EntityState.Modified;
                db.SaveChanges();


                using (var db1 = new Contexto())
                {
                    var parent = db1.Planes.Include(p => p.PlanesEstratos)
                        .SingleOrDefault(p => p.Id == plan.Id);

                    //foreach (var child in parent.PlanesEstratos.ToList())
                    //    db1.PlanesEstratos.Remove(child);
                    //db1.SaveChanges();

                    db1.PlanesEstratos.RemoveRange(parent.PlanesEstratos);
                    db1.SaveChanges();
                }


                int loop = 0;
                foreach (var Estrato in EstratoId)
                {
                    var valor = valorId[loop];
                    if (valor != "")
                        using (Contexto db1 = new Contexto())
                        {
                            db1.PlanesEstratos.Add(new PlanesEstratos() { EstratoId = Estrato, PlanId = plan.Id, Costo = valor });
                            db1.SaveChanges();
                        }

                    loop++;

                }


                return RedirectToAction("Index");
            }
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre", plan.ServicioId);
            ViewBag.EstratoId = db.Configurations.Where(c => c.CategoriesId == new Guid("65F56BE4-1A43-438D-8F1C-1FEEBD6733EB")).OrderBy(c => c.OrderId);
            ViewBag.planesEstrato = db.PlanesEstratos.Where(c => c.PlanId == plan.Id).ToList();
            return View(plan);
        }

        // GET: Planes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plan plan = db.Planes.Find(id);
            if (plan == null)
            {
                return HttpNotFound();
            }
            return View(plan);
        }

        // POST: Planes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Plan plan = db.Planes.Find(id);
            db.Planes.Remove(plan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
