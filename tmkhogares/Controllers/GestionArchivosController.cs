﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tmkhogares.Controllers
{
    public class GestionArchivosController : Controller
    {
        // GET: GestionArchivos
        public JsonResult CargarEvidenciaGestion(int idGestion, string tipoEvidencia)
        {
            string pathValue = System.Configuration.ConfigurationManager.AppSettings["RutaServidorEvidencias"];
            try
            {

                if (Request.Files.Count > 0)
                {
                    var test = Request.Files[0];

                    string nombreArchivo = "Gestion_" + idGestion + "_tipo_";

                    nombreArchivo = nombreArchivo + tipoEvidencia;
                    nombreArchivo = nombreArchivo + ".jpg";

                    string nombre_archivo = pathValue + nombreArchivo;
                    //guardar imagen en ruta compartida
                    test.SaveAs(nombre_archivo);

                }

            }
            catch (Exception ex)
            {
                return Json(new { type = "ERROR", message = "La evidencia no se pudo cargar " }, JsonRequestBehavior.DenyGet);
            }

            return Json(new { type = "OK", message = "El archivo se cargo correctamente" }, JsonRequestBehavior.DenyGet);
        }





    }




}