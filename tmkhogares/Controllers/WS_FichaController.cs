﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class WS_FichaController : Controller
    {
        private Contexto db = new Contexto();


        // obtener la lista de productos ofrecidos
        public JsonResult getOfertas(Guid TipoVentaId)
        {
            return Json(db.ProductosOfrecidos.Where(c => c.TipoVentaId == TipoVentaId).ToList());
        }


        // obtiene los servicios de una oferta especifica.
        public JsonResult GetServicioAdicional(int productoPrincipal, int? productoId)
        {
            var ServicioAdicional = db.TarifaAdicionales.Where(c => c.productoId == null || c.productoId == productoId);


            return Json(ServicioAdicional.Where(c => c.ProductosOfrecidosId == productoPrincipal).ToList());
        }


        //public JsonResult getPlanesPrinciaples(Guid State)
        //{
        //     var planes = db.Planes
        //        .Include(c => c.PlanesEstratos).Include(c => c.Servicio)
        //        .Where(c => c.EstadoId == true)
        //        .OrderBy(c => c.Servicio.CreatedAt).ToList();

        //    return Json(planes);
        //}


        //public JsonResult getProductosNoPrincipales(Guid State)
        //{

        //    var producto = db.ValorAgregado
        //    .Where(c => c.EstadoId== true).ToList();
        //    return Json(db.Ciudad.Where(c => c.StateId == State));
        //}


    }
}