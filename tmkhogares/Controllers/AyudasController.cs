﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    public class AyudasController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Ayudas
        // obtener la lista de ciudades
        public JsonResult Getplanes(int Tipo = 2)
        {
            if(Tipo != 1)
            {
                Tipo = 2;
            }

            var configuracionesPrincipales = db.Configurations
                .Where(c => c.CategoriesId == _Configuraciones.PlanesdeTv 
                            ||  c.CategoriesId == _Configuraciones.Megasdevelocidad  
                            || c.CategoriesId == _Configuraciones.planesMinutos)
               .ToList();
            AyudaVentaVM ayudaventa = new AyudaVentaVM()
            {
                TarifasPlenas = db.TarifaPlena.Include( c => c.TipoPlan).Where(c => c.Estado == true && (c.ProductoId == Tipo || c.ProductoId == null)).ToList(),
                Campanas = db.Campanas.Where(c => c.Estado == true && (c.Campana_producto == Tipo || c.Campana_producto == null)).ToList(),
                CiudadesCampanas = db.CiudadCampanas.Where(c => c.Campana.Estado == true && (c.Campana.Campana_producto == Tipo || c.Campana.Campana_producto == null)).ToList(),
                PaquetesEspecificos = db.PaquetesEspecificos.Where(c => c.Campana.Estado == true && c.Campana.Campana_producto == Tipo || c.Campana.Campana_producto == null).ToList(),
                PlanesEspecificos = db.PlanesEspecificos.Include(c => c.TarifaPlena).Where(c => c.Campana.Estado == true && ( c.Campana.Campana_producto == Tipo || c.Campana.Campana_producto == null)   && c.DeletedAt == null).ToList(),
                DescuentoSecuencial = db.DescuentoSecuencial.Where(c => c.Campana.Estado == true && (c.Campana.Campana_producto == Tipo || c.Campana.Campana_producto == null)).ToList(),
                TarifaAdicionales = db.TarifaAdicionales.Where(c => c.DeletedAt == null).ToList()

            };

            if(Tipo == 1)
            {
                ayudaventa.listaBa = configuracionesPrincipales.Where(con => con.IntId == 1 && con.Status == true  && con.CategoriesId == _Configuraciones.Megasdevelocidad).ToList();
                ayudaventa.listaTv = configuracionesPrincipales.Where(con => con.IntId == 1 && con.Status == true && con.CategoriesId == _Configuraciones.PlanesdeTv).ToList();
                ayudaventa.listaLinea = configuracionesPrincipales.Where(con => con.IntId == 1 && con.Status == true && con.CategoriesId == _Configuraciones.planesMinutos).ToList();
                ayudaventa.ciudadesDTH = db.CiudadDTH.ToList();
            }
            else
            {
                ayudaventa.listaBa = configuracionesPrincipales.Where(con => con.IntId == 2 && con.Status == true && con.CategoriesId == _Configuraciones.Megasdevelocidad).ToList();
                ayudaventa.listaTv = configuracionesPrincipales.Where(con => con.IntId == 2 && con.Status == true && con.CategoriesId == _Configuraciones.PlanesdeTv).ToList();
                ayudaventa.listaLinea = configuracionesPrincipales.Where(con => con.IntId == 2 && con.Status == true && con.CategoriesId == _Configuraciones.planesMinutos).ToList();
            }

            return Json(ayudaventa);
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
