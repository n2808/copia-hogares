﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models.Security;


namespace ClaroGrandesSuperficies.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Index()
        {
            if (SessionPersister.Id != null)
                return RedirectToAction("HomeAdmin","Home",null);

            return View();
        }






        [HttpPost]
        public ActionResult Login(AccountsViewModel avm)
        {

            AccountsModel am = new AccountsModel();
            Accounts ResultUser = am.Login(avm.Accounts.UserName, avm.Accounts.Password);
            if (string.IsNullOrEmpty(avm.Accounts.UserName) || string.IsNullOrEmpty(avm.Accounts.Password) || ResultUser == null)
            {
                ViewBag.Error = "Usuario o contraseña incorrectos";
                return View("index");
            }
            // get user data for the session.
            SessionPersister.UserName = avm.Accounts.UserName;
            SessionPersister.Id = ResultUser.Id;
            
           

            return RedirectToAction("index");

        }

        public ActionResult Logout()
        {
            SessionPersister.UserName = string.Empty;
            SessionPersister.Id = null;
            return View("index");
        }


        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}