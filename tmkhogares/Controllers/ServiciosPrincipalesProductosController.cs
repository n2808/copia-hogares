﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Oferta;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class ServiciosPrincipalesProductosController : Controller
    {
        private Contexto db = new Contexto();

        // GET: ServiciosPrincipalesProductos
        public ActionResult Index()
        {
            var serviciosPrincipalesProductos = db.ServiciosPrincipalesProductos.Include(s => s.ProductoOfrecido).Include(s => s.Servicio);
            return View(serviciosPrincipalesProductos.ToList());
        }

        // GET: ServiciosPrincipalesProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiciosPrincipalesProductos serviciosPrincipalesProductos = db.ServiciosPrincipalesProductos.Find(id);
            if (serviciosPrincipalesProductos == null)
            {
                return HttpNotFound();
            }
            return View(serviciosPrincipalesProductos);
        }

        // GET: ServiciosPrincipalesProductos/Create
        public ActionResult Create()
        {
            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos.Include(c => c.TipoVenta).Select(c => new { c.Id , Nombre = c.Nombre +"_"+c.TipoVenta.Name }), "Id", "Nombre");
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre");
            return View();
        }

        // POST: ServiciosPrincipalesProductos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductoOfrecidoId,ServicioId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ServiciosPrincipalesProductos serviciosPrincipalesProductos)
        {
            if (ModelState.IsValid)
            {
                db.ServiciosPrincipalesProductos.Add(serviciosPrincipalesProductos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre", serviciosPrincipalesProductos.ProductoOfrecidoId);
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre", serviciosPrincipalesProductos.ServicioId);
            return View(serviciosPrincipalesProductos);
        }

        // GET: ServiciosPrincipalesProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiciosPrincipalesProductos serviciosPrincipalesProductos = db.ServiciosPrincipalesProductos.Find(id);
            if (serviciosPrincipalesProductos == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre", serviciosPrincipalesProductos.ProductoOfrecidoId);
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre", serviciosPrincipalesProductos.ServicioId);
            return View(serviciosPrincipalesProductos);
        }

        // POST: ServiciosPrincipalesProductos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductoOfrecidoId,ServicioId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ServiciosPrincipalesProductos serviciosPrincipalesProductos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviciosPrincipalesProductos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductoOfrecidoId = new SelectList(db.ProductosOfrecidos, "Id", "Nombre", serviciosPrincipalesProductos.ProductoOfrecidoId);
            ViewBag.ServicioId = new SelectList(db.Servicio, "Id", "Nombre", serviciosPrincipalesProductos.ServicioId);
            return View(serviciosPrincipalesProductos);
        }

        // GET: ServiciosPrincipalesProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiciosPrincipalesProductos serviciosPrincipalesProductos = db.ServiciosPrincipalesProductos.Find(id);
            if (serviciosPrincipalesProductos == null)
            {
                return HttpNotFound();
            }
            return View(serviciosPrincipalesProductos);
        }

        // POST: ServiciosPrincipalesProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiciosPrincipalesProductos serviciosPrincipalesProductos = db.ServiciosPrincipalesProductos.Find(id);
            db.ServiciosPrincipalesProductos.Remove(serviciosPrincipalesProductos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
