﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Sales;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    public class CiudadCampanasController : Controller
    {
        private Contexto db = new Contexto();


        // POST: CiudadCampanas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,CiudadId,CiudadNombre,CampanaId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] CiudadCampana ciudadCampana)
        {
            if (ModelState.IsValid)
            {
                db.CiudadCampanas.Add(ciudadCampana);
                db.SaveChanges();
                var Ciudad = db.CiudadCampanas.Include(c => c.Ciudad.State).Where(ciudad => ciudad.Id == ciudadCampana.Id).FirstOrDefault();
                return Json(new { status = 200 , message = Ciudad });

            }
            return Json(new { status = 403 , message =  "no se ha podido crear la ciudad" });
        }

        // POST: CiudadCampanas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            CiudadCampana ciudadCampana = db.CiudadCampanas.Find(id);

            if(ciudadCampana == null)
            {
                return Json(new { status = 201 });

            }
            else
            {
                db.CiudadCampanas.Remove(ciudadCampana);
                db.SaveChanges();

            }

            return Json(new { status = 200 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
