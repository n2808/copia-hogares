﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.location;
using Core.Models.Security;
using tmkhogares.Models;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class CiudadesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Ciudades
        public ActionResult Index()
        {
            var ciudad = db.Ciudad.Include(c => c.State).OrderBy(c => c.State.Name);
            return View(ciudad.ToList());
        }

        // GET: Ciudades/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Ciudad.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // GET: Ciudades/Create
        public ActionResult Create()
        {
            ViewBag.StateId = new SelectList(db.Departamento, "Id", "Name");
            return View();
        }

        // POST: Ciudades/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code,Status,StateId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] City city)
        {
            if (ModelState.IsValid)
            {
                city.Id = Guid.NewGuid();
                db.Ciudad.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StateId = new SelectList(db.Departamento, "Id", "Name", city.StateId);
            return View(city);
        }

        // GET: Ciudades/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Ciudad.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            ViewBag.StateId = new SelectList(db.Departamento, "Id", "Name", city.StateId);
            return View(city);
        }

        // POST: Ciudades/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Code,Status,StateId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StateId = new SelectList(db.Departamento, "Id", "Name", city.StateId);
            return View(city);
        }

        // GET: Ciudades/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Ciudad.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // POST: Ciudades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            City city = db.Ciudad.Find(id);
            db.Ciudad.Remove(city);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
