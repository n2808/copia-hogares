﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class GestionActivacion : EntityWithIntId
	{
        public DateTime fechaVenta { get; set; } = DateTime.Now;
        public Guid?  AgenteId { get; set; }
        public int GestionId { get; set; }
        public Guid? EstadosId { get; set; }

        [ForeignKey("EstadosId")]
        public Configuration Estado { get; set; }

        [ForeignKey("GestionId")]
        public Gestion Gestion { get; set; }

    }
}