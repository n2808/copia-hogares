﻿using Core.Models.Common;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class ProductosDetalleVenta : EntityWithIntId
    {

        /*
           * 1 - para upgrade, 2 para servicio nuevo.
         */
        public int tipoServicio { get; set; } = 1;

        /*
           * 1 - para upgrade, 2 para servicio nuevo.
         */
        public string NombreServicio { get; set; }

        public string tarifaServicio { get; set; }

        public int NumeroServicios { get; set; }

        public bool Mostrar { get; set; } = true;

        public int DetalleventaId { get; set; }

        /*
           * se almacena Megas contratadas y tv contratada.
         */

        public string Ba { get; set; }
        public string Tv { get; set; }


        [ForeignKey("DetalleventaId")]
        public DetalleVenta Detalleventa { get; set; }

    }
}