﻿using Core.Models.Common;
using Core.Models.configuration;
using Core.Models.location;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Core.Models.User;
using System.ComponentModel;

namespace Core.Models.Gestion
{
    public class Gestion : EntityWithIntId
    {
        public Guid EstadoId { get; set; } = new Guid("46e6db07-510a-4715-81ac-40cc4f7733bc");
        public int ClientId { get; set; }
        public string Ip { get; set; }
        public string MacchineName { get; set; }

        /*---------------------------------------------------------------------*
        * DATOS DE SEGUIMIENTO
        *-----------------------------------------------------------------------*/

        public Guid? ValidadoPor { get; set; }
        
        
        public Guid? soportadaPor { get; set; }
        public Guid? AuditadoPor { get; set; }
        public DateTime? soportadaEn { get; set; }
        public Guid? InstaladaActulizadoPor { get; set; }
        public DateTime? instaladaEn { get; set; }
        public DateTime? AuditadoEn { get; set; }


        // datos de la instalción

        
        public string seguimiento_can_Obser { get; set; }

        public DateTime? seguimiento_legalizacion { get; set; }

        public Guid? seguimiento_AuditadoTipificacion { get; set; }
        public Guid? seguimiento_BackofficeTipificacion { get; set; }
        public string back_campo1 { get; set; } // Auditado Observacion
        
        public string back_campo3 { get; set; } // Venta soportada observacion

        public string back_campo4 { get; set; }

        /*---------------------------------------------------------------------*
        * GESTION PARA LA INSTALACION.
        *-----------------------------------------------------------------------*/
        public Guid? Instalacion_EstadoInicial { get; set; }
        public Guid? Instalacion_EstadoFinal { get; set; }
        public Guid? Instalacion_RazonAgendamiento { get; set; }
        public DateTime? seguimiento_fechaInstalacion { get; set; }
        public DateTime? fechaReprogramacion { get; set; }
        public string back_campo2 { get; set; } // Digitado Observacion 


        /*---------------------------------------------------------------------*
        * GESTION PARA LA DIGITACION.
        *-----------------------------------------------------------------------*/

        public string Digitacion_RegistroNumero { get; set; }
        public string Digitacion_Contrato { get; set; }
        public string Digitacion_Cuenta { get; set; }
        public string Franja { get; set; }
        public string seguimiento_OT { get; set; }
        public DateTime? seguimiento_prox_fecha_inst { get; set; }
        public string seguimiento_obs { get; set; }
        public Guid? DigitadaPor { get; set; }
        public DateTime? DigitadaEn { get; set; }
       

        /*---------------------------------------------------------------------*
        * DATOS PARA LA NO VENTA.
        *-----------------------------------------------------------------------*/
        public string  captura_OperadorActual { get; set; }
        public decimal? captura_MontoActual { get; set; }
        public DateTime? captura_fechaPermanencia { get; set; }
        public string captura_planActual { get; set; }

        public DateTime? captura_fechaRellamada { get; set; }
        public string captura_horaRellamada { get; set; }

        /*---------------------------------------------------------------------*
        * DATOS PARA LA VENTA.
        *-----------------------------------------------------------------------*/
        // captura del cliente.
        public string captura_Nombre { get; set; }
        public string captura_Nombre1 { get; set; }
        public string captura_docCliente { get; set; }
        public string captura_Direccion { get; set; }
        public Guid? captura_departamentoId { get; set; }
        public Guid? captura_ciudadId { get; set; }
        public string captura_barrio { get; set; }
        public string captura_telefono1 { get; set; }
        public string captura_telefono2 { get; set; }
        public string captura_Observacion { get; set; }
        public Guid? cantadaPor { get; set; }
        public DateTime? cantadaEn { get; set; }
        public DateTime? RecuperadaEn { get; set; }
        public DateTime? CaidaRecuperacionAgenteEn { get; set; }
        public bool? VentaReferido { get; set; } = false;


    /*---------------------------------------------------------------------*
    * DATOS PARA LA VENTA.
    *-----------------------------------------------------------------------*/



        /*---------------------------------------------------------------------*
        * SABANA DEL CLIENTE. DATOS PARA LA VENTA.
        *-----------------------------------------------------------------------*/

        [DisplayName("PERMANENCIA")]
        public string VENTA_PERMANENCIA { get; set; }
        [DisplayName("COORDINADOR")]
        public string VENTA_COORDINADOR { get; set; }
        [DisplayName("ASESOR")]
        public string VENTA_ASESOR { get; set; }
        [DisplayName("TIPODE_DE_CLIENTE")]
        public string VENTA_TIPODE_DE_CLIENTE { get; set; }
        [DisplayName("TIPO_DE_SOLICITUD")]
        public string VENTA_TIPO_DE_SOLICITUD { get; set; }
        [DisplayName("MULTIPLAY")]
        public string VENTA_MULTIPLAY { get; set; }
        [DisplayName("CUENTA_VENTA")]
        public string VENTA_CUENTA_VENTA { get; set; }
        [DisplayName("NOMBRE_COMPLETO_CLIENTE")]
        public string VENTA_NOMBRE_COMPLETO_CLIENTE { get; set; }
        [DisplayName("TIPO_CEDULA_CE/CC")]
        public string VENTA_captura_tipodoc { get; set; }
        [DisplayName("NUMERO_CEDULA")]
        public string VENTA_NUMERO_CEDULA { get; set; }
        [DisplayName("FECHA_EXPEDICION")]
        public DateTime? VENTA_captura_fechaExped { get; set; }
        [DisplayName("TIPO_DE_RED")]
        public string VENTA_TIPO_DE_RED { get; set; }
        [DisplayName("CIUDAD")]
        public string VENTA_CIUDAD { get; set; }
        [DisplayName("DIRECCION")]
        public string VENTA_DIRECCION { get; set; }
        [DisplayName("CUENTA_MATRIZ")]
        public string VENTA_CUENTA_MATRIZ { get; set; }
        [DisplayName("BARRIO")]
        public string VENTA_BARRIO { get; set; }
        [DisplayName("ESTRATO")]
        public string VENTA_captura_Estrato { get; set; }
        [DisplayName("OPERADOR_ACTUAL?")]
        public string VENTA_OPERADOR_ACTUAL {get; set; }
        [DisplayName("TELEFONO_FIJO")]
        public string VENTA_TELEFONO_FIJO { get; set; }
        [DisplayName("CELULAR")]
        public string VENTA_CELULAR { get; set; }
        [DisplayName("ENTREGA_CORRESPONDENCIA")]
        public string VENTA_captura_ent_correspondencia { get; set; }
        [DisplayName("CORREO")]
        public string VENTA_Captura_Email { get; set; }
        [DisplayName("CODIGO_DE_VENDEDOR")]
        public string VENTA_CODIGO_DE_VENDEDOR { get; set; }
        [DisplayName("CEDULA_DE_VENDEDOR")]
        public string VENTA_CEDULA_DE_VENDEDOR { get; set; }
        [DisplayName("CANAL")]
        public string VENTA_CANAL { get; set; }
        [DisplayName("SERVICIOS_ACTUALES")]
        public string VENTA_captura_serviciosActuales { get; set; }
        [DisplayName("SERVICIOS_VENDIDOS")]
        public string VENTA_SERVICIOS_VENDIDOS { get; set; }
        [DisplayName("TV_BASICA/AVANZADA")]
        public string VENTA_AVANZADA { get; set; }
        [DisplayName("VELOCIDAD_@")]
        public string VENTA_VELOCIDAD_M { get; set; }
        [DisplayName("TOMAS_ADICIONALES_TV")]
        public string VENTA_TOMAS_ADICIONALES_TV { get; set; }
        [DisplayName("COBRO_DE_INSTALACION")]
        public bool? VENTA_captura_cobroInstalacion { get; set; }
        [DisplayName("CAMPAÑA")]
        public string VENTA_CAMPANA { get; set; }
        [DisplayName("POLITICA")]
        public string VENTA_POLITICA { get; set; }
        [DisplayName("ACTUALIZAR_TARIFA")]
        public bool? VENTA_captura_ActualizarTarifa { get; set; }
        [DisplayName("RENTA_MENSUAL")]
        public string VENTA_RENTA_MENSUAL { get; set; }
        [DisplayName("FECHA_AGENDAMIENTO")]
        public DateTime? VENTA_captura_fechaAgendamiento { get; set; }
        [DisplayName("FRANJA")]
        public string VENTA_captura_franja { get; set; }
        [DisplayName("RENTA_MENSUAL")]
        public string VENTA_captura_rentaMensual { get; set; }
        [DisplayName("OBSERVACION")]
        public string VENTA_OBSERVACION { get; set; }
        [DisplayName("EVIDENTE_-_ID_VISION")]
        public string VENTA__ID_VISION { get; set; }
        [DisplayName("RECHAZO_BACK")]
        public string VENTA_RECHAZO_BACK { get; set; }
        [DisplayName("NUMERO_A_ACTIVAR")]
        public string VENTA_NUMERO_A_ACTIVAR { get; set; }
        [DisplayName("PLAN_ACT")]
        public string VENTA_PLAN_ACT { get; set; }

        [DisplayName("VENTA_FECHAEXPEDICION")]
        public string VENTA_LUGAREXPEDICION { get; set; }

        [DisplayName("DECOSADICIONALES")]
        public string VENTA_DECOSADICIONALES { get; set; }
        [DisplayName("CODIGO_EVIDENTE")]
        public string CODIGO_EVIDENTE { get; set; }

        [DisplayName("Venta_CantidadDeServicios")]
        public string Venta_CantidadDeServicios { get; set; }


        public string QueQuiereComprar { get; set; }
        public string TeamLeader { get; set; }
        public string Gerente { get; set; }



        //presence
        public decimal? presence_Id { get; set; }
        public string TelefonoLlamada { get; set; }
        public int? productoIdLlamada { get; set; } 
        public string vdnLlamada { get; set; }
        public string vdnLlamadaInbound { get; set; }



        /*---------------------------------------------------------------------*
        * DATOS PARA LA GESTION INBOUND
        *-----------------------------------------------------------------------*/

        public Guid? Inbound_RazonLlamadaId { get; set; }
        [ForeignKey("Inbound_RazonLlamadaId")]
        public Configuration Inbound_RazonLlamada { get; set; }

        //Inbound OutBound.
        public string TipoServicioCamapa { get; set; }
        public string ID_ASIGNACION { get; set; }
        public DateTime? UltimoUpdate { get; set; } = DateTime.Now;

        #region RELACIONES
        [ForeignKey("captura_departamentoId")]
            public State captura_departamento { get; set; }

            [ForeignKey("captura_ciudadId")]
            public City captura_ciudad { get; set; }

            [ForeignKey("EstadoId")]
            public Configuration Estado { get; set; }

            public int? TipificacionId { get; set; }
            [ForeignKey("TipificacionId")]
            public Tipificacion Tipificacion { get; set; }

            public int? TipificacionRecuperacionId { get; set; }
            [ForeignKey("TipificacionRecuperacionId")]
            public Tipificacion TipificacionRecuperacion { get; set; }


            public int? CargaId { get; set; }
            [ForeignKey("CargaId")]
            public Carga Carga { get; set; }



        public Guid? TipificacioBackofficeId { get; set; }
            [ForeignKey("TipificacioBackofficeId")]
            public Configuration TipificacioBackoffice { get; set; }


        [ForeignKey("ClientId")]
        public Cliente Cliente { get; set; }
        /*---------------------------------------------------------------------*
        * RELACIONES USUARIOS
        *-----------------------------------------------------------------------*/
        [ForeignKey("cantadaPor")]
            public User.User cantadaPor__Agente { get; set; }

            [ForeignKey("ValidadoPor")]
            public User.User ValidadoPor_Agente { get; set; }

            [ForeignKey("DigitadaPor")]
            public User.User DigitadaPor_Agente { get; set; }

            [ForeignKey("InstaladaActulizadoPor")]
            public User.User InstaladaActulizadoPor_Agente { get; set; }

            [ForeignKey("soportadaPor")]
            public User.User soportadaPor_Agente { get; set; }

            [ForeignKey("AuditadoPor")]
            public User.User AuditadoPor_Agente { get; set; }



        /*---------------------------------------------------------------------*
        * RELACIONES DE BACKOFFICE
        *-----------------------------------------------------------------------*/

        [ForeignKey("Instalacion_EstadoInicial")]
        public Configuration Instalacion_EstadoInicial_llave { get; set; }

        [ForeignKey("Instalacion_EstadoFinal")]
        public Configuration Instalacion_EstadoFinal_llave { get; set; }

        [ForeignKey("Instalacion_RazonAgendamiento")]
        public Configuration Instalacion_RazonAgendamiento_llave { get; set; }

        [ForeignKey("seguimiento_AuditadoTipificacion")]
        public Configuration seguimiento_AuditadoTipificacion_llave { get; set; }

        [ForeignKey("seguimiento_BackofficeTipificacion")]
        public Configuration seguimiento_BackofficeTipificacion_llave { get; set; }



        #endregion

    }
}