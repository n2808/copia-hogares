﻿using Core.Models.Common;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class AgregadosVenta : EntityWithIntId
	{
       
        public int GestionId { get; set; }

        [ForeignKey("GestionId")]
        public Gestion Gestion { get; set; }

        public Guid AgregadoId { get; set; }

        public string NombreAgregado { get; set; }
        public int cantidad { get; set; }


        

    }
}