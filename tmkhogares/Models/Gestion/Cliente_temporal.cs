﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class Cliente_temporal : EntityWithIntId
	{

        #region DATOS GENERALES

        public string CONTRATO_CUSTCODE { get; set; }
        public string CUENTA_CO_ID { get; set; }
        public string TELE_NUMB { get; set; }
        public string ESTADO { get; set; }
        public string NOMBRE_CLIENTE { get; set; }
        public string TIPO_IDENT { get; set; }
        public string NUMERO_IDENTIFICACION { get; set; }
        public string TIPO_CLIENTE { get; set; }
        public string DIRECCION { get; set; }
        public string CIUDAD { get; set; }
        public string ESTRATO { get; set; }
        public string TELEFONO_1 { get; set; }
        public string TELEFONO_2 { get; set; }
        public string TELEFONO_3 { get; set; }
        public string CELULAR_1 { get; set; }
        public string CELULAR_2 { get; set; }
        public string EMAIL { get; set; }

        #endregion

        #region DATOS CLIENTE MOVIL
        public string FECHA_VINCULACION { get; set; }
        public string ANTIGUEDAD { get; set; }
        public string CALIFICACION_CLIENTE {get; set;}
        public string TMCODE_ACTUAL { get; set; }
        public string TIPO_PRODUCTO { get; set; }
        public string SEGMENTO_PLAN { get; set; }
        public string TECNOLOGIA_PLAN { get; set; }
        public string DES_TMCODE { get; set; }
        public string CFM_PLAN { get; set; }
        public string SPCODE { get; set; }
        public string DES_SPCODE { get; set; }
        public string TECNOLOGIA_PAQUETE { get; set; }
        public string CFM_PAQ { get; set; }
        public string ARPU { get; set; }
        public string DETALLE_SERVICIOS_ADICIONALES { get; set; }
        public string COMPORTAMIENTO_PAGO { get; set; }
        public string CONSUMO_DATOS_MES_1 { get; set; }
        public string CONSUMO_DATOS_MES_2 { get; set; }
        public string CONSUMO_DATOS_MES_3 { get; set; }
        public string MOU_MES { get; set; }
        public string TIPO_EQUIPO { get; set; }
        public string TICKLER { get; set; }
        public string DESACTIV { get; set; }
        public string PLAN_PAR { get; set; }
        public string AJUSTES { get; set; }
        public string minutos_incluidos {get; set;}
        public string megas_incluidas {get; set;}
        public string Campo_Movil_3 { get; set; }
        public string Campo_Movil_4 { get; set; }
        public string Campo_Movil_5 { get; set; }
        #endregion

        #region DATOS CLIENTE FIJO
        public string NODO { get; set; }
        public string COMUNIDAD { get; set; }
        public string DIVISION { get; set; }
        public string TARIFA { get; set; }
        public string RENTA_RR { get; set; }
        public string SERVICIO_TV { get; set; }
        public string SERVICIO_INTERNET { get; set; }
        public string MINTIC { get; set; }
        public string SERVICIO_VOZ { get; set; }
        public string TIPO_HD { get; set; }
        public string TIPO_HBO { get; set; }
        public string TIPO_FOX { get; set; }
        public string TIPO_ADULTO { get; set; }
        public string DECOS_ADICIONALES_HD_PVR { get; set; }
        public string TIPO_CLAROVIDEO { get; set; }
        public string NUM_DECOS_ADICIONALES_TV { get; set; }
        public string TIPO_REVISTA { get; set; }
        public string TIPO_OTROS { get; set; }
        public string SEGMENTO_CLIENTE { get; set; }
        public string DIVISION_COMERCIAL { get; set; }
        public string AREA_COMERCIAL { get; set; }
        public string ZONA_COMERCIAL { get; set; }
        public string Empaquetamiento { get; set; }
        public string PAQUETE_ACTUAL { get; set; }
        public string RENTA_BASICOS { get; set; }
        public string RENTA_ADICIONALES { get; set; }
        public string RENTA_TOTAL_IVA { get; set; }
        public string BENEFICIO { get; set; }
        public string DESCUENTO_OFERTA_COMERCIAL { get; set; }
        public string ABS_DESCUENTO { get; set; }
        public string VALOR_COMERCIAL_OFERTA { get; set; }
        public string VALOR_OFERTA_CON_DESC { get; set; }
        public string VENTA_TECNOLOGIA { get; set; }
        public string EQUIPO_ACTUAL { get; set; }
        public string OFERTA_MULTIPLAY { get; set; }
        public string BLINDAJE_MOVIL { get; set; }
        public string Campo_Fijo_4 { get; set; }
        public string Campo_Fijo_5 { get; set; }

        #endregion

        #region DATOS BD
        public string adicionales_APELLIDO { get; set; }
        public string adicionales_NOMBRE { get; set; }
        public string adicionales_ESTRATEGIA { get; set; }
        public string adicionales_TIPO_PAQUETE { get; set; }
        public string adicionales_TELEFONO_TELMEX { get; set; }
        public string adicionales_FECHA_ULTIMA_OT { get; set; }
        public string adicionales_RENTA_ESTIMADA { get; set; }
        public string adicionales_TRABAJOREVISTA { get; set; }
        public string adicionales_PROBABILIDAD_VENTA { get; set; }
        public string adicionales_CTA_MATRIZ { get; set; }
        public string adicionales_CORTE { get; set; }
        public string adicionales_CUENTAS_ALTERNAS { get; set; }
        public string adicionales_DIRECCIONES_ALTERNAS { get; set; }
        public string adicionales_Oferta_1 { get; set; }
        public string adicionales_Valor_Oferta_1 { get; set; }
        public string adicionales_Diferencia_OFerta_1 { get; set; }
        public string adicionales_Codigo_Tarifa_1 { get; set; }
        public string adicionales_Codigo_Politica_1 { get; set; }
        public string adicionales_Oferta_2 { get; set; }
        public string adicionales_Valor_Oferta_2 { get; set; }
        public string adicionales_Diferencia_OFerta_2 { get; set; }
        public string adicionales_Codigo_Tarifa_2 { get; set; }
        public string adicionales_Codigo_Politica_2 { get; set; }
        public string adicionales_Oferta_3 { get; set; }
        public string adicionales_Valor_Oferta_3 { get; set; }
        public string adicionales_Diferencia_OFerta_3 { get; set; }
        public string adicionales_Codigo_Tarifa_3 { get; set; }
        public string adicionales_Codigo_Politica_3 { get; set; }
        public string adicionales_Oferta_1_Adicional { get; set; }
        public string adicionales_Oferta_4 { get; set; }
        public string adicionales_Valor_Oferta_4 { get; set; }
        public string adicionales_Codigo_Tarifa_4 { get; set; }
        #endregion

        #region DATOS BD CRUZADA

        public string cruzada_NUM_DECOS_ADI { get; set; }
        public string cruzada_DIVISION_COMERCIAL { get; set; }

        #endregion

        #region DATOS DE LA CARGA
            public int? CargaId { get; set; }
            public int? car_ProductoId { get; set; }
            public int? id_clienteCargado { get; set; }
            public string COMMENTS { get; set; }
            public int ClI_FILA { get; set; }
            public string ID_ASIGNACION { get; set; }
        #endregion





    }
}