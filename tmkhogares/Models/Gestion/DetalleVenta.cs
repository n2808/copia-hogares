﻿using Core.Models.Common;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class DetalleVenta : EntityWithIntId
    {
       
        public int GestionId { get; set; }

        [ForeignKey("GestionId")]
        public Gestion Gestion { get; set; }

        public string NombreProducto { get; set; } // se guarda el nombre del producto
        public string CampanaPague1 { get; set; } // se guarda si aplica, politia de pague en un mes posterior.
        public string CampanaPromocion { get; set; } // aca se almacena la informacion de la ptar que aplica para este producto.
        public string Descripcion { get; set; }
        public decimal? Valor1 { get; set; } // valor fijo del plan al momento de la venta.
        public decimal? ValorPromocion { get; set; } // valor del plan con la promocion
        public string MegasPromocion { get; set; } // megas de la promocion. 
        public string TVPromocion  { get; set; } // tv de la promocion.

        /*
          1 - Producto principal
          2 - producto adicional
         */
        public int TipoProducto { get; set;  }
        
        
        /*
          * Contiene un producto si es DTH - 1 o si es HFC 2
             */
        public int productoId { get; set;  }

        /*
            * Contiene la categoria y subcategoria de los productos
        */
        public string CategoriaProducto{ get; set; }
        public string SubCategoriaProducto{ get; set; }


        /*
            * Cantidad de productos que se vendieron 
        */
        public int cantidad { get; set; }
        /*
            * Se almacena la cantidad que equivale el producto de venta individual que estaba configurada ene l el software
        */

        public int  CantidadBase { get; set; }

        /*
         * Almacena la cantidad de servicios que se realizaron el upgrade.y cuantos como nuevos
        */

        public int CantidadUpgrade { get; set; }
        public int CantidadNuevos { get; set; }
    }
}