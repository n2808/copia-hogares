﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class CampanasVdns : EntityWithIntId
	{
        public string NombreMostrarCanal { get; set; }
        public string Descripcion { get; set; }
        public string CanalIngreso { get; set; }
        public string Producto { get; set; }
        public bool Estado { get; set; } = true;


    }
}