﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Gestion
{
	public class Carga : EntityWithIntId
	{
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int NumeroRegistros { get; set; } = 0;
        public int servicio_codigo { get; set; }
        public bool estado { get; set; } = true;
        public Guid usu_cargo { get; set; }
        public int ProductoId { get; set; }


    }
}