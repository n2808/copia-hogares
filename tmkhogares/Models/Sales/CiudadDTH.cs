﻿using Core.Models.Common;
using Core.Models.configuration;
using Core.Models.location;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Core.Models.Sales
{
    
    public class CiudadDTH : EntityWithIntId
	{
        [DisplayName("IdCiudad")]
        public Guid CiudadId { get; set; }

        [DisplayName("Nombre Ciudad")]
        public string CiudadNombre { get; set; }

        public string Tv { get; set; }
        public string Internet { get; set; }
        public string Linea { get; set; }
        #region Relaciones

        [ForeignKey("CiudadId")]
        public City Ciudad { get; set; }
        #endregion
    }
}