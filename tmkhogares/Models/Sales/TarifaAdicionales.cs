﻿using Core.Models.Common;
using Core.Models.configuration;
using Core.Models.location;
using Core.Models.Oferta;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Core.Models.Sales
{
    [Table("TarifaAdicionales")]
    public class TarifaAdicionales : EntityWithIntId
	{
        public string Nombre { get; set; }
        public string ServicioAlqueaplica { get; set; }
        public string Codigo { get; set; }
        public decimal valor { get; set; }
        public decimal? valor1 { get; set; }
        public decimal? valor2 { get; set; }
        public decimal? valor3 { get; set; }
        public decimal? valor4 { get; set; }
        public decimal? valor5 { get; set; }
        public decimal? valor6 { get; set; }
        public decimal? valor7 { get; set; }
        public int? productoId { get; set; } = 2;

        public int cantidadMaxima { get; set; }
        public int? cantidadDeVenta { get; set; } = 1;
        public int orden { get; set; }



        public int? ProductosOfrecidosId { get; set; }

        [ForeignKey("ProductosOfrecidosId")]
        public ProductosOfrecidos ProductoOfrecidoOferta { get; set; }

    }
}