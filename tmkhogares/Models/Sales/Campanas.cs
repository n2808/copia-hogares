﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Sales
{
    public class Campana : EntityWithIntId
    {
        [DisplayName("Nombre Campaña")]
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string DescripcionLarga { get; set; }

        public bool Estado { get; set; }

        [DisplayName("Tipo de ciudad")]

        public Guid TipoCiudadesId { get; set; }

        [DisplayName("Tipo de productos")]
        public Guid TipoProductoId { get; set; }

        public string Estratos { get; set; }

        [DisplayName("¿Aplica para Empaquetamiento?")]
        public bool ClienteFijo { get; set; }
        [DisplayName("¿Debe tener Linea Movil con claro?")]
        public bool ClienteMovil { get; set; }
        [DisplayName("¿Aplica para cliente Nuevo?")]
        public bool ClienteNuevo { get; set; }
        [DisplayName("Tipo de oferta a mostrar")]

        public Guid TipoOfertaId { get; set; }


        [DisplayName("Porcentaje de valor")]
        public decimal? percentValue { get; set; }

        [DisplayName("Porcentaje de Velocidad")]
        public decimal? percentBaVelocity { get; set; }

        [DisplayName("Porcentaje de Velocidad cliente Movil")]
        public decimal? percentBaVelocityClienteMovil { get; set; }

        [DisplayName("Porcentaje de valor cliente Movil")]
        public decimal? percentValueClienteMovil { get; set; }

        public int? Campana_producto { get; set; }
        public Guid? Campana_Tipo { get; set; }


        // trabajo con ptar inc/exc
        public Guid? TipoPtar { get; set; }
        public string Ptars { get; set;  }



        #region Relaciones

        [ForeignKey("TipoCiudadesId")]
        public Configuration TipoCiudades { get; set; }


        [ForeignKey("TipoProductoId")]
        public Configuration TipoProducto { get; set; }

        [ForeignKey("TipoOfertaId")]
        public Configuration TipoOferta { get; set; }


        #endregion

    }
}