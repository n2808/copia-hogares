﻿using Core.Models.Common;
using Core.Models.configuration;
using Core.Models.Gestion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Sales
{
    public class TarifaPlena : EntityWithIntId
    {
        [DisplayName("Nombre Plan")]
        public string Nombre { get; set; }

        public Guid? TvId { get; set; }
        public Guid? LineId { get; set; }
        public Guid? BaId { get; set; }

        public string TvName { get; set; }
        public string LineName { get; set; }
        public string BaName { get; set; }


        public Guid? TipoPlanId { get; set; }
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal?  BaVelocity { get; set; }

        public string CEstrato1 {get; set;}
        public string CEstrato2 {get; set;}
        public string CEstrato3 {get; set;}
        public string CEstrato4 {get; set;}
        public string CEstrato5 { get; set;}
        public string CEstrato6 { get; set; }

        public string CSEstrato1 { get; set; }
        public string CSEstrato2 { get; set; }
        public string CSEstrato3 { get; set; }
        public string CSEstrato4 { get; set; }
        public string CSEstrato5 { get; set; }
        public string CSEstrato6 { get; set; }

        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal? VEstrato1 { get; set; }
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal? VEstrato2 { get; set; }
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal? VEstrato3 { get; set; }
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal? VEstrato4 { get; set; }
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal? VEstrato5 { get; set; }
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal? VEstrato6 { get; set; }
        public bool Estado { get; set; } = true;


        /*
         - 1 = Tarifa vigente de trabajo.
         - 2 = Tarifa solo para aplicar en planes especiales.
          */
        public int? TarifaEspecial { get; set; } = 1;
        public int? ProductoId { get; set; } = 2;
        #region Relaciones

        [ForeignKey("TvId")]
        public Configuration Tv { get; set; }
        [ForeignKey("LineId")]
        public Configuration Line { get; set; }
        [ForeignKey("BaId")]
        public Configuration Ba { get; set; }

        [ForeignKey("TipoPlanId")]
        public Servicios.Servicio TipoPlan { get; set; }

        [ForeignKey("ProductoId")]
        public Productos Producto { get; set; }

        #endregion


    }
}