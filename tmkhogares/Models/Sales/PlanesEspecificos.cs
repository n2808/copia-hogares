﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography.Xml;
using System.Web;

namespace Core.Models.Sales
{

    public class PlanesEspecificos : EntityWithIntId
    {
        public int TarifaPlenaId { get; set; }
        public int CampanaId { get; set; }

        public string CEstrato1 { get; set; }
        public string CEstrato2 { get; set; }
        public string CEstrato3 { get; set; }
        public string CEstrato4 { get; set; }
        public string CEstrato5 { get; set; }
        public string CEstrato6 { get; set; }

        public string CSEstrato1 { get; set; }
        public string CSEstrato2 { get; set; }
        public string CSEstrato3 { get; set; }
        public string CSEstrato4 { get; set; }
        public string CSEstrato5 { get; set; }
        public string CSEstrato6 { get; set; }

        public decimal? VEstrato1 { get; set; }
        public decimal? VEstrato2 { get; set; }
        public decimal? VEstrato3 { get; set; }
        public decimal? VEstrato4 { get; set; }
        public decimal? VEstrato5 { get; set; }
        public decimal? VEstrato6 { get; set; }
    
        public int? velocidad { get; set; }
        public string Nombre_plan { get; set; }
        public string Codigo_plan { get; set; }


        #region Relaciones

        [ForeignKey("CampanaId")]
        public Campana Campana { get; set; }
        [ForeignKey("TarifaPlenaId")]
        public TarifaPlena TarifaPlena { get; set; }

        #endregion

    }
}