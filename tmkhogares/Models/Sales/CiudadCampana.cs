﻿using Core.Models.Common;
using Core.Models.configuration;
using Core.Models.location;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Core.Models.Sales
{
    
    public class CiudadCampana : EntityWithIntId
	{
        [DisplayName("IdCiudad")]
        public Guid CiudadId { get; set; }

        [DisplayName("Nombre Ciudad")]
        public string CiudadNombre { get; set; }

        public int CampanaId { get; set; }

        #region Relaciones

        [ForeignKey("CiudadId")]
        public City Ciudad { get; set; }
        [ForeignKey("CampanaId")]
        public Campana Campana { get; set; }


        #endregion
    }
}