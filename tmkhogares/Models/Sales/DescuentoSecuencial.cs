﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Sales
{
	public class DescuentoSecuencial : EntityWithIntId
	{
        public int CampanaId { get; set; }
        public int mes { get; set; }
        public decimal? porcentajeDescuento { get; set; }
        public decimal? valorDescuento { get; set; }

        #region Relaciones

        [ForeignKey("CampanaId")]
        public Campana Campana { get; set; }

        #endregion

    }
}