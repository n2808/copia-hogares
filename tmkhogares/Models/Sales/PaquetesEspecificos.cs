﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Core.Models.Sales
{
    
    public class PaquetesEspecificos : EntityWithIntId
	{
        public int CampanaId { get; set; }
        public string tv { get; set; }
        public string ba { get; set; }
        public string Line { get; set; }
        public string Cod1 { get; set; }
        public string Cod2 { get; set; }

        #region Relaciones

        [ForeignKey("CampanaId")]
        public Campana Campana { get; set; }


        #endregion
    }
}