﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Oferta
{
    [Table("ProductosOfrecidos")]
	public class ProductosOfrecidos : EntityWithIntId
	{

        public string Nombre { get; set; }

        public Guid? TipoVentaId { get; set; }

        [ForeignKey("TipoVentaId")]
        public Configuration TipoVenta { get; set; }

        public bool EsProductoPrincipal { get; set; }

    }
}