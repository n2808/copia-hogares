﻿using Core.Models.Common;
using Core.Models.Gestion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;



namespace Core.Models.User
{
	public class User : Entity
	{
        [DisplayName("Documento")]
		public string Document { get; set; }
        [DisplayName("Nombres")]
        public string Names { get; set; }
        [DisplayName("Apellidos")]
        public string LastName { get; set; }
        [DisplayName("Telefono 1")]
        public string Phone1 { get; set; }
        [DisplayName("Telefono 2")]
        public string Phone2 { get; set; }
        [DisplayName("Telefono 3")]
        public string Phone3 { get; set; }
        [DisplayName("Correo")]
        public string Email { get; set; }
        [DisplayName("Estado")]
        public bool Status { get; set; }
        [DisplayName("password")]
        public string PassWord { get; set; }

        [Column(TypeName = "text")]
        public string BlogNotas { get; set; }

        public string login { get; set; }
        
        public List<UserRol> UserRol { get; set; }

        public Guid? CoordinadorId { get; set; }

        [ForeignKey("CoordinadorId")]
        public User Coordinador { get; set; }


        public Guid? GerenteId { get; set; }

        


        public int? campanaId { get; set; }

        [ForeignKey("campanaId")]
        public  Productos Campana { get; set; }



    }
}