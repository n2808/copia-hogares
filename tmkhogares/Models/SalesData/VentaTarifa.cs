﻿using Core.Models.Common;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.SalesData
{

    // contiene la lista de servicios principales vendidos 
	public class VentaTarifa : EntityWithIntId
	{
       
        public int GestionId { get; set; }
        public string NombrePlan { get; set; }
        public string CodigoPlan { get; set; }
        public decimal ValorPlan { get; set; }
        public string DescripcionPlan { get; set; }
        public string Tv { get; set;}
        public string Ba { get; set;}
        public string Linea { get; set;}
        public string CodigoHogar { get; set; }
        public string CodigoSoho { get; set; }


        [ForeignKey("GestionId")]
        public Gestion.Gestion Gestion { get; set; }



    }
}