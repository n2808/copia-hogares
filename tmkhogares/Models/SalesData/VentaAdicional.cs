﻿using Core.Models.Common;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.SalesData
{
    // contiene la lista de servicios adicionales vendidos
	public class VentaAdicional : EntityWithIntId
	{
       
        public int GestionId { get; set; }

        public decimal ValorNormal { get; set;  }
        public decimal ValorDescuento { get; set;  }
        public int Cantidad { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string CodigoHogar { get; set; }
        public string CodigoSoho { get; set;  }



        [ForeignKey("GestionId")]
        public Gestion.Gestion Gestion { get; set; }


        

    }
}