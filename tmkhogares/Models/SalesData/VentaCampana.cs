﻿using Core.Models.Common;
using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.SalesData
{
    //lista de campañas que aplican para la venta.
    public class VentaCampana : EntityWithIntId
    {

        public int GestionId { get; set; }

        public string NombreCampana { get; set; }
        public string Codigo { get; set; }
        public string Ba_Velocidad {get;set;}
        public string precio { get; set; }
        public string plan_seleccionado { get; set; }
        public string Descripcion { get; set; }


        [ForeignKey("GestionId")]
        public Gestion.Gestion Gestion { get; set; }



        

    }
}