﻿using Core.Models.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Servicios
{
    public class TipificacionServicio
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int TipificacionId { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "El Campo {0} no puede tener mas de {1} caracteres", MinimumLength = 4)]
        public string Vdn { get; set; }

        [ForeignKey("TipificacionId")]
        public Tipificacion Tipificacions { get; set; }

    }
}