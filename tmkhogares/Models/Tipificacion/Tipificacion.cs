﻿using Core.Models.Common;
using Core.Models.Gestion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Servicios
{
	public class Tipificacion : EntityWithIntId
	{
        public string Nombre { get; set; }
        public string Nombre2 { get; set; }
        public string Nombre3 { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;
        public bool GestionEfectiva { get; set; }
        public int order { get; set; }

        public int padreId{ get; set; }
        public string codigoClaro { get; set; }
        public string finalClaro { get; set; }
        public string procesar { get; set; }
        public bool final { get; set; } = false;
        public int? codigo { get; set; }
        public int productoId { get; set; }

        [ForeignKey("productoId")]
        public Productos Producto { get; set; }



    }
}