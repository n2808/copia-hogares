﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.location
{
	public class State : Entity
	{
		public string Name { get; set; }
        public string Code { get; set; }
		public bool Status { get; set; }
		public Guid CountryId { get; set; }


		[ForeignKey("CountryId")]
		public Country Country { get; set; }

	}
}