﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.location
{
	public class City : Entity
	{
		public string Name { get; set; }
		public string Code { get; set; }
        public bool Status { get; set; }
		public Guid StateId { get; set; }


		[ForeignKey("StateId")]
		public State State  { get; set; }

	}
}