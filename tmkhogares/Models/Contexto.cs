﻿namespace tmkhogares.Models
{
    using Core.Models.configuration;
    using Core.Models.Gestion;
    using Core.Models.location;
    using Core.Models.Oferta;
    using Core.Models.Sales;
    using Core.Models.SalesData;
    using Core.Models.Servicios;
    using Core.Models.User;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class Contexto : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'Contexto' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'tmkhogares.Models.Contexto' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'Contexto'  en el archivo de configuración de la aplicación.
        public Contexto()
            : base("name=Contexto")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.


        #region CONFIGURATIONS
        public DbSet<Category> Categories { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        #endregion

        #region SERVICIOS 
        public DbSet<Plan> Planes { get; set; }
        public DbSet<PlanesEstratos> PlanesEstratos { get; set; }
        public DbSet<Servicio> Servicio { get; set; }
        public DbSet<ValorAgregado> ValorAgregado { get; set; }
        #endregion


        #region TIPIFICACION
        public DbSet<Tipificacion> Tipificacions { get; set; }
        public DbSet<TipificacionServicio> TipificacionServicio { get; set; }
        #endregion

        #region OFERTA
        public DbSet<ProductosOfrecidos> ProductosOfrecidos { get; set; }
        //public DbSet<ServiciosPrincipalesProductos> ServiciosPrincipalesProductos { get; set; }
        #endregion


        #region GESTION
        public DbSet<AgregadosVenta> AgregadosVenta { get; set; }
        public DbSet<DetalleVenta> DetalleVenta { get; set; }
        public DbSet<Gestion> Gestion { get; set; }
        public DbSet<Cliente> cliente { get; set; }
        public DbSet<Cliente_temporal> cliente_temporal { get; set; }
        public DbSet<Carga> Cargar { get; set; }
        public DbSet<GestionActivacion> GestionActivacion { get; set; }
        public DbSet<Productos> Productos { get; set; }
        public DbSet<CampanasVdns> CampanasVdns { get; set; }
        public DbSet<ProductosDetalleVenta> ProductosDetalleVenta { get; set; }


        #endregion


        #region LOCALIZACION
        public DbSet<Country> Pais { get; set; }
        public DbSet<State> Departamento { get; set; }
        public DbSet<City> Ciudad { get; set; }

        #endregion

        #region USUARIOS
        public DbSet<User> Usuario { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<UserRol> UsuariosRoles { get; set; }
        #endregion


        #region AYUDAVENTAS
        public DbSet<Campana> Campanas { get; set; }
        public DbSet<CiudadCampana> CiudadCampanas { get; set; }
        public DbSet<CiudadDTH> CiudadDTH { get; set; }
        public DbSet<DescuentoSecuencial> DescuentoSecuencial { get; set; }
        public DbSet<PaquetesEspecificos> PaquetesEspecificos { get; set; }
        public DbSet<PlanesEspecificos> PlanesEspecificos { get; set; }
        public DbSet<TarifaPlena> TarifaPlena { get; set; }
        public DbSet<TarifaAdicionales> TarifaAdicionales { get; set; }

        public DbSet<VentaTarifa> VentaTarifa { get; set; }
        public DbSet<VentaCampana> VentaCampana { get; set; }
        public DbSet<VentaAdicional> VentaAdicional { get; set; }

        #endregion



    }

}