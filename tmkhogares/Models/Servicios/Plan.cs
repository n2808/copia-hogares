﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Servicios
{
	public class Plan : Entity
	{
        [DisplayName("Plan")]
        public string Nombre { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }
        
        public List<PlanesEstratos> PlanesEstratos { get; set; }

        public bool EstadoId { get; set; } = true;

        [DisplayName("Tipo De tv")]
        public string TipoTv { get; set; }

        [DisplayName("Tipo De ba")]
        public string tipoBa { get; set; }

        public Guid ServicioId { get; set; }

        [DisplayName("Tipo de plan ")]
        [ForeignKey("ServicioId")]
        public Servicio Servicio { get; set; }

    }
}