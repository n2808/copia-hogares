﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Core.Models.Servicios
{
	public class PlanesEstratos : Entity
	{
        public Guid PlanId { get; set; }

        [DisplayName("Codigo")]
        public string Costo { get; set; }

        [DisplayName("precio")]
        public string precio { get; set; }

        public Guid EstratoId { get; set; }
        [ForeignKey("PlanId")]
        public  Plan Plan { get; set; }
        [ForeignKey("EstratoId")]
        public Configuration Estrato { get; set; }

    }
}