﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Servicios
{
	public class ValorAgregado : Entity
	{
        public string Nombre { get; set; }
        public bool EstadoId { get; set; } = true;
        public int cantidad { get; set; } = 1;
        public string costo { get; set; }

    }
}