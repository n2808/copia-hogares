﻿using Core.Models.Common;
using Core.Models.Oferta;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Servicios
{
    public class Servicio : Entity
    {
        public string Nombre { get; set; }

        public bool Estado { get; set; } = true;

        public int? NumeroServicios { get; set; } = 1;
        public bool? ServicioPrincipal  { get; set; } = false;


        public int? ProductoOfrecidoId { get; set; }

        [ForeignKey("ProductoOfrecidoId")]
        public ProductosOfrecidos productos {get; set;}

    }
}