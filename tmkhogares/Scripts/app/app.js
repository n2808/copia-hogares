﻿


/*-----------------------------------------------------------------------------------*/
/*---------| CLASE CARRO DE COMPRAS
/*-----------------------------------------------------------------------------------*/

class Carrito extends General {
    constructor() {
        super()
        this.Items = []
        this.Total = 0;
        this.UpdateCarrito();
        this.ShoppingCart = $("#tbl_planes");

        //datos de la compra
        this.tipotv = "";
        this.tipoBa = "";
        this.canServicios = 0;
        this.serviciosVendidos = ""
        this.optionTipificaion;
        this.efectiva = 0;
        this.Tipificacion = 0;
        this.CamposValidarVenta = ".campoVenta";
        this.Estratos = ["",
            "65F56BE4-1A43-438D-8F1C-1FEEBD6733EB",
            "3901D691-2F04-4260-A613-4BEA542BF053",
            "303396DD-26A5-4ED5-8DE6-C71F2CB27B8A",
            "166820B5-E6F2-4998-A028-2F7A2803C2F1",
            "7FD529E8-781B-4000-A897-DB5D267F81F9",
            "1AC4224A-3656-4633-9209-A49F6C0B4955"];
        
    }

    // GET CITIES FROM STATE
    ActualizarCiudades(value, callback) {
        this.data = { State: value };
        var url = this.url;
        $.post(url.Gestion + 'getCities', this.data)
            .done(Result => {
                $("#captura_ciudadId").html("");
                this.PopulateDropdown(Result, "#captura_ciudadId");
                if (typeof callback === "function") callback();
            })
            .fail((data, status) => {
                _console("Error interno favor contactar al administrador");
            })

    }


    // GET CITIES FROM STATE
    ActualizarInboundMotivoLLamada(value, callback) {
        this.data = { Motivo: value };
        var url = this.url;
        $.post(url.Gestion + 'getMotivoLlamada', this.data)
            .done(Result => {
                $("#venta_RazonLLamada_Inbound").html("");
                this.PopulateDropdown(Result, "#venta_RazonLLamada_Inbound");
                if (typeof callback === "function") callback();
            })
            .fail((data, status) => {
                _console("Error interno favor contactar al administrador");
            })

    }

    UpdateCarrito() {


        
        var joinservices = this.Items.map(function (Item) { return Item.descripcion })
        this.serviciosVendidos = joinservices.join(" + ");
  
        var servicios = this.Items.map((Item) => { return Item.canservicios });
        if (servicios.length > 0) {
            _console(servicios);
            this.canServicios = servicios.reduce((accumulator, num) => {
                return accumulator + num;
            });

        }
        
        let _campana = [];
        $.each($("#carro_tbl tbody tr"), function (index, val) {
            const item = $(this);
            const joincampanas = item.find("select").val();
            if (joincampanas != null) {
                _campana.push(joincampanas.join(" + "));
                _error(joincampanas)
                _error(_campana)
            }
        });

        
        //ACTUALIZAR INFORMACION DE TV Y BA.
        $("#VENTA_AVANZADA").val(this.tipotv);
        $("#VENTA_VELOCIDAD_M").val(this.tipoBa);
        $("#VENTA_SERVICIOS_VENDIDOS").val(this.serviciosVendidos);
        $("#Venta_CantidadDeServicios").val(this.canServicios);
        $("#VENTA_CAMPANA").val(_campana.join(" + "));
    }

    AddItemtoShoppingCart(Item) {

        //var Item = {
        //    Id: fila.data('id'),
        //    nombre: fila.data('nombre'),
        //    estrato: fila.data('estrato'),
        //    tv: fila.data('tv'),
        //    ba: fila.data('ba'),
        //    canservicios: fila.data('canservicios'),
        //    tipo: fila.data('tipo'),
        //codigo: fila.data('codigo'),
        //    cantidad: cantidad
        //}

        var clone = $('#campana_plantilla').clone()

        const ElementItem = "<tr data-id='" + Item.Id + "' data-tipo='" + Item.tipo + "' data-estrato='" + Item.estratonombre + "' data-codigo='" + Item.codigo + "' data-cantidad='" + Item.cantidad + "'  data-nombreplan='" + Item.nombre + "'  > \
            <td>" + Item.nombre + "</td><td>" + Item.estratonombre + "</td><td>" + Item.codigo + "</td><td class='campana'></td><td>" + Item.cantidad + "</td><td><button class='btn btn-danger removeItem'><i class='fa fa-trash'></i></button></td></tr>";


        this.ShoppingCart.append(ElementItem);
        $("#tbl_planes tr:last .campana").append(clone);
        $("#tbl_planes tr:last .campana select").removeAttr('id').select2();
    
    }
    //añade un producto a la compra
    AddProduct(Item) {
        
        if (Item.tipo == "principal") {
            this.tipotv = Item.tv;
            this.tipoBa = Item.ba;
        }




        this.Items.push(Item);
        this.AddItemtoShoppingCart(Item);
        this.UpdateCarrito();
    }


    // VERIFICA QUE NO SE HAYA ESCOGIDO UN PRODUCTO PRINCIPAL
    ChecPrincipal(item) {
        if (item.tipo != "principal") {
            return false;
        }

        var Item = this.Items.filter(obj => {
            return obj.tipo === "principal"
        })


        return Item.length == 0 ? false : true;
    }


    // VERIFICA QUE SEA UN PRODUCTO DIFERENTE
    checkExist(Id) {
        var Item = this.Items.filter(obj => {
            return obj.Id === Id
        })
        return Item.length == 0 ? false : true;
    }


    // VERIFICA QUE EL ESTRATO SEA EL MISMO
    checkEstrato(item) {

        if (item.tipo  != "principal") {
            return false;
        }

        var Item = this.Items.filter(obj => {
            _console(obj.estrato);
            return obj.estrato !== item.estrato && obj.estrato != "0"
        })
        return Item.length == 0 ? false : true;
    }


    removeProduct(id) {

        var Item = this.Items.filter(obj => {
            return obj.Id === id
        })
        if (Item.length == 0) {    return;        }
        if (Item.length == 1) { Item = Item[0] }

        if (Item.tipo == "principal") {
            this.tipotv = "";
            this.tipoBa = "";
            this.serviciosVendidos = "";
        }

        this.canServicios = this.canServicios - parseInt(Item.canservicios);
        this.Items = this.Items.filter(obj => {
            return obj.Id !== id
        })
        this.UpdateCarrito();

    }



    ValidarVenta(Editable = false) {
        console.log("Es Editable", Editable)
        $(".alert-danger").remove();
        b_validacion = true;
        primerElementoG = 1;

        
        
        // datos de la tipificacion.
        var tipval = $("select.dro_cascade_tipificacion:last");
        var efectiva = tipval.find("option:selected").data("efectiva");
        this.optionTipificaion = tipval;
        this.efectiva = efectiva;


        if (Editable == false && document.getElementById('acw').value == -1) {
            return true;
        }

        
        // codigo para validar la venta.



        if (this.efectiva == 0) {
            return true;
        }
        else {
            // venta.
            if ($("#tbl_ventaProductos tr").length == 0 && this.efectiva == 1) {
                $("#GuardarGestion").parent().append(string_mensaje("Se debe seleccionar un producto"))
                $("#GuardarGestionEditable").parent().append(string_mensaje("Se debe seleccionar un producto"))
                return false;
            }
            //borrar esta linea
            
            $(this.CamposValidarVenta).each(void_validaCampo);
            _console("saliendo de las validaciones");
            if (!b_validacion) { return false }

            return true;


        }
        

    }

    /*--------------------------------*/
    /*---------| GUARDA UNA VENTA .
    /*------------------------------*/

    GuardarVenta() {

        //let DetallePlan = Carro.Finish();
        //let campanas = DetallePlan.campana.join(",");
        //validaciones.
        if ($("#acw").val() != -1) {
            if (this.optionTipificaion.val() == 0) {
                $("#GuardarGestion").parent().append(string_mensaje("Se debe seleccionar la tipificacion"))
                $("#GuardarGestionEditable").parent().append(string_mensaje("Se debe seleccionar la tipificacion"))
                return;
            }
 
        }
        else {
            $("#tipificacion").val(100);
        }


        var fechaProgramada =  $("#fechaLlamada").val() + " " + $("#horaLlamada").val() + ":" + $("#minutoLlamar").val();
        this.Tipificacion = $("#tipificacion").val();

        this.data = {
            CodigoTipificacion: this.Tipificacion,
            deletedAt: fechaProgramada,
            VENTA_PERMANENCIA: $("#VENTA_PERMANENCIA").val(),
            VENTA_TIPODE_DE_CLIENTE: $("#VENTA_TIPODE_DE_CLIENTE").val(),
            VENTA_TIPO_DE_SOLICITUD: $("#VENTA_TIPO_DE_SOLICITUD").val(),
            VENTA_MULTIPLAY: $("#VENTA_MULTIPLAY").val(),
            VENTA_CUENTA_VENTA: $("#VENTA_CUENTA_VENTA").val(),
            VENTA_NOMBRE_COMPLETO_CLIENTE: $("#VENTA_NOMBRE_COMPLETO_CLIENTE").val(),
            VENTA_captura_tipodoc: $("#VENTA_captura_tipodoc").val(),
            VENTA_NUMERO_CEDULA: $("#VENTA_NUMERO_CEDULA").val(),
            VENTA_captura_fechaExped: $("#VENTA_captura_fechaExped").val(),
            VENTA_TIPO_DE_RED: $("#VENTA_TIPO_DE_RED").val(),
            VENTA_CUENTA_MATRIZ: $("#VENTA_CUENTA_MATRIZ").val(),
            VENTA_captura_Estrato: $("#slct_ayuda_estrato").val(),
            VENTA_OPERADOR_ACTUAL: $("#VENTA_OPERADOR_ACTUAL").val(),
            VENTA_TELEFONO_FIJO: $("#VENTA_TELEFONO_FIJO").val(),
            VENTA_CELULAR: $("#VENTA_CELULAR").val(),
            VENTA_captura_ent_correspondencia: $("#VENTA_captura_ent_correspondencia").val(),
            VENTA_Captura_Email: $("#VENTA_Captura_Email").val(),
            VENTA_CANAL: $("#VENTA_CANAL").val(),
            VENTA_captura_serviciosActuales: $("#VENTA_captura_serviciosActuales").val(),
            VENTA_SERVICIOS_VENDIDOS: $("#VENTA_SERVICIOS_VENDIDOS").val(),
            VENTA_AVANZADA: $("#VENTA_AVANZADA").val(),
            VENTA_VELOCIDAD_M: $("#VENTA_VELOCIDAD_M").val(),
            VENTA_TOMAS_ADICIONALES_TV: $("#VENTA_TOMAS_ADICIONALES_TV").val(),
            VENTA_captura_cobroInstalacion: $("#VENTA_captura_cobroInstalacion").val(),
            //VENTA_CAMPANA: campanas,
            VENTA_POLITICA: $("#VENTA_POLITICA").val(),
            VENTA_captura_ActualizarTarifa: $("#VENTA_captura_ActualizarTarifa").val(),
            VENTA_RENTA_MENSUAL: $("#VENTA_RENTA_MENSUAL").val(),
            VENTA_captura_fechaAgendamiento: $("#VENTA_captura_fechaAgendamiento").val(),
            VENTA_captura_franja: $("#VENTA_captura_franja").val(),
            VENTA_captura_rentaMensual: $("#VENTA_captura_rentaMensual").val(),
            VENTA__ID_VISION: $("#VENTA__ID_VISION").val(),
            VENTA_RECHAZO_BACK: $("#VENTA_RECHAZO_BACK").val(),
            VENTA_NUMERO_A_ACTIVAR: $("#VENTA_NUMERO_A_ACTIVAR").val(),
            VENTA_PLAN_ACT: $("#VENTA_PLAN_ACT").val(),
            Venta_CantidadDeServicios: $("#Venta_CantidadDeServicios").val(),
            CODIGO_EVIDENTE: $("#CODIGO_EVIDENTE").val(),
            VENTA_DECOSADICIONALES: $("#VENTA_DECOSADICIONALES").val(),
            VENTA_LUGAREXPEDICION: $("#VENTA_LUGAREXPEDICION").val(),
           
            procesar: this.efectiva,
            acw: $("#acw").val(),
            TipificacionId: this.optionTipificaion.val(),
            Id: $("#gtuuId").val(),
            
            captura_ciudadId: $("#ayuda_ciudad").val(),
            captura_barrio: $("#captura_barrio").val(),
            captura_telefono1: $("#captura_telefono1").val(),
            captura_telefono2: $("#captura_telefono2").val(),
            captura_Direccion: $("#txt_direccion").val(),
            uVerificador: $("#UsuarioVerificador").val(),
            pVerificador: $("#PassVerificador").val(),
            captura_Observacion: $("#captura_Observacion").val(),
            AsuuId: $("#AsuuId").val(), // usu id
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val(),
            Inbound_RazonLlamadaId: $("#venta_RazonLLamada_Inbound").val(),
            vdnLlamadaInbound: $("#vdnLlamadaInbound").val(),
            back_campo4: $("#TipoCliente:checked").val(),
            QueQuiereComprar: $("#QueQuiereComprar").val(),
            VENTA_POLITICA: $("#MedioEnteroPromocion").val()
            // ENVIANDO DATOS DE INBOUND
        };

        var ventaData = this.efectiva == 0 ? "false" : "true";
        ventaData = "true";
        var url = this.url;
        _error(this.data)
        $("#GuardarGestion").attr("disabled", true);
        $.post(url.Gestion + 'CreateSale', this.data)
            .done((Result, status, statuscode) => {
          
             //TIPIFICACIONES A INBLUIR EN EL REMARCADO DE INBOUND
               
            var a_tipExcluir = [
                22055, //	VOLVER A LLAMAR
                30001, //	TELEFONO ERRADO
                22053, //	CONTESTADOR
                30003, //	FAX
                22000, //	LLAMADA CORTADA
                100, //	ACW
                30010, //	NUMERO NO EXISTE
                30000, //	NO HAY CONTACTO CON EL TITULAR
                30011, //	VOLVER A LLAMAR
                22054, //	NO CONTESTAN
                30005, //	OTROS TONOS
                30006, //	TELEFONO DAÑADO
                30007, //	TELEFONO DESCONECTADO
                30008, //	TELEFONO OCUPADO
                9999, //	LLAMADA_ENTRA_CORTADA / INTERACCION CAIDA
                32014, //	REGISTRO NO GESTIONABLE
                32015	//LLAMADA OCIOSA / MENOR DE EDAD
            ];
            var a_tipReMarcar = [
                22055, //	VOLVER A LLAMAR
                30001, //	TELEFONO ERRADO
                22053, //	CONTESTADOR
                22054, //	CONTESTADOR
                30003, //	FAX
                22000, //	LLAMADA CORTADA
                100, //	ACW
                30010, //	NUMERO NO EXISTE
                30000, //	NO HAY CONTACTO CON EL TITULAR
                30011, //	VOLVER A LLAMAR
                22054, //	NO CONTESTAN
                30005, //	OTROS TONOS
                30006, //	TELEFONO DAÑADO
                30007, //	TELEFONO DESCONECTADO
                30008, //	TELEFONO OCUPADO
                9999, //	LLAMADA_ENTRA_CORTADA / INTERACCION CAIDA
                32014, //	REGISTRO NO GESTIONABLE
                32015	//LLAMADA OCIOSA / MENOR DE EDAD
                ];

                var Campaign = 17;
                if (VDNLlamada == "7621" || VDNLlamada == "7623" || VDNLlamada == "7625" || VDNLlamada == "7620")
                    Campaign = 17;
                else if (VDNLlamada == "7624" || VDNLlamada == "7619")
                    Campaign = 18;


            var CodigoPresence = parseInt($("select.dro_cascade_tipificacion:last option:selected").data('codigo'));
                console.log("codigo de presence", CodigoPresence)

            if (statuscode.status == 201) {

                $("#GuardarGestion").attr("disabled", false);
                $.each(Result, function (val, elm) {
                    $("#GuardarGestion").parent().append(Carro.JsonDecodeErrors(elm));
                    $("#GuardarGestionEditable").parent().append(Carro.JsonDecodeErrors(elm));
                    Carro.toBottom()
                });
            }
            else {



                /*
                    * CODIGO PARA GUARDAR EN EL WEBSERVICE DE INBOUND
                 */



                /** VDN LLAMADA DIGITAL  **/
                if (
                           VDNLlamada == "7621"
                        || VDNLlamada == "7623"
                        || VDNLlamada == "7624"
                        || VDNLlamada == "7619"
                        || VDNLlamada == "7625"
                        || VDNLlamada == "7620"
                ) {
                    console.log("Guardando Vdn llamda ", VDNLlamada)

                    ///TIPIFICACIONES A REMARCAR DIGITAL.
                    if (a_tipReMarcar.includes(CodigoPresence)) {
                        console.log("Agendando", CodigoPresence)
                        const data = {
                            telefono: $("#G_TelefonoLlamada").val(),
                            Agente: 0,
                            CustomData1: $("#gtuuId").val(),
                            CampaignId: Campaign,
                            SourceIdPresence: G_SourcePresence
                        }
                        var ruta = "http://172.16.5.152:6792/ingresaSolicitud";


                        $.post(ruta, data)
                            .always(Result => {
                                console.log("Redireccionando tipRemarcar")
                                this.RedirectFin(fechaProgramada, ventaData, 2);
                            })

                    }
                    else {
                        this.RedirectFin(fechaProgramada, ventaData, 1);
                    }

                }
                // TIPIFICACION PARA EL CASO DE INBOUND.
                else if (IsInbound) {
                    if (a_tipExcluir.includes(CodigoPresence)) {
                        const data = {
                            telefono: $("#G_TelefonoLlamada").val(),
                            Agente: 0,
                            CustomData1: $("#gtuuId").val(),
                            CampaignId: 9
                        }
                        var ruta = "http://172.16.5.152:6792/ingresaSolicitud";

                        console.log("Guardando Programacion Inbound")
                        $.post(ruta, data)
                            .always(Result => {
                                console.log("Redireccionando Tip excluir")
                                this.RedirectFin(fechaProgramada, ventaData, 1);
                            })

                    }
                    else {
                        this.RedirectFin(fechaProgramada, ventaData, 2);
                    }


                }
                else {
                    console.log("Tipificacion general")
                    this.RedirectFin(fechaProgramada, ventaData, 2);
                }
                //console.log("Redireccionando Nunca Se reagendo")
                //this.RedirectFin(fechaProgramada, ventaData, 2);
            }


        })
            .fail((data, status) => {
             $("#GuardarGestion").attr("disabled", false);

        })
    }


    /*--------------------------------*/
    /*---------| GUARDA UNA VENTA .
    /*------------------------------*/

    RedirectFin(fechaProgramada, ventaData , programado) {
        const url_fin = BaseUrl + 'Home/fin/' + $("#gtuuId").val() + '?tipificacion=' + this.Tipificacion + '&fechaProgramada=' + fechaProgramada + '&Venta=' + ventaData + '&Programado=' + programado;
        window.location = url_fin;
    }

    GuardarVentaEditable() {



 
        if (this.optionTipificaion.val() == 0) {
            $("#GuardarGestionEditable").parent().append(string_mensaje("Se debe seleccionar la tipificacion"))
            return;
        }

        var fechaProgramada = $("#fechaLlamada").val() + " " + $("#horaLlamada").val() + ":" + $("#minutoLlamar").val();

        this.Tipificacion = $("#tipificacion").val();

        this.data = {
            CodigoTipificacion: this.Tipificacion,
            deletedAt: fechaProgramada,
            VENTA_PERMANENCIA: $("#VENTA_PERMANENCIA").val(),
            VENTA_TIPODE_DE_CLIENTE: $("#VENTA_TIPODE_DE_CLIENTE").val(),
            VENTA_TIPO_DE_SOLICITUD: $("#VENTA_TIPO_DE_SOLICITUD").val(),
            VENTA_MULTIPLAY: $("#VENTA_MULTIPLAY").val(),
            VENTA_CUENTA_VENTA: $("#VENTA_CUENTA_VENTA").val(),
            VENTA_NOMBRE_COMPLETO_CLIENTE: $("#VENTA_NOMBRE_COMPLETO_CLIENTE").val(),
            VENTA_captura_tipodoc: $("#VENTA_captura_tipodoc").val(),
            VENTA_NUMERO_CEDULA: $("#VENTA_NUMERO_CEDULA").val(),
            VENTA_captura_fechaExped: $("#VENTA_captura_fechaExped").val(),
            VENTA_TIPO_DE_RED: $("#VENTA_TIPO_DE_RED").val(),
            VENTA_CUENTA_MATRIZ: $("#VENTA_CUENTA_MATRIZ").val(),
            VENTA_captura_Estrato: $("#VENTA_captura_Estrato").val(),
            VENTA_OPERADOR_ACTUAL: $("#VENTA_OPERADOR_ACTUAL").val(),
            VENTA_TELEFONO_FIJO: $("#VENTA_TELEFONO_FIJO").val(),
            VENTA_CELULAR: $("#VENTA_CELULAR").val(),
            VENTA_captura_ent_correspondencia: $("#VENTA_captura_ent_correspondencia").val(),
            VENTA_Captura_Email: $("#VENTA_Captura_Email").val(),
            VENTA_CANAL: $("#VENTA_CANAL").val(),
            VENTA_captura_serviciosActuales: $("#VENTA_captura_serviciosActuales").val(),
            VENTA_SERVICIOS_VENDIDOS: $("#VENTA_SERVICIOS_VENDIDOS").val(),
            VENTA_AVANZADA: $("#VENTA_AVANZADA").val(),
            VENTA_VELOCIDAD_M: $("#VENTA_VELOCIDAD_M").val(),
            VENTA_TOMAS_ADICIONALES_TV: $("#VENTA_TOMAS_ADICIONALES_TV").val(),
            VENTA_captura_cobroInstalacion: $("#VENTA_captura_cobroInstalacion").val(),
            VENTA_POLITICA: $("#VENTA_POLITICA").val(),
            VENTA_captura_ActualizarTarifa: $("#VENTA_captura_ActualizarTarifa").val(),
            VENTA_RENTA_MENSUAL: $("#VENTA_RENTA_MENSUAL").val(),
            VENTA_captura_fechaAgendamiento: $("#VENTA_captura_fechaAgendamiento").val(),
            VENTA_captura_franja: $("#VENTA_captura_franja").val(),
            VENTA_captura_rentaMensual: $("#VENTA_captura_rentaMensual").val(),
            VENTA__ID_VISION: $("#VENTA__ID_VISION").val(),
            VENTA_RECHAZO_BACK: $("#VENTA_RECHAZO_BACK").val(),
            VENTA_NUMERO_A_ACTIVAR: $("#VENTA_NUMERO_A_ACTIVAR").val(),
            VENTA_PLAN_ACT: $("#VENTA_PLAN_ACT").val(),
            Venta_CantidadDeServicios: $("#Venta_CantidadDeServicios").val(),
            CODIGO_EVIDENTE: $("#CODIGO_EVIDENTE").val(),
            VENTA_DECOSADICIONALES: $("#VENTA_DECOSADICIONALES").val(),
            VENTA_LUGAREXPEDICION: $("#VENTA_LUGAREXPEDICION").val(),
            procesar: this.efectiva,
            acw: 0,
            TipificacionId: this.optionTipificaion.val(),
            Id: $("#gtuuId").val(),
            captura_ciudadId: $("#ayuda_ciudad").val(),
            captura_barrio: $("#captura_barrio").val(),
            captura_telefono1: $("#captura_telefono1").val(),
            captura_telefono2: $("#captura_telefono2").val(),
            captura_Direccion: $("#txt_direccion").val(),
            uVerificador: $("#UsuarioVerificador").val(),
            pVerificador: $("#PassVerificador").val(),
            captura_Observacion: $("#captura_Observacion").val(),
            AsuuId: $("#AsuuId").val(), // usu id
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        _error("listo para crear la venta")
        $("#GuardarGestion").attr('enabled',false)
        $("#GuardarGestionEditable").attr('enabled', false)
        var url = this.url;
        $.post(url.Gestion + 'CreateSaleEdit', this.data)
            .done((Result, status, statuscode) => {
                $("#GuardarGestion").attr('enabled', true)
                $("#GuardarGestionEditable").attr('enabled', true)
                if (statuscode.status == 201) {
                    _console("error data");
                    console.log(Result)
                    $.each(Result, function (val, elm) {
                        _console(elm);
                        _console(Carro.JsonDecodeErrors(elm));
                        $("#GuardarGestion").parent().append(Carro.JsonDecodeErrors(elm));
                        $("#GuardarGestionEditable").parent().append(Carro.JsonDecodeErrors(elm));
                        Carro.toBottom()
                    });
                }
                else {

                    const url_fin = BaseUrl + 'Home/fin/' + $("#gtuuId").val() + '?tipificacion=' + this.Tipificacion + '&fechaProgramada=' + fechaProgramada;
                    window.location = url_fin;
                }


            })
            .fail((data, status) => {
                $("#GuardarGestion").attr('enabled', true)
                $("#GuardarGestionEditable").attr('enabled', true)
            })
    }



}
var Carro = new Carrito();

/*
 *  AÑADE PRODUCTOS AL CARRO DE COMPRAS
 */
$("body").on("click", ".addDatos", function () {
    var s_elm = $(this);
    const padre = s_elm.parents("div.TipoPlan");
    _console("tipo plan padre");
    _console(padre);
    var fila = padre.find("select.select2 option:selected");
    _console(fila);
    cantidad = 1;

    if (padre.find("input[type=number]")) {
        cantidad = parseInt(padre.find("input[type=number]").val());
        _console("esta es la cantidad......")
        _console(cantidad);
    }

    if (isNaN(cantidad)) {
        cantidad = 1
    }
    _console("esta es la cantidad");
    _error(cantidad);


    var Item = {
        Id: fila.data('id'),
        nombre: fila.data('nombre'),
        estrato: fila.data('estrato'),
        tv: fila.data('tv'),
        ba: fila.data('ba'),
        estratonombre : fila.data('estratonombre'),
        canservicios: fila.data('canservicios'),
        tipo: fila.data('tipo'),
        descripcion: fila.data('descripcion'),
        codigo: fila.data('codigo'),
        cantidad: cantidad
    }


    if (Carro.checkExist(Item.Id)) {
        _error("Este producto ya existe");
        alert("este item ya fue agregado");
    }
    else if (Carro.checkEstrato(Item)) {
        _error("Este producto ya existe");
        alert("ya hay un producto con un estrato diferente");

    }
    //else if (Carro.ChecPrincipal(Item)) {
    //    _error("Ya se escogio un producto principal");
    //    alert("Ya se escogio un producto principal");

    //}

    else {
        
        Carro.AddProduct(Item);
    }

})


















function adminCalendar(Tipificacion, final) {
    const TipoProgramacionRegistro = [30009, 30011, 90, 22055, 22021];
    _error(Tipificacion)

    if (TipoProgramacionRegistro.includes(Tipificacion) && final == 1) {
        $("#idProgramarLlamada").show();
    }
    else {
        
        $("#idProgramarLlamada").hide();

    }
}

/**
 * Descripccion: Actualiza el dropdown de la tipificacion.
 * @param {any} elm
 * @param {any} Result
 */
function updateDropdown(elm, Result) {
    let dropdown = $("<select class='form-control'></select>");
    let dropdownContent = "<option selected value='0'>Seleccione</option>";
    dropdown.append(dropdownContent);

    $.each(Result, function (index, Value) {
        $(dropdown).append('<option  data-codigo="' + Value.codigo + '" data-final="' + normalizeTrueFalse(Value.final) + '" data-efectiva="' + normalizeTrueFalse(Value.GestionEfectiva) + '"  value="'
            + Value.Id + '">' +
            Value.Nombre + '</option>');
    });
    dropdown.addClass("dro_cascade_tipificacion");
    elm.parent().append("<br \>");
    elm.parent().append(dropdown);

    window.scrollTo(0, document.body.scrollHeight);
}



//muestra el formulario de creacion de cliente.
function showForm() {
    $("#fmrCrearCliet").show();
}


$("body").on("click", ".img-responsive.mostrarModalEvidencia", function () {
    $(".imgVista").attr("src", $(this).attr("src"));
    $("#viewImgNewPage").attr("href", $(this).attr("src"));
    $(".modal.modalEvidencia").modal().show();
})